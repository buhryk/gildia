<?php
namespace console\controllers;

use backend\modules\news\models\News;
use backend\modules\news\models\NewsLang;
use phpQuery;
use yii\console\Controller;
use Yii;

class ParseController extends Controller
{
    public function actionStart()
    {

        $news = [];

        $j = 0;
        for ($i = 1; $i <= 91; $i++) {
//            pr($i);

            $url = 'https://www.uscc.ua/ru/infocentr/novosti/page/'.$i;
            $data_site = file_get_contents($url); // получаем страницу сайта-донора

            $document = phpQuery::newDocument($data_site);
            $year = $document->find('.news .dates em:first')->text();
            $content_prev = $document->find('.news .item');

            // перебираем в цикле все посты
            $positionOnPage = 0;
            foreach ($content_prev as $el) { $j++; $positionOnPage++;
                // Парсим превьюшки статей
                $pq = pq($el); // pq это аналог $ в jQuery
                $title = $pq->find('strong')->text(); // парсим заголовок статьи
                $description = $pq->find('.iWrp span')->text();
                $day = $pq->find('.day')->text();
                $month = $pq->find('.month')->text();
                $img = $pq->find('img')->attr('src');

                $href = 'https://www.uscc.ua' . $pq->find('a')->attr('href');
                $view = phpQuery::newDocument(file_get_contents($href));
                pq($view)->find('h5')->remove();
                $text = pq($view)->find('#printElement')->html();
                $documentLink = 'https://www.uscc.ua' . pq($view)->find('.hasLink a')->attr('href');

                $urlImage = 'https://www.uscc.ua' . $img;
//                file_put_contents("frontend/web/img/news/parse/$j.jpeg", file_get_contents($urlImage));
//                file_put_contents("frontend/web/img/news/parse/$j.jpeg", file_get_contents('https://www.uscc.ua/files_small/46b5a425ba8ed540e1a4d449b0ae36ad_ext_jpeg_height_96_width_144_src_files_1_14042020_png.jpeg'));
//                pr($year);
                $year = $year != '' ? $year : 2020;
                $alias = stristr($href, '.html', true);
                $alias = stristr($alias, 'novosti/');
                $alias = substr($alias, 8,100);
//                pr($alias);
                $news[] = [
                    'item' => 'page - ' .$i . '; position - ' .$positionOnPage,
                    'title' => $title,
                    'description' => $description,
                    'day' => $day,
                    'month' => $month,
                    'year' => $year,
                    'data' => $year.'-'. $this->monthNumber($month) .'-'. $day,
                    'img' => $urlImage,
                    'alias' => $alias,
                    'text' => $text,
                ];
            }
        }

        News::deleteAll(['parsed' => 1]);
//pr($news);
        foreach ($news as $item){
            echo $item['item'] . "\n";

            $model = new News();
            $model->parsed = 1;
            $model->type = 1;
            $model->status = 1;
            $model->alias = $item['alias'];
            $model->data_pub = $item['data'];

            $model->save(false);
//            pr($item);

            $modelLang = new NewsLang();
            $modelLang->record_id = $model->getPrimaryKey();
            $modelLang->lang_id = 1;
            $modelLang->title = $item['title'];
            $modelLang->description = $item['description'];
            $modelLang->text = $item['text'];
            $modelLang->save(false);

            $modelLang = new NewsLang();
            $modelLang->record_id = $model->getPrimaryKey();
            $modelLang->lang_id = 3;
            $modelLang->title = $item['title'];
            $modelLang->description = $item['description'];
            $modelLang->text = $item['text'];
            $modelLang->save(false);
        }

echo 'ok';


    }

    function monthNumber($month) {
        $months = [
            'Янв' => '01',
            'Фев' => '02',
            'Мар' => '03',
            'Апр' => '04',
            'Май' => '05',
            'Июн' => '06',
            'Июл' => '07',
            'Авг' => '08',
            'Сен' => '09',
            'Окт' => '10',
            'Ноя' => '11',
            'Дек' => '12',
        ];
        return $months[$month];
    }
}