<?php
namespace console\controllers;

use common\components\UserRoleRule;
use yii\console\Controller;
use Yii;

class RbacController extends Controller
{
    public function actionStart()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $dashboard = $auth->createPermission('dashboard');
        $dashboard->description = 'Админ панель';
        $auth->add($dashboard);

        $userManagement = $auth->createPermission('userManagement');
        $userManagement->description = 'Управление пользователями';
        $auth->add($userManagement);

        $setting = $auth->createPermission('setting');
        $setting->description = 'Налаштування';
        $auth->add($setting);

        $rule = new UserRoleRule();
        $auth->add($rule);


        // добавляем роль manager
        $manager = $auth->createRole('manager');
        $manager->ruleName = $rule->name;
        $auth->add($manager);

        // добавляем роль admin
        $admin = $auth->createRole('admin');
        $admin->ruleName = $rule->name;
        $auth->add($admin);
        $auth->addChild($admin, $dashboard);
        $auth->addChild($admin, $manager);

    }
}