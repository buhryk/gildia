<?php

use yii\db\Migration;
use backend\modules\core\models\Lang;

/**
 * Class m171209_113430_add_lang
 */
class m171209_113430_add_lang extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = new Lang([
            'name' => 'ua',
            'url' => 'ua',
            'local' => 'uk-UK',
            'status' => Lang::STATUS_ACTIVE,
            'default' => Lang::DEFAULT_YES,
            'position' => 1,
        ]);

        $model->save();

        $model = new Lang([
            'name' => 'en',
            'url' => 'en',
            'local' => 'en',
            'status' => Lang::STATUS_ACTIVE,
            'default' => Lang::DEFAULT_NO,
            'position' => 2,
        ]);

        $model->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171209_113430_add_lang cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171209_113430_add_lang cannot be reverted.\n";

        return false;
    }
    */
}
