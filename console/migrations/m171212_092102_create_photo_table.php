<?php

use yii\db\Migration;

/**
 * Handles the creation of table `photo`.
 */
class m171212_092102_create_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('photo', [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'data_pub' => $this->dateTime()->null(),
            'image' => $this->string(),
            'alias' => $this->string(),
            'additional_data' => $this->json(),
        ]);

        $this->createTable('photo_lang', [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->null(),
        ]);

        $this->addForeignKey('photo-lang-id','photo_lang', 'lang_id', '{{%lang}}', 'id', 'CASCADE');
        $this->addForeignKey('photo-record-id', 'photo_lang', 'record_id', '{{%photo}}', 'id', 'CASCADE');
        $this->addPrimaryKey('photo_lang-pk', 'photo_lang', ['record_id', 'lang_id']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('photo');
    }

    public function json()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('JSON');
    }
}
