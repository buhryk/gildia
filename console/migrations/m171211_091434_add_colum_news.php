<?php

use yii\db\Migration;

/**
 * Class m171211_091434_add_colum_news
 */
class m171211_091434_add_colum_news extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('news', 'data_pub', $this->dateTime()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171211_091434_add_colum_news cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_091434_add_colum_news cannot be reverted.\n";

        return false;
    }
    */
}
