<?php

use yii\db\Migration;

/**
 * Class m171212_201109_add_slider
 */
class m171212_201109_add_slider extends Migration
{
    public $table = '{{%slider}}';
    public $table_item = '{{%slider_item}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'status' => $this->smallInteger(),
            'position' => $this->integer(),
        ]);

        $this->createTable($this->table_item, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'status' => $this->smallInteger(),
            'position' => $this->integer(),
            'image' => $this->string(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
            'lang_id' => $this->integer()->notNull(),
            'slider_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('alider_item_slider-id', $this->table_item, 'slider_id', $this->table, 'id', 'CASCADE');
        $this->addForeignKey('alider_item_lang-id', $this->table_item, 'lang_id', "{{%lang}}", 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171212_201109_add_slider cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171212_201109_add_slider cannot be reverted.\n";

        return false;
    }
    */
}
