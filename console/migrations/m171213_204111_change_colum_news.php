<?php

use yii\db\Migration;

/**
 * Class m171213_204111_change_colum_news
 */
class m171213_204111_change_colum_news extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('news_lang', 'title', $this->string(255)->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171213_204111_change_colum_news cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171213_204111_change_colum_news cannot be reverted.\n";

        return false;
    }
    */
}
