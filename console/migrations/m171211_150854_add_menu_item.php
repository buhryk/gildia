<?php

use yii\db\Migration;

/**
 * Class m171211_150854_add_menu_item
 */
class m171211_150854_add_menu_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('menu', 'level', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171211_150854_add_menu_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_150854_add_menu_item cannot be reverted.\n";

        return false;
    }
    */
}
