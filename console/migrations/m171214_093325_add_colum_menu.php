<?php

use yii\db\Migration;

/**
 * Class m171214_093325_add_colum_menu
 */
class m171214_093325_add_colum_menu extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('menu', 'show_children', $this->smallInteger(5)->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171214_093325_add_colum_menu cannot be reverted.\n";

        $this->dropColumn('menu','show_children' );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171214_093325_add_colum_menu cannot be reverted.\n";

        return false;
    }
    */
}
