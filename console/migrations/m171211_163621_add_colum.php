<?php

use yii\db\Migration;

/**
 * Class m171211_163621_add_colum
 */
class m171211_163621_add_colum extends Migration
{

    public function safeUp()
    {
        $this->addColumn('page_lang', 'additional_title', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171211_163621_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_163621_add_colum cannot be reverted.\n";

        return false;
    }
    */
}
