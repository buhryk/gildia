<?php

use yii\db\Migration;

/**
 * Class m171209_122310_add_colum_category
 */
class m171209_122310_add_colum_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('page_category', 'parent_id', $this->integer()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171209_122310_add_colum_category cannot be reverted.\n";

        return false;
    }

}
