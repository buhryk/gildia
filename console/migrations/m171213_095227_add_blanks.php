<?php

use yii\db\Migration;

/**
 * Class m171213_095227_add_blanks
 */
class m171213_095227_add_blanks extends Migration
{
    public $blanks = 'blank';
    public $blanks_item = 'blank_item';

    public function safeUp()
    {
        $this->createTable($this->blanks, [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'name' => $this->string(),
            'menu_id' => $this->integer(),
            'additional_data' => $this->json(),
        ]);

        $this->createTable($this->blanks_item, [
            'id' => $this->primaryKey(),
            'title' => $this->string(300)->notNull(),
            'file' => $this->string(300)->notNull(),
            'blank_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('blank-lang-id', $this->blanks_item, 'lang_id', 'lang', 'id', 'CASCADE');
        $this->addForeignKey('blank-record-id', $this->blanks_item, 'blank_id', $this->blanks, 'id', 'CASCADE');

        $this->addForeignKey('blank-menu-id-key', $this->blanks, 'menu_id', 'menu', 'id', 'CASCADE');
    }


    public function json()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('JSON');
    }

    public function safeDown()
    {
        echo "m171213_095227_add_blanks cannot be reverted.\n";

        return false;
    }

}
