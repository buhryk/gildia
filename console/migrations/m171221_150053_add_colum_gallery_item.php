<?php

use yii\db\Migration;

/**
 * Class m171221_150053_add_colum_gallery_item
 */
class m171221_150053_add_colum_gallery_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('gallery_item', 'description', $this->text()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171221_150053_add_colum_gallery_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171221_150053_add_colum_gallery_item cannot be reverted.\n";

        return false;
    }
    */
}
