<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 01.02.2018
 * Time: 10:20
 */

namespace common\components;

use backend\modules\catalog\models\Catalog;
use backend\modules\catalog\models\CatalogCategorLang;
use backend\modules\catalog\models\CatalogCategory;
use backend\modules\catalog\models\CatalogLang;
use backend\modules\catalog\models\CatalogToCategory;
use common\models\Lang;
use Yii;
use yii\db\Exception;

use yii\base\Component;
require_once(Yii::getAlias('@my_vendor/PHPExcel/Classes/PHPExcel.php'));

class ExcelCatalog extends Component
{
    static $arr = [];

    public function error($err = false)
    {
        array_push(self::$arr, $err);
    }

    public static function import($inputFile)
    {

        try{

            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExel = $objReader->load($inputFile);
        }catch (Exception $e){
            die('Error');
        }



        if (Lang::getCurrent()->id == 1)
        {
            $sheet = $objPHPExel->getSheet(2);
        } else {
            $sheet = $objPHPExel->getSheet(1);
        }

//        $sheet_ua = $objPHPExel->getSheet(2);

//        pr(Lang::getCurrent()->id);
        for( $row = 3; $row <= 11; $row++){
            $columnLetters = ['B','C','D','E','F','H','I','K','L','M','O','P','Q','R','T','U', 'G', 'N'];
            $category_ids = [];
            $category_ids_ua = [];
            foreach ($columnLetters as $column){
                // RU
               $cellValue = $sheet->getCell($column.$row)->getValue();
               $query = CatalogCategorLang::find()->where(['title' => trim($cellValue), 'lang_id' => Lang::getCurrent()->id]);

               if ($query->count() == 1) {
                   $category_ids[] = $query->one()->record_id;
               }

            }


            $text = '';

            $title = $sheet->getCell('A'.$row)->getValue();

            $company_name = $sheet->getCell('B'.$row)->getValue();
            $housing_construction = $sheet->getCell('C'.$row)->getValue();
            $agrarian_complex = $sheet->getCell('D'.$row)->getValue();
            $industrial_building = $sheet->getCell('F'.$row)->getValue();
            $logistic_complex = $sheet->getCell('G'.$row)->getValue();
            $h = $sheet->getCell('H'.$row)->getValue();
            $construction_region = $sheet->getCell('J'.$row)->getValue();
            $construction_type = $sheet->getCell('K'.$row)->getValue();
            $bmz_technology = $sheet->getCell('L'.$row)->getValue();
            $main_material = $sheet->getCell('M'.$row)->getValue();
            $o = $sheet->getCell('O'.$row)->getValue();
            $p = $sheet->getCell('P'.$row)->getValue();
            $s = $sheet->getCell('S'.$row)->getValue();
            $t = $sheet->getCell('T'.$row)->getValue();
            $v = $sheet->getCell('V'.$row)->getValue();
//            $length = $sheet->getCell('M'.$row)->getValue();

            $additional = $sheet->getCell('W'.$row)->getValue();

            if ($company_name) {
                $text .= '<p><strong>Участник УЦСС</strong>: ' . $company_name . '</p>';
            }
//            if ($year_of_implementation) {
//                $text .= '<p><strong>Год реализации объекта</strong>: ' . $year_of_implementation . '</p>';
//            }
            if ($housing_construction) {
                $text .= '<p><strong>Жилищное строительство</strong>: ' . $housing_construction . '</p>';
            }
            if ($agrarian_complex) {
                $text .= '<p><strong>Аграрный комплекс</strong>: ' . $agrarian_complex . '</p>';
            }
            if ($industrial_building) {
                $text .= '<p><strong>Промышленные здания</strong>: ' . $industrial_building . '</p>';
            }
            if ($logistic_complex) {
                $text .= '<p><strong>Логистический коплекс</strong>: ' . $logistic_complex . '</p>';
            }
            if ($h) {
                $text .= '<p><strong>Инфраструктурные объекты</strong>: ' . $h . '</p>';
            }
            if ($construction_region) {
                $text .= '<p><strong>Регион строительства</strong>: ' . $construction_region . '</p>';
            }
            if ($construction_type) {
                $text .= '<p><strong>Тип строительства объекта</strong>: ' . $construction_type . '</p>';
            }
            if ($bmz_technology) {
                $text .= '<p><strong>Технология строительства</strong>: ' . $bmz_technology . '</p>';
            }
            if ($main_material) {
                $text .= '<p><strong>Основной материал несущих конструкций</strong>: ' . $main_material . '</p>';
            }
            if ($o) {
                $text .= '<p><strong>Этажность</strong>: ' . $o . '</p>';
            }
            if ($p) {
                $text .= '<p><strong>Конструктивное решение несущего каркаса здания/сооружения</strong>: ' . $p . '</p>';
            }
            if ($s) {
                $text .= '<p><strong>Тип кровли здания</strong>: ' . $s . '</p>';
            }
            if ($t) {
                $text .= '<p><strong>Конструктивные решения кровли здания</strong>: ' . $t . '</p>';
            }
            if ($v) {
                $text .= '<p><strong>Конструктивные решения плит перекрытий</strong>: ' . $v . '</p>';
            }
//            if ($length) {
//                $text .= '<p><strong>Длина пролета</strong>: ' . $length . '</p>';
//            }

            if ($additional) {
                $text .= '<p><strong>Дополнительная информация</strong>: ' . $additional . '</p>';
            }


//        pr($text);
//            Catalog::deleteAll(['excel_row' => $row]);
            CatalogLang::deleteAll(['lang_id' => Lang::getCurrent()->id, 'record_id' => $row]);

//            $query = Catalog::findOne($row);
//                pr($query);
            if ($query = Catalog::findOne($row)){
                $model = $query;

                $model->lang_id = Lang::getCurrent()->id;
                
                $model->title = $title;
                $model->text = $text;
                $model->data_pub = date('Y-m-d');
                $model->update();
            } else {
                $model = new Catalog();

                if (!$model->image){
                    $model->image = '/images/catalog.png';
                }
                $model->lang_id = Lang::getCurrent()->id;
                $model->id = $row;
                $model->title = $title;
                $model->text = $text;
                $model->data_pub = date('Y-m-d');
                $model->save();
            }


            CatalogToCategory::deleteAll(['catalog_id' => $model->id]);

            foreach ($category_ids as $item) {

                $catalog_to_category = new CatalogToCategory();
                $catalog_to_category->catalog_id = $model->id;
                $catalog_to_category->category_id = $item;
                $catalog_to_category->save();
            }
        }
        pr('ok');


    }


}