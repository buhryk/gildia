<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 01.02.2018
 * Time: 10:20
 */

namespace common\components;

use backend\modules\catalog\models\Catalog;
use backend\modules\catalog\models\CatalogCategorLang;
use backend\modules\catalog\models\CatalogCategory;
use backend\modules\catalog\models\CatalogLang;
use backend\modules\catalog\models\CatalogToCategory;
use backend\modules\table\models\TableToFirm;
use backend\modules\table\models\FirmLang;
use backend\modules\table\models\TableExcel;
use backend\modules\table\models\TableExcelLang;
use backend\modules\table\models\TableTypeLang;
use common\models\Lang;
use Yii;
use yii\db\Exception;

use yii\base\Component;
require_once(Yii::getAlias('@my_vendor/PHPExcel/Classes/PHPExcel.php'));

class ExcelTable extends Component
{

    public static function import($inputFile)
    {

        try{

            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExel = $objReader->load($inputFile);
        }catch (Exception $e){
            die('Error');
        }



        $sheet = $objPHPExel->getSheet(0);
        $sheet2 = $objPHPExel->getSheet(1);
//pr($sheet->getHighestRow());
        for( $row = 2; $row <= $sheet->getHighestRow(); $row++){
//            pr($sheet->getCell('B'.$row)->getValue());

            $table_id = $sheet->getCell('H'.$row)->getFormattedValue();

            $title = $sheet->getCell('A'.$row)->getValue();
            $title_ua = $sheet2->getCell('A'.$row)->getValue();
//pr($title);
            $cut = $sheet->getCell('B'.$row)->getValue();
            $cut_ua = $sheet2->getCell('B'.$row)->getValue();

            $mark = $sheet->getCell('C'.$row)->getValue();
            $mark_ua = $sheet2->getCell('C'.$row)->getValue();

            $standard = $sheet->getCell('D'.$row)->getValue();
            $standard_ua = $sheet2->getCell('D'.$row)->getValue();

            $type_id = 0;
           $type = $sheet->getCell('G'.$row)->getValue();
           $query = TableTypeLang::find()->where(['title' => trim($type), 'lang_id' => 3]);
           if ($query->count() == 1) {
               $type_id = $query->one()->record_id;
           }


           $firm_ids = [];
           $firms = explode(',', $sheet->getCell('E'.$row)->getValue());

           foreach ($firms as $firm){
               $query = FirmLang::find()->where(['title' => trim($firm), 'lang_id' => 3]);
               if ($query->count() == 1) {
                   $firm_ids[] = $query->one()->record_id;
               }
           }



            TableExcelLang::deleteAll(['record_id' => $table_id]);


            if ($query = TableExcel::findOne($table_id)){
                $model = $query;
                $model->type_id = $type_id;
                $model->update(false);


                $modelLang = new TableExcelLang();
                $modelLang->record_id = $table_id;
                $modelLang->lang_id = 3;
                $modelLang->title = $title;
                $modelLang->cut = $cut;
                $modelLang->mark = $mark;
                $modelLang->standard = $standard;
                $modelLang->save();

                $modelLang = new TableExcelLang();
                $modelLang->record_id = $table_id;
                $modelLang->lang_id = 1;
                $modelLang->title = $title_ua;
                $modelLang->cut = $cut_ua;
                $modelLang->mark = $mark_ua;
                $modelLang->standard = $standard_ua;
                $modelLang->save();
            } else {
                $model = new TableExcel();

                $model->id = $table_id;
                $model->lang_id = 3;
                $model->title = $title;
                $model->cut = $cut;
                $model->mark = $mark;
                $model->standard = $standard;
                $model->type_id = $type_id;

                $model->save();
            }

            TableToFirm::deleteAll(['table_id' => $model->id]);

            foreach ($firm_ids as $item) {

                $table_to_firm = new TableToFirm();
                $table_to_firm->table_id = $model->id;
                $table_to_firm->firm_id = $item;
                $table_to_firm->save();
            }

        }
        return true;
        pr('ok');


    }


}