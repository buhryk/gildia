<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 01.02.2018
 * Time: 10:20
 */

namespace common\components;

use backend\modules\catalog\models\Catalog;
use backend\modules\catalog\models\CatalogCategorLang;
use backend\modules\catalog\models\CatalogCategory;
use backend\modules\catalog\models\CatalogLang;
use backend\modules\catalog\models\CatalogToCategory;
use backend\modules\table\models\TableClassLang;
use backend\modules\table\models\TableExcelMark;
use backend\modules\table\models\TableExcelMarkLang;
use backend\modules\table\models\TableToFirm;
use backend\modules\table\models\FirmLang;
use backend\modules\table\models\TableExcel;
use backend\modules\table\models\TableExcelLang;
use backend\modules\table\models\TableTypeLang;
use common\models\Lang;
use Yii;
use yii\db\Exception;

use yii\base\Component;
require_once(Yii::getAlias('@my_vendor/PHPExcel/Classes/PHPExcel.php'));

class ExcelMarkTable extends Component
{

    public static function import($inputFile)
    {

        try{

            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExel = $objReader->load($inputFile);
        }catch (Exception $e){
            die('Error');
        }



        $sheet = $objPHPExel->getSheet(0);
        $sheet2 = $objPHPExel->getSheet(1);
//pr($sheet->getHighestRow());
        for( $row = 2; $row <= $sheet->getHighestRow(); $row++){
//            pr($sheet->getCell('B'.$row)->getValue());

            $table_id = $sheet->getCell('H'.$row)->getFormattedValue();
//pr($table_id);

//pr($title);
            $class_or_mark = $sheet->getCell('B'.$row)->getValue();
            $class_or_mark_ua = $sheet2->getCell('B'.$row)->getValue();

            $gost = $sheet->getCell('C'.$row)->getValue();
            $gost_ua = $sheet2->getCell('C'.$row)->getValue();

            $action = $sheet->getCell('D'.$row)->getValue();
            $action_ua = $sheet2->getCell('D'.$row)->getValue();

            $alternative = $sheet->getCell('E'.$row)->getValue();
            $alternative_ua = $sheet2->getCell('E'.$row)->getValue();

            $standard = $sheet->getCell('F'.$row)->getValue();
            $standard_ua = $sheet2->getCell('F'.$row)->getValue();

            $notion = $sheet->getCell('G'.$row)->getValue();
            $notion_ua = $sheet2->getCell('G'.$row)->getValue();

            $class_id = 0;
           $class = $sheet->getCell('A'.$row)->getValue();
           $query = TableClassLang::find()->where(['title' => trim($class), 'lang_id' => 3]);
           if ($query->count() == 1) {
               $class_id = $query->one()->record_id;
           }


            TableExcelMarkLang::deleteAll(['record_id' => $table_id]);


            if ($query = TableExcelMark::findOne($table_id)){
                $model = $query;
                $model->class_id = $class_id;
                $model->update(false);


                $modelLang = new TableExcelMarkLang();
                $modelLang->record_id = $table_id;
                $modelLang->lang_id = 3;
                $modelLang->class_or_mark = $class_or_mark;
                $modelLang->gost = $gost;
                $modelLang->action = $action;
                $modelLang->alternative = $alternative;
                $modelLang->standard = $standard;
                $modelLang->notion = $notion;
                $modelLang->save();

                $modelLang = new TableExcelMarkLang();
                $modelLang->record_id = $table_id;
                $modelLang->lang_id = 1;
                $modelLang->class_or_mark = $class_or_mark_ua;
                $modelLang->gost = $gost_ua;
                $modelLang->action = $action_ua;
                $modelLang->alternative = $alternative_ua;
                $modelLang->standard = $standard_ua;
                $modelLang->notion = $notion_ua;
                $modelLang->save();
            } else {
                $model = new TableExcelMark();

                $model->id = $table_id;
                $model->lang_id = 3;
                $model->class_or_mark = $class_or_mark;
                $model->gost = $gost;
                $model->action = $action;
                $model->alternative = $alternative;
                $model->standard = $standard;
                $model->notion = $notion;
                $model->class_id = $class_id;

                $model->save();
            }

        }
        return true;
        pr('ok');


    }


}