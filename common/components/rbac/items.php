<?php
return [
    'dashboard' => [
        'type' => 2,
        'description' => 'Админ панель',
    ],
    'userManagement' => [
        'type' => 2,
        'description' => 'Управление пользователями',
    ],
    'setting' => [
        'type' => 2,
        'description' => 'Налаштування',
    ],
    'manager' => [
        'type' => 1,
        'ruleName' => 'userRole',
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userRole',
        'children' => [
            'dashboard',
            'manager',
        ],
    ],
];
