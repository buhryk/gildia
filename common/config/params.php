<?php
return [
    'adminEmail' => 'info@ukragroexpert.com.ua',
    'supportEmail' => 'info@ukragroexpert.com.ua',
    'user.passwordResetTokenExpire' => 3600,
];
