<table style="border: 1px solid #ddd; border-collapse: collapse; width: 100%;">

    <tbody>
    <?php use yii\helpers\Url;

    $i = 1; foreach($data as $k => $item): ?>
        <?php if ($item):?>
            <tr>
                <td style="padding: 8px; border: 1px solid #ddd;"><?= $k ?></td>
                <?php if ($k != 'Файл'): ?>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= is_array($item) ? implode(', ', $item) : $item ?></td>
                <?php else: ?>
                    <td style="padding: 8px; border: 1px solid #ddd;"><a href="<?= $_SERVER['SERVER_NAME'].'/uploads/contact/' . $item?>"><?= Url::base(true).'/uploads/contact/' . $item?></a></td>
                <?php endif; ?>
            </tr>
        <?php endif; ?>
        <?php $i++; endforeach; ?>
    </tbody>
</table>