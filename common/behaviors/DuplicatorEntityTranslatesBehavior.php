<?php
namespace common\behaviors;

use common\models\Lang;
use yii\base\Behavior;

class DuplicatorEntityTranslatesBehavior extends Behavior
{
    public function duplicate()
    {
        $langs = Lang::find()->all();
        $newEntities = [];

        foreach ($langs as $lang) {
            if ($lang->id != $this->owner->lang_id) {
                $newTranslate = clone $this->owner;
                $newTranslate->lang_id = $lang->id;

                $attributes = $newTranslate->attributes;
                if (isset($attributes['id'])) {
                    unset($attributes['id']);
                }
                $newEntities[] = $attributes;
            }
        }

        if ($newEntities) {
            $attributes = array_keys($newEntities[0]);

            \Yii::$app->db->createCommand()->batchInsert($this->owner->tableName(), $attributes, $newEntities)->execute();
        }
    }

    public function events()
    {
        return [
            \yii\db\ActiveRecord::EVENT_AFTER_INSERT => 'duplicate',
        ];
    }
}