<?php

return [
    'Your name' => 'Ваше имя',
    'Your email' => 'Ваш email',
    'Website url' => 'Адрес сайта',
    'Success complete form message' => 'Ваша заявка успешно отправлена. Скоро наш менеджер свяжется с вами.',
    'Get Your Free SEOWAVE' => 'Get Your Free SEOWAVE',
    'Analysis Quote' => 'Analysis Quote',
    'Create request' => 'Отправить заявку'
];