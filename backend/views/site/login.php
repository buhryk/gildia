<?php


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="animate form login_form">
    <section class="login_content">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <h1>Вхід</h1>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton('Вхід', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <br />
        <div>
            <h1><i class="fa fa-paw"></i> <?= Yii::$app->name; ?></h1>
            <p>©<?= date('Y'); ?> All Rights Reserved. <?= Yii::$app->name; ?> </p>
        </div>

        <?php ActiveForm::end(); ?>
    </section>
</div>