<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;

$user = Yii::$app->user->identity;
?>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<?=Url::to(['/']) ?>" class="site_title"><i class="fa fa-paw"></i> <span>Admin</span></a>
        </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <?php echo \backend\widgets\AdminMenuWidget::widget([]); ?>
<!--                <h3>Меню</h3>-->
<!--                --><?//= Menu::widget([
//                        'options' => ['class' => 'nav side-menu'],
//                        'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
//                        'encodeLabels' => false,
//                        'activateParents' => true,
//                        'items' => [
//                            ['label' => '<i class="fa fa-folder"></i> Сторінки ', 'url' => ['/core/page']],
//                            ['label' => '<i class="fa fa-bars"></i> Меню ', 'url' => ['/core/menu']],
//                            ['label' => '<i class="fa  fa-window-maximize"></i> Бланки ', 'url' => ['/blank']],
//                            ['label' => '<i class="fa fa-file-powerpoint-o"></i>Видання ', 'url' => ['/publication']],
//                            ['label' => '<i class="fa fa-sticky-note"></i> Віджети', 'url' => ['/core/widget/']],
//                            ['label' => '<i class="fa fa-television"></i> Слайдер', 'url' => ['/slider/']],
//                            ['label' => '<i class="fa fa-folder"></i> Новини <span class="fa fa-chevron-down"></span>', 'url' => '#',
//                                'items' => [
//                                    ['label' => 'Новини', 'url' => ['/news/news']],
//                                    ['label' => 'Категорії новин ', 'url' => ['/news/category']
//                                ],
//                            ]],
//                            ['label' => '<i class="fa fa-pencil-square"></i> Галерея', 'url' => ['/gallery/gallery']],
//                            ['label' => '<i class="fa fa-question-circle"></i> FAQ', 'url' => ['/faq/faq']],
//                            ['label' => '<i class="fa fa-pencil-square"></i> Переклад інтерфейсу', 'url' => ['/translate']],
////                            ['label' => '<i class="fa fa-users"></i> Користувачі', 'url' => ['/user/user/index'],  'visible' => Yii::$app->user->can('admin')],
//                            ['label' => '<i class="fa fa-cogs"></i>Налаштування', 'url' => ['/core/setting/index']],
//                        ],
//                    ]);
//                ?>
            </div>
        </div>
        <!-- /sidebar menu -->
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip"  data-method="post" data-placement="top" title="Logout" href="<?=Url::to(['/site/logout']) ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="/images/user.png" alt=""><?= Yii::$app->user->identity->name . ' ' . Yii::$app->user->identity->surname; ?>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
<!--                        <li><a href="--><?//=Url::to(['/user/profile/view']) ?><!--"> Профиль </a></li>-->
                        <li><a href="<?=Url::to(['/site/logout']) ?>" data-method="post"><i class="fa fa-sign-out pull-right"></i> Выйти</a></li>
                    </ul>
                </li>
                <li style="float: right"><?= \backend\widgets\WLangHeader::widget([]); ?></li>
            </ul>
        </nav>
    </div>
</div>
<!-- top navigation -->