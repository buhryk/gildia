<?php
/**
 * @var View $this
 * @var SourceMessageSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
use Zelenin\yii\modules\I18n\models\search\SourceMessageSearch;
use Zelenin\yii\modules\I18n\models\SourceMessage;

$this->title = 'Переклади';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="news-index">
    <?php Pjax::begin(['id' => 'translate-list']);
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            [
                'attribute' => 'id',
                'value' => function ($model, $index, $dataColumn) {
                    return $model->id;
                },
                'filter' => false
            ],
            [
                'attribute' => 'translate',
                'label' => 'Переклад',
                'format' => 'raw',
                'value' => function ($model) {
                    $messages = $model->messages;
                    return implode('<br>---------------------<br>', ArrayHelper::getColumn($messages, 'translation'));
                }
            ],
            [
                'attribute' => 'message',
                'label' => 'Джерело повідомлення',
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return Html::a($model->message, ['update', 'id' => $model->id], ['data' => ['pjax' => 0], 'class'=>'modalButton']);
                }
            ],
            [
                'attribute' => 'category',
                'label' => 'Категорія',
                'value' => function ($model, $index, $dataColumn) {
                    return $model->category;
                },
                'filter' => ArrayHelper::map($searchModel::getCategories(), 'category', 'category')
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'value' => function ($model, $index, $widget) {
                    /** @var SourceMessage $model */
                    return $model->isTranslated() ? 'Translated' : 'Not translated';
                },
                'filter' => $searchModel->getStatus()
            ],
            [
                'contentOptions' => ['style'=>'width: 72px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs modalButton', 'title' => 'Редагувати']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-danger btn-xs', 'title' => 'Видалити',
                                'onclick' => 'return confirm("Ви дійсно хочете видалити даний запис?")']
                        );
                }
            ],
        ]
    ]);

    Pjax::end(); ?>
</div>

