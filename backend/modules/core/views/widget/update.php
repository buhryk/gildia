<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\Widget */

$this->title = 'Редагування: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Віджети', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';
?>
<div class="widget-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
