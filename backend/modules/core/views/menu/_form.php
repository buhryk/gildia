<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\core\models\Menu;
use yii\helpers\ArrayHelper;
use backend\modules\core\models\Page;
use kartik\select2\Select2;

$parentsList = Menu::find()
    ->where(['status' => Menu::STATUS_ACTIVE])
    ->andFilterWhere(['!=', 'id', $model->id])
    ->orderBy(['position' => SORT_ASC])
    ->all();
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
<!--        <div class="col-md-6">-->
<!--            --><?//=  $form->field($model, 'page_id')->widget(Select2::classname(), [
//                'data' => Page::getPageAll(true),
//                'language' => 'de',
//                'options' => ['placeholder' => 'Select a state ...'],
//                'pluginOptions' => [
//                    'allowClear' => true
//                ],
//            ]); ?>
<!--        </div>-->
    </div>

    <div class="row">
        <div class="col-md-6 item">
            <?= $form->field($model, 'absolute_url')->checkbox() ?>
            <div class="<?= $model->absolute_url ? '' : 'hidden' ?>" id="block-url">
                <?= $form->field($model, 'url')->textInput() ?>
            </div>

        </div>
    </div>

    <div class="row">
<!--        <div class="col-md-4">-->
<!--            --><?//=  $form->field($model, 'parent')->widget(Select2::classname(), [
//                'data' => Menu::getMenuAll(true, null, $model->id),
//                'language' => 'de',
//                'options' => ['placeholder' => 'Select a state ...'],
//                'pluginOptions' => [
//                    'allowClear' => true
//                ],
//            ]); ?>
<!--        </div>-->
<!--        <div class="col-md-4">-->
<!--            --><?//= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
<!--        </div>-->
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList($model::getTypeList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'show_children')->checkbox() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'not_show_menu')->checkbox() ?>
        </div>
<!--        <div class="col-md-4">-->
<!--            --><?//= $form->field($model, 'show_in_head')->checkbox() ?>
<!--        </div>-->
<!--        <div class="col-md-4">-->
<!--            --><?//= $form->field($model, 'show_in_footer')->checkbox() ?>
<!--        </div>-->
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?=$this->registerJs("
    $('#menu-absolute_url').on('click', function() {
        if ($(this).prop('checked')) {
           $('#block-url').removeClass('hidden');
        } else {
            $('#block-url').addClass('hidden');
        }
    })
", \yii\web\View::POS_READY) ?>