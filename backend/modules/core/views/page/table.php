<?php

use backend\assets\PageAsset;
use backend\modules\core\models\Page as CurrentModel;
use backend\modules\event\models\Event;
use backend\modules\faq\models\Faq;
use backend\modules\section\models\SectionItem;
use backend\modules\table\models\Firm;
use backend\modules\table\models\Table as ModelData;
use backend\modules\table\models\TableType;
use backend\widgets\ImperaviWidget;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use backend\modules\core\models\Page;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

\backend\widgets\SortWidget::widget(['className' => CurrentModel::className()]);
/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\News */
$this->title = 'Таблица';
$this->params['breadcrumbs'][] = ['label' => 'Сторінки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';

\backend\widgets\SortActionWidget::widget(['className' => ModelData::className()]);
?>

<div class="page-update">

    <?= $this->render('_submenu', [
        'model' => $model
    ]) ?>

    <h1>
        <?= Html::encode($this->title) ?>
        <a href="<?= Url::to(['/table/table/create', 'page_id' => $model->id]); ?>" class="btn btn-primary block right">
            Добавить запись
        </a>
    </h1>
    <?php Pjax::begin(['id' => 'content-list']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
            ],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'type_id',
                'filter' => ArrayHelper::map(TableType::find()->joinWith('lang')->all(), 'id', 'title'),
                'format' => 'raw',
                'value' => function ($model) {
                    $type = $model->tableType;
                    return $type ? $type->title : '';
                }
            ],
//            'cut',
//            'mark',
//            'standard',
            [
                'attribute' => 'firm_id',
                'filter' => ArrayHelper::map(Firm::find()->joinWith('lang')->all(), 'id', 'title'),
                'format' => 'raw',
                'value' => function ($model) {
                    $firm = $model->firm;
                    return $firm ? $firm->title : '';
                }
            ],
            [
                'attribute' => 'page_id',
                'filter' => ArrayHelper::map(Page::find()->joinWith('lang')->all(), 'id', 'title'),
                'format' => 'raw',
                'value' => function ($model) {
                    $page = $model->page;
                    return $page ? $page->title : '';
                }
            ],
            [
                'attribute' => 'status',
                'filter' => $searchModel::getStatusList(),
                'value' => 'statusDetail'
            ],
            'updated_at:date',

            [
                'contentOptions'=>['style'=>'width: 120px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['/table/table/view', 'id' => $model->primaryKey, 'page_id' => $model->id],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            ['/table/table/update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['/table/table/delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>
<?php Pjax::end() ?>
</div>
