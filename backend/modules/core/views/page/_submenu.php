<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;
?>
<div class="row">
    <div class="col-md-9">
        <ul class="nav nav-tabs">
            <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
                <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
                    Редагування
                </a>
            </li>
            <li <?= ($action === 'seo') ? 'class="active"' : '' ?>>
                <a href="<?= Url::to(['seo', 'id' => $model->primaryKey]) ?>">
                    Редактирование SEO
                </a>
            </li>

        </ul>
    </div>
    <div class="col-md-3" style="text-align: right">
        <?= Html::submitButton('Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'onClick' =>'validForm()' ]) ?>
        <?php if (!empty($model->alias)):?>
<!--            <a href="/--><?//=\common\models\Lang::getCurrent()->url.'/'.$model->alias?><!--" target="_blank"><input type="button" value="Перейти" class="btn btn-success"></a>-->
            <a href="/<?=$model->alias?>" target="_blank"><input type="button" value="Перейти" class="btn btn-success"></a>
        <?php endif;?>
    </div>
</div>
<br>



