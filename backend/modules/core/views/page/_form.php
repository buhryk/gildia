<?php

use backend\modules\core\models\Setting;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\core\models\PageCategory;
use yii\helpers\Url;
use backend\assets\FrontendAsset;
use backend\assets\RedactorAsset;
use backend\widgets\ImperaviWidget;

FrontendAsset::register($this);
RedactorAsset::register($this);

?>

<?php $form = ActiveForm::begin(['id'=>'form']); ?>

<div class="page-form">
    <?= $form->field($model, 'title')->textInput() ?>

<!--    --><?//= $form->field($model, 'additional_title')->textInput() ?>

    <?= $form->field($model, 'description')->widget(ImperaviWidget::className(), []) ?>

<!--    --><?//= $form->field($model, 'text')->widget(ImperaviWidget::className(), []) ?>

    <div class="row">
<!--        <div class="col-md-6">-->
<!--            --><?//= $form->field($model, 'category_id')->dropDownList(PageCategory::getCategoryAll(true), ['prompt' => '']) ?>
<!--        </div>-->
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'image')->widget(\backend\widgets\MainInputFile::className(), [
                'language'      => 'ru',
                'path'          => 'page/images',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ])->label(false); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script language="JavaScript">
    function validForm() {
        form.submit() // Отправляем на сервер
    }
</script>