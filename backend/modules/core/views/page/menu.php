<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\core\models\Menu;
use kartik\select2\Select2;
use backend\modules\core\models\Page;

/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\News */

$this->title = 'Редагування: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Сторінки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';

$parentsList = Menu::find()
    ->where(['status' => Menu::STATUS_ACTIVE])
    ->andFilterWhere(['!=', 'id', $menuModel->id])
    ->orderBy(['position' => SORT_ASC])
    ->all();
?>
<div class="page-update">

    <?= $this->render('_submenu', [
        'model' => $model
    ]) ?>
    <div class="menu-form">
        <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($menuModel, 'name')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?=  $form->field($menuModel, 'page_id')->widget(Select2::classname(), [
                        'data' => Page::getPageAll(true),
                        'language' => 'de',
                        'options' => ['placeholder' => 'Select a state ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 item">
                    <?= $form->field($menuModel, 'absolute_url')->checkbox() ?>
                    <div class="<?= $menuModel->absolute_url ? '' : 'hidden' ?>" id="block-url">
                        <?= $form->field($menuModel, 'url')->textInput() ?>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?=  $form->field($menuModel, 'parent')->widget(Select2::classname(), [
                        'data' => Menu::getMenuAll(true, null, $menuModel->id),
                        'language' => 'ru',
                        'options' => ['placeholder' => 'Select a state ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($menuModel, 'status')->dropDownList($menuModel::getStatusList()) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($menuModel, 'type')->dropDownList($menuModel::getTypeList()) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($menuModel, 'show_children')->checkbox() ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($menuModel->isNewRecord ? 'Зберегти' : 'Редагувати', ['class' => $menuModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

<?=$this->registerJs("
    $('#menu-absolute_url').on('click', function() {
        if ($(this).prop('checked')) {
           $('#block-url').removeClass('hidden');
        } else {
            $('#block-url').addClass('hidden');
        }
    })
", \yii\web\View::POS_READY) ?>
