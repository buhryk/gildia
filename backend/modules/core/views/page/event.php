<?php

use backend\assets\PageAsset;
use backend\modules\core\models\Page as CurrentModel;
use backend\modules\event\models\Event;
use backend\modules\faq\models\Faq;
use backend\modules\section\models\SectionItem;
use backend\widgets\ImperaviWidget;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;
use backend\modules\core\models\Page;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

\backend\widgets\SortWidget::widget(['className' => CurrentModel::className()]);
/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\News */
$this->title = Yii::t('event', 'Events list');
$this->params['breadcrumbs'][] = ['label' => 'Сторінки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';


?>

<div class="page-update">

    <?= $this->render('_submenu', [
        'model' => $model
    ]) ?>

    <h1>
        <?= Html::encode($this->title) ?>
        <a href="<?= Url::to(['/event/event/create', 'page_id' => $model->id]); ?>" class="btn btn-primary block right">
            <?= Yii::t('event', 'Add event'); ?>
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'title',
                'label' => Yii::t('event', 'Title'),
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['event/view', 'id' => $model->primaryKey]);
                },
            ],

//            'alias',
            [
                'attribute' => 'start_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],

            [
                'attribute' => 'active',
                'filter' => Event::getAllActiveProperties(),
                'value' => 'activeDetail'
            ],
            [
                'contentOptions'=>['style'=>'width: 120px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['/event/event/view', 'id' => $model->primaryKey, 'page_id' => $model->id],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['/event/event/update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['/event/event/delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>

</div>
