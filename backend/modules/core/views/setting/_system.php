<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="setting-system">
    <div class="row">
        <?php if ($models['emails']): ?>
            <div class="form-group">
                <label for="input02" class="col-sm-4 control-label">E-mail</label>
                <div class="col-sm-4">
                    <?=$form->field($models['emails'], '[emails]value')->textarea()->label(false); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>



