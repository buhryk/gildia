<?php
use backend\modules\core\models\Setting;
use yii\helpers\Html;
use dominus77\iconpicker\IconPicker;

$model = current($models);

$items = $model->getData(Setting::ADDITIONAL_DATA_ITEMS) ?: [];
$addItem = Yii::$app->request->get('add-items');
$key = 0;
?>
<div class="col-md-12">
    <?php foreach ($items as $key => $item) : ?>
        <?= $this->render('form/_filter_count_user', ['key' => $key, 'item' => $item]); ?>
    <?php endforeach; ?>
    <?php if ($addItem) : ?>
        <?php $key = $key + 1; ?>
        <?= $this->render('form/_filter_count_user', ['key' => $key, 'item' => '']); ?>
    <?php else: ?>
        <hr>
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Add item',
            ['index', 'group' => $model->group, 'add-items' => 'colum'],
            ['class' => 'btn btn-success btn-sm']) ?>
    <?php endif; ?>
    <hr>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
