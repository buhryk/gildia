<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<h3>Confirm usual emails </h3>
<div class="row">
    <?php if ($models['emails_normal']): ?>
        <div class="form-group">
            <label for="input02" class="col-sm-1 control-label">Emails</label>
            <div class="col-sm-7">
                <?= $form->field($models['emails_normal'], '[emails_normal]value')->textInput()->label(false); ?>
            </div>
        </div>
    <?php endif; ?>
</div>


<div class="form-group">
    <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Save', ['class' => 'btn btn-success']) ?>
</div>