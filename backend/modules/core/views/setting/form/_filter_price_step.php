<?php
use yii\helpers\Html;
use backend\modules\core\models\Setting;


//$items = Setting::getDataByKey(Setting::FILTER_TIME_ITEMS, Setting::ADDITIONAL_DATA_ITEMS);
?>

<div class="row item-setting">

    <div class="col-md-3">
        <label>Min</label>
        <?= Html::textInput('setting[' . $key . '][min]', $item['min'] ?? '', ['class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="col-md-3">
        <label>Max</label>
        <?= Html::textInput('setting[' . $key . '][max]', $item['max'] ?? '', ['class' => 'form-control']) ?>
    </div>
    <div class="col-md-1">
        <a href="#" class="setting-delete-item" title="Delete" ><i class="glyphicon glyphicon-trash"></i> </a>
    </div>

</div>
