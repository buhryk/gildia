<?php
use yii\helpers\Html;

?>

<div class="row item-setting">
    <div class="col-md-3">
        <label>Title</label>
        <?= Html::textInput('setting[' . $key . '][title]', $item['title'] ?? '', ['class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="col-md-3">
        <label>Key</label>
        <?= Html::textInput('setting[' . $key . '][key]', $item['key'] ?? '', ['class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="col-md-5">
        <label>Time interval</label>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3">
                        From
                    </label>
                    <div class="col-md-9">
                        <?= Html::input('time','setting[' . $key . '][from_time]', $item['from_time'] ?? '',
                            ['class' => 'form-control', 'placeholder' => 'From']) ?>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3">
                        To
                    </label>
                    <div class="col-md-9">
                        <?= Html::input('time','setting[' . $key . '][to_time]', $item['to_time'] ?? '',
                            ['class' => 'form-control',  'placeholder' => 'To']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-1">
        <a href="#" class="setting-delete-item" title="Delete" ><i class="glyphicon glyphicon-trash"></i> </a>
    </div>
</div>
