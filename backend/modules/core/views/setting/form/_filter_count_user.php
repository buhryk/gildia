<?php
use yii\helpers\Html;
?>
<div class="row item-setting">

    <div class="col-md-3">
        <label>Value</label>
        <?= Html::textInput('setting[' . $key . '][value]', $item['value'] ?? '', ['class' => 'form-control', 'required' => 'required']) ?>
</div>

<div class="col-md-1">
    <a href="#" class="setting-delete-item" title="Delete" ><i class="glyphicon glyphicon-trash"></i> </a>
</div>

</div>
