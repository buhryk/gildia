<?php

namespace backend\modules\core\models;

use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use common\models\Lang;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "page_category".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $alias
 * @property string $additional_data
 *
 * @property News[] $pages
 * @property NewsCategorLang[] $pageCategorLangs
 * @property Lang[] $langs
 */
class PageCategory extends BaseDataModel
{
    public $title;
    public $description;
    public $text;

    public static function tableName()
    {
        return 'page_category';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
            ],
            [
                'class' => LangBehavior::className(),
                't' => new PageCategorLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['alias'], 'string', 'max' => 255],
            [['parent_id'], 'integer'],
            [['title', 'description', 'text'], 'safe']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => 'Alias',
            'title' => 'Заголовок',
            'description' => 'Опис',
            'text' => 'Контент',
            'parent_id' => 'Батьківська категорія'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageCategorLangs()
    {
        return $this->hasMany(PageCategorLang::className(), ['record_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Lang::className(), ['id' => 'lang_id'])->viaTable('page_categor_lang', ['record_id' => 'id']);
    }

    public function getChildrens()
    {
        return $this->hasMany(PageCategory::className(), ['parent_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(PageCategory::className(), ['id' => 'parent_id']);
    }

    static public function getCategoryAll($map = false)
    {
        $query = self::find()->orderBy(['position' => SORT_ASC]);

        $models = $query->all();


        if ($map) {

            $arr = [];
            foreach ($models as $item) {
                $arr[$item->id] = $item->title;
                $arr = $arr+ self::findChildren($item, '-');
            }

            return $arr ;
        }
        return $models;
    }

    static public function findChildren($model, $prefix = ' -')
    {
        $items = [];
        foreach ($model->childrens as $item) {
            $items[$item->id] = $prefix . $item->title;
            $items = $items+ self::findChildren($item, $prefix . '-');
        }

        return $items;
    }
}
