<?php

namespace backend\modules\core\models;

use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use Yii;

/**
 * This is the model class for table "widget".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $key
 * @property string $title
 * @property string $additional_data
 *
 * @property WidgetLang[] $widgetLangs
 * @property Lang[] $langs
 */
class Widget extends BaseDataModel
{
    public $content;


    public static function tableName()
    {
        return 'widget';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => LangBehavior::className(),
                't' => new WidgetLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['key', 'string'],
            ['title', 'string'],
            [['content'], 'safe']
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'title' => 'Назва',
            'content' => 'Контент',
        ]);
    }

    public function getLang()
    {
        return $this->hasMany(WidgetLang::className(), ['record_id' => 'id']);
    }

    public static function getWidgetByKey($key)
    {
        $model = self::find()->andWhere(['key'=> trim($key)])->andWhere(['status' => self::STATUS_ACTIVE])->one();

        return $model ? $model->content : '';
    }
}
