<?php

namespace backend\modules\core\models;

use Yii;
use common\models\Lang;

/**
 * This is the model class for table "widget_lang".
 *
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $content
 *
 * @property Lang $lang
 * @property Widget $record
 */
class WidgetLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['content'], 'string'],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Widget::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'content' => 'Content',
        ];
    }
}
