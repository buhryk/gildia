<?php

namespace backend\modules\core\controllers;

use backend\controllers\BaseController;
use backend\modules\core\models\Setting;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;


class SettingController extends BaseController
{

    public function actionIndex($group = Setting::COMMON)
    {
        $models = Setting::find()->indexBy('key')->andWhere(['group' => $group])->all();
        $groupItem = Setting::getGroupItem();


        if (Model::loadMultiple($models, Yii::$app->request->post()) && Model::validateMultiple($models)) {
            foreach ($models as $item) {
                $item->save(false);
            }
            Yii::$app->session->setFlash('success', 'Changes saved.');
            return $this->refresh();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->post('setting')) {
            $model = current($models);

            $data = Yii::$app->request->post('setting');
            $new_data = [];

            foreach ($data as $k => $item){
                if (!isset($item['delete'])){
                    $new_data[$k] = $item;
                }
            }
            $model->setData($model::ADDITIONAL_DATA_ITEMS, $new_data);

            $model->save();

            Yii::$app->session->setFlash('success', 'Changes saved.');
            return $this->redirect(['index', 'group' => $model->group]);
        }

        $template = '_' . $group;

        return $this->render('index', [
            'models' => $models,
            'groupItem' => $groupItem,
            'group' => $group,
            'template' => $template
        ]);
    }

    public function actionCreate()
    {
        $count = count(Yii::$app->request->post('Setting', []));
        $settings = [new Setting()];
        for($i = 1; $i < $count; $i++) {
            $settings[] = new Setting();
        }
    }

    public function actionDelete($id)
    {
        $count = count(Yii::$app->request->post('Setting', []));
        $settings = [new Setting()];
        for($i = 1; $i < $count; $i++) {
            $settings[] = new Setting();
        }
    }

    protected function findModel($id)
    {
        if (($model = Setting::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
