<?php

namespace backend\modules\slider\models;

use backend\modules\slider\models\SliderItem;
use common\models\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property integer $created
 * @property integer $updated
 * @property string $name
 */
class Slider extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    public function behaviors()
    {
        return [
           TimestampBehavior::className()
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Створено',
            'updated_at' => 'Редаговано',
            'name' => 'Назва',
            'status' => 'Статус',
        ];
    }
    /**
     * Получение списка статусы
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_NOTACTIVE => 'Неактивный'
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }

    public function getItems()
    {
        return $this->hasMany(SliderItem::className(), ['slider_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id])
            ->orderBy('position ASC');
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public static function getSlider($id)
    {
        $slider = SliderItem::find()
            ->innerJoin('slider', 'slider.id = slider_id')
            ->andWhere(['slider_id' => $id])
            ->andWhere(['slider.status' => self::STATUS_ACTIVE])
            ->andWhere(['lang_id' => Lang::getCurrent()->id])
            ->orderBy('position')
            ->all();

        return $slider;
    }
    static public function getSliderAll($map = false)
    {
        $query = self::find()->where(['status' => self::STATUS_ACTIVE]);


        $models = $query->all();
        if ($map) {
            return [false => 'Выберите слайдер'] + ArrayHelper::map($models, 'id', 'name');
        }
//        pr(ArrayHelper::map($models, 'id', 'name'));
        return $models;
    }
}
