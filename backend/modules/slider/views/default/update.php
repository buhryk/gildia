<?php

use backend\modules\core\models\Page;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="x_panel">
    <!-- tile body -->
    <div class="tile-body">
        <p class="pull-right">

            <?php if ($page_id): ?>
                <?= Html::a('Назад к "' . Page::getPageById($page_id)->title .'"', ['/core/page/widget', 'id' => $page_id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
        </p>
        <?php $form = ActiveForm::begin([ 'options' => ['class'=>'form-horizontal']]); ?>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'status')->checkbox()?>
            </div>
            <div class="col-md-12">
                <button class="btn btn-primary" type="submit"> Сохранить </button>
            </div>
        </div>
        <hr class="line-dashed line-full">

        <a class="btn btn-primary modalButton" href="<?=Url::to(['/slider/slider-item/create', 'slider_id'=>$model->id, 'page_id' => $page_id]) ?>">
            Добавить слайд
        </a>

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Картинка</th>
                <th>Название</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($model->items as $item): ?>
                <tr>
                    <td><img height="80px" src="<?=$item['image'] ?>"> </td>
                    <td><?=$item['title'] ?></td>
                    <td><a  class="btn btn-primary modalButton" title="Edit" href="<?=Url::to(['/slider/slider-item/update','id'=>$item->id, 'slider_id'=>$model->id, 'page_id' => $page_id]) ?>">
                            <i class="glyphicon glyphicon-pencil"></i> </a>
                        <a href="<?=Url::to(['/slider/slider-item/delete','id'=>$item->id, 'slider_id'=>$model->id]) ?>"
                           title="Delete"
                           aria-label="Delete"
                           data-confirm="Are you sure you want to delete this item?"
                           data-method="post"
                           data-pjax="0">
                           <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?php ActiveForm::end()  ?>

    </div>
    <!-- /tile body -->

</div>