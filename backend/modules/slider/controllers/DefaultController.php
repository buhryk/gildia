<?php
namespace backend\modules\slider\controllers;

use backend\controllers\BaseController;
use backend\modules\slider\models\SliderItem;
use backend\modules\slider\models\Slider;
use yii\base\Exception;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Default controller for the `slider` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($page_id = false)
    {
        $query = Slider::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'page_id' => $page_id,
        ]);
    }

    public function actionCreate($content_id = null, $page_id = false)
    {
        $model = new Slider();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id, 'page_id' => $page_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'page_id' => $page_id,
            ]);
        }
    }

    public function actionUpdate($id, $page_id = false)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'page_id' => $page_id,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/slider/']);
    }

    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
