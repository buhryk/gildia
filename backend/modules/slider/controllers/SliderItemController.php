<?php

namespace backend\modules\slider\controllers;

use backend\controllers\BaseController;
use common\models\Lang;
use Yii;
use backend\modules\slider\models\SliderItem;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SliderItemController implements the CRUD actions for SliderItem model.
 */
class SliderItemController extends BaseController
{
    /**
     * Lists all SliderItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SliderItem::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SliderItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SliderItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($slider_id, $page_id = false)
    {
        $model = new SliderItem();
        $model->slider_id = $slider_id;
        $model->lang_id = Lang::getCurrent()->id;
        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                return $this->redirect(['/slider/default/update', 'id' => $slider_id, 'page_id' => $page_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SliderItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$slider_id, $page_id = false)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/slider/default/update', 'id' => $slider_id, 'page_id' => $page_id]);
        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SliderItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$slider_id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/slider/default/update', 'id' => $slider_id]);
    }

    /**
     * Finds the SliderItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SliderItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SliderItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
