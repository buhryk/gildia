<?php

namespace backend\modules\news\models;

use Yii;

/**
 * This is the model class for table "catalog_propery".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $catalog_id
 * @property integer $property_data_id
 * @property string $slug
 *
 * @property catalog $catalog
 * @property PropertyData $propertyData
 * @property Property $property
 */
class NewsToNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_to_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['related_id', 'news_id'], 'required'],
            [['related_id', 'news_id'], 'integer'],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'catalog_id' => 'catalogSoap ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(News::className(), ['id' => 'related_id']);
    }

}
