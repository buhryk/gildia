<?php

namespace backend\modules\news\models;

use Yii;

/**
 * This is the model class for table "catalog_propery".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $catalog_id
 * @property integer $property_data_id
 * @property string $slug
 *
 * @property catalog $catalog
 * @property PropertyData $propertyData
 * @property Property $property
 */
class NewsToCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_to_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'news_id'], 'required'],
            [['category_id', 'news_id'], 'integer'],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'catalog_id' => 'catalogSoap ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }
}
