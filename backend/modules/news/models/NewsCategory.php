<?php

namespace backend\modules\news\models;

use backend\modules\core\models\Lang;
use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "page_category".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $alias
 * @property string $additional_data
 *
 * @property News[] $pages
 * @property NewsCategorLang[] $pageCategorLangs
 * @property Lang[] $langs
 */
class NewsCategory extends BaseDataModel
{
    public $title;
    public $description;
    public $text;

    public static function tableName()
    {
        return 'news_category';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
            [
                'class' => LangBehavior::className(),
                't' => new NewsCategorLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['alias'], 'string', 'max' => 255],
            [['parent_id'], 'integer'],
            [['title', 'description', 'text'], 'safe']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => 'Alias',
            'title' => 'Заголовок',
            'description' => 'Опис',
            'text' => 'Контент',
            'parent_id' => 'Батьківська категорія'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsCategorLangs()
    {
        return $this->hasMany(NewsCategorLang::className(), ['record_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Lang::className(), ['id' => 'lang_id'])->viaTable('news_categor_lang', ['record_id' => 'id']);
    }

    public function getChildrens()
    {
        return $this->hasMany(NewsCategory::className(), ['parent_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'parent_id']);
    }


    static public function getCategoryAll($map = false, $parent_element = null, $not_equal_id = null)
    {
        $query = self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy(['position' => SORT_ASC]);

        if ($map) {

            $models = $query->all();

            $arr = [];
            foreach ($models as $k => $item) {

                if (!isset($arr[$item->id])) {
                    $arr[$item->id] =  $item->title;
                    if (!empty($item->childrens)){
                        $arr = $arr+ self::findChildren($item, " - - ");
                    }
                }
            }

            return [''] + $arr ;
        }
        return $query->all();
    }

    static public function findChildren($model, $prefix = "-", $not_equal_id = null)
    {
        $items = [];
        foreach ($model->childrens as $k => $item) {
            if (!isset($items[$item->id])) {
                $items[$item->id] = $prefix . $item->title;
                if (!empty($item->childrens)) {
                    $items = $items + self::findChildren($item, $prefix . " - - ");
                }
            }
        }

        return $items;
    }
}
