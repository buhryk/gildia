<?php
use backend\widgets\ImagesWidget;

$this->title = 'Редагування зображень';
$this->params['breadcrumbs'][] = ['label' => 'Новина', 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => 'Новина #' . $model->primaryKey, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= ImagesWidget::widget([
        'model' => $model,
        'parameters' => [
            'table_name' => $model::tableName()
        ]
    ]); ?>
</div>