<?php
//use vova07\imperavi\Widget as ImperaviWidget;
use backend\assets\FrontendAsset;
use backend\assets\RedactorAsset;
use backend\modules\catalog\models\CatalogCategory;
use backend\modules\core\models\Page;
use backend\modules\news\models\News;
use backend\widgets\ImperaviWidget;
use backend\widgets\MainInputFile;
use frontend\models\User;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\modules\news\models\NewsCategory;
use mihaildev\elfinder\InputFile;
/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\News */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="page-category-form">

    <?php $form = ActiveForm::begin(['id'=>'form']); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description')->widget(ImperaviWidget::className(), []) ?>

    <?= $form->field($model, 'text')->widget(ImperaviWidget::className(), [])?>

    <div class="row">
        <div class="col-md-4">
            <label class="control-label" >Категории</label>
            <?= Select2::widget([
                'name' => 'category_id',
                'value' => ArrayHelper::getColumn($model->categories, 'category_id'),
                'data' => NewsCategory::getCategoryAll(true),
                'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
            ]); ?>
        </div>

        <div class="col-md-4">
            <label class="control-label" >Рекомендуемые новости</label>
            <?= Select2::widget([
                'name' => 'related_news',
                'value' => ArrayHelper::getColumn($model->news, 'related_id'),
                'data' => News::getCategoryAll(true),
                'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
            ]); ?>
        </div>

<!--        <div class="col-md-4">-->
<!--            --><?//= $form->field($model, 'type')->dropDownList($model::getTypeList()) ?>
<!--        </div>-->
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?php
            if($model->data_pub) {
                $model->created_at = date("yyyy-mm-dd hh:ii", $model->data_pub);
            }
            echo $form->field($model, 'data_pub')->widget(DateTimePicker::className(),[
                'options' => [],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii'
                ]
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'image')->widget(MainInputFile::className(), [
                'language'      => 'ru',
                'path'          => 'news',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ])->label(false); ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script language="JavaScript">
    function validForm() {
        form.submit() // Отправляем на сервер
    }
</script>

