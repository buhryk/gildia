<?php

namespace backend\modules\catalog\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "page_lang".
 *
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 * @property string $text
 */
class CatalogLang extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'catalog_lang';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['description', 'text', 'text_3', 'text_4', 'text_5', 'first_text'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
        ];
    }

    public function getRecord()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'record_id']);
    }
}
