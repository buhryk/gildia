<?php

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseController;
use backend\modules\catalog\models\CatalogCategory;
use backend\modules\catalog\models\CatalogPropery;
use backend\modules\catalog\models\CatalogToCategory;
use backend\modules\catalog\models\ProductPropery;
use backend\modules\catalog\models\PropertyBilder;
use backend\modules\catalog\models\PropertyData;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;
use common\models\Lang;
use Yii;
use backend\modules\catalog\models\Catalog;
use backend\modules\catalog\models\CatalogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class CatalogController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CatalogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Catalog();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $category_ids = Yii::$app->request->post('category_id');
            CatalogToCategory::deleteAll(['catalog_id' => $model->id]);
            foreach ($category_ids as $item) {
                $catalog_to_category = new CatalogToCategory();
                $catalog_to_category->catalog_id = $model->id;
                $catalog_to_category->category_id = $item;
                $catalog_to_category->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $category_ids = Yii::$app->request->post('category_id');
            CatalogToCategory::deleteAll(['catalog_id' => $model->id]);
            foreach ($category_ids as $item) {
                $catalog_to_category = new CatalogToCategory();
                $catalog_to_category->catalog_id = $model->id;
                $catalog_to_category->category_id = $item;
                $catalog_to_category->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return catalog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Catalog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionProperty($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->post()) {
            $propertys = Yii::$app->request->post('property');
            CatalogPropery::deleteAll(['catalog_id' => $model->id]);
            foreach ($propertys as $item) {
                $propertyData = PropertyData::findOne($item);
                $propertyBilder = new PropertyBilder(['catalog' => $model, 'propertyData' => $propertyData]);
                $propertyBilder->setProperty();
            }

            return $this->redirect(['property', 'id' => $id]);
        }

        return $this->render('property', ['model' => $model]);

    }

    public function actionImages($id)
    {
        $model = $this->findModel($id);

        $image = new Image();
        $imageLang = new ImageLang();
        $imageLang->lang_id = Lang::getCurrent()->id;

        return $this->render('images', [
            'model' => $model,
            'image' => $image,
            'imageLang' => $imageLang
        ]);
    }
}
