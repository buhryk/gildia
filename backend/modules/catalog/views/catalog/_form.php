<?php
//use vova07\imperavi\Widget as ImperaviWidget;
use backend\assets\FrontendAsset;
use backend\assets\RedactorAsset;
use backend\modules\catalog\models\CatalogCategory;
use backend\modules\slider\models\Slider;
use backend\widgets\ImperaviWidget;
use backend\widgets\MainInputFile;
use frontend\models\User;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\modules\news\models\NewsCategory;
use mihaildev\elfinder\InputFile;
/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\News */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="page-category-form">

    <?php $form = ActiveForm::begin(['id'=>'form']); ?>

    <?= $form->field($model, 'title')->textarea() ?>

    <?= $form->field($model, 'first_text')->widget(ImperaviWidget::className(), [])?>

    <?= $form->field($model, 'description')->widget(ImperaviWidget::className(), []) ?>

    <?= $form->field($model, 'text')->widget(ImperaviWidget::className(), [])?>

    <label class="control-label" >Слайдер 1</label>
    <?= Select2::widget([
        'name' => 'Catalog[slider_1]',
        'value' => $model->slider_1,
        'data' => Slider::getSliderAll(true),
        'options' => [ 'placeholder' => 'Выберите нужный слайдер']
    ]); ?>

    <?= $form->field($model, 'text_3')->widget(ImperaviWidget::className(), [])?>

    <label class="control-label" >Слайдер 2</label>
    <?= Select2::widget([
        'name' => 'Catalog[slider_2]',
        'value' => $model->slider_2,
        'data' => Slider::getSliderAll(true),
        'options' => [ 'placeholder' => 'Выберите нужный слайдер']
    ]); ?>

    <label class="control-label" >Слайдер 3</label>
    <?= Select2::widget([
        'name' => 'Catalog[slider_3]',
        'value' => $model->slider_3,
        'data' => Slider::getSliderAll(true),
        'options' => [ 'placeholder' => 'Выберите нужный слайдер']
    ]); ?>

    <?= $form->field($model, 'text_4')->widget(ImperaviWidget::className(), [])?>
    <?= $form->field($model, 'text_5')->widget(ImperaviWidget::className(), [])?>

    <div class="row">
        <div class="col-md-4">
            <label class="control-label" >Категории</label>
            <?= Select2::widget([
                'name' => 'category_id',
                'value' => ArrayHelper::getColumn($model->categories, 'category_id'),
                'data' => CatalogCategory::getCategoryAll(true),
                'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
            ]); ?>

        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'position')->textInput() ?>
        </div>

        <div class="col-md-6">
            <label class="control-label" >Картинка 1</label>
            <?= $form->field($model, 'image')->widget(MainInputFile::className(), [
                'language'      => 'ru',
                'path'          => 'catalog',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ])->label(false); ?>
        </div>
        <div class="col-md-6">
            <label class="control-label" >Картинка 2</label>
            <?= $form->field($model, 'image_2')->widget(MainInputFile::className(), [
                'language'      => 'ru',
                'path'          => 'catalog',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ])->label(false); ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script language="JavaScript">
    function validForm() {
        form.submit() // Отправляем на сервер
    }
</script>

