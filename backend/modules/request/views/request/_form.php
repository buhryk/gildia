<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\request\models\Request;

/* @var $this yii\web\View */
/* @var $model backend\modules\request\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-call-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'type')->dropDownList(Request::getAllTypes()) ?>
    <?= $form->field($model, 'text')->textarea(['rows' => 2]) ?>
    <?= $form->field($model, 'status')->dropDownList(Request::getAllStatuses()) ?>
    <?= $form->field($model, 'admin_note')->textarea(['rows' => 2]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
