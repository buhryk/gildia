<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\request\models\Request;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\request\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Формы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-call-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {
            switch ($model->status) {
                case Request::STATUS_NEW:
                    return ['class' => 'danger'];
                case Request::STATUS_PROCESSED:
                    return ['class' => 'success'];
                case Request::STATUS_VIEWED:
                    return ['class' => 'info'];
                default:
                    return ['class' => 'active'];
            }
        },
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['style'=>'width: 60px;']
            ],
            'name',
            'phone',
            'email:email',
            'text:ntext',
            'form_title',
            [
                'attribute' => 'status',
                'filter' => Request::getAllStatuses(),
                'value' => 'statusDetail'
            ],
            // 'admin_note:ntext',
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'contentOptions' => ['style'=>'width: 110px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . ' ' .
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            [
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")',
                                'data-method' => 'post'
                            ]
                        );
                }
            ],
        ],
    ]); ?>
</div>
