<?php
use backend\modules\request\models\Request;

/* @var $model Request */
/* @var $user backend\models\User */
?>

<table>
    <tr>
        <td>
            <p>
                <b>Форма:</b> <?= $model->form_title; ?><br>
                <b>Имя:</b> <?= $model->name; ?><br>
                <b>Телефон:</b> <?= $model->phone; ?><br>
                <b>Email:</b> <?= $model->email; ?><br>
                <b>Сообщение:</b> <?= $model->text; ?>
            </p>
        </td>
    </tr>
</table>