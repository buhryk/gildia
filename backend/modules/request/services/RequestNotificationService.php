<?php

namespace backend\modules\request\services;

use backend\modules\core\models\Setting;
use backend\modules\request\models\Request;
use Yii;
use backend\models\User;

class RequestNotificationService
{
    public function notifyAboutNewRequest(Request $request)
    {
//        $users = User::find()->where(['notification_of_request' => User::YES])->all();

        if ($request) {
//            foreach ($users as $user) {
                Yii::$app->mailer->compose('@backend/modules/request/views/mails/new-request-notification',
                    ['model' => $request])
                    ->setFrom([$request->email => Yii::$app->name])
                    ->setTo(explode(',',Setting::getValueByKey('emails')))
                    ->setSubject('Отправка формы с сайта')
                    ->send();
//            }
        }
    }
}