<?php

namespace backend\modules\faq\models;

use Yii;

/**
 * This is the model class for table "catalog_propery".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $catalog_id
 * @property integer $property_data_id
 * @property string $slug
 */
class FaqToCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_to_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'faq_id'], 'required'],
            [['category_id', 'faq_id'], 'integer'],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'catalog_id' => 'catalogSoap ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaq()
    {
        return $this->hasOne(Faq::className(), ['id' => 'faq_id']);
    }
    public function getFaqs()
    {
        return $this->hasMany(Faq::className(), ['id' => 'faq_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FaqCategory::className(), ['id' => 'category_id']);
    }
}
