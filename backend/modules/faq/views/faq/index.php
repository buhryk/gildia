<?php

use backend\modules\core\models\Page;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\faq\models\Faq as ModelData;

use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\faq\models\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Faqs');
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => ModelData::className()]);
?>
<div class="faq-index">

    <?php Pjax::begin(['id' => 'content-list']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <span>
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i>' .Yii::t('app', ' Додати'), ['create'], ['class' => 'btn btn-success']) ?>
    </span>

    <div class="pull-right">
        <?= \backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => ModelData::className()]) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
            ],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'page_id',
                'filter' => ArrayHelper::map(Page::find()->joinWith('lang')->all(), 'id', 'title'),
                'format' => 'raw',
                'value' => function ($model) {
                    $page = $model->page;
                    return $page ? $page->title : '';
                }
            ],
            [
                'attribute' => 'status',
                'filter' => $searchModel::getStatusList(),
                'value' => 'statusDetail'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
