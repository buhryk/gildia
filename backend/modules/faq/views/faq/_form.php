<?php

use backend\modules\catalog\models\CatalogCategory;
use backend\modules\core\models\Page;
use backend\modules\faq\models\FaqCategory;
use backend\widgets\ImperaviWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\faq\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'text')->widget(ImperaviWidget::className(), []) ?>

    <label class="control-label" >Категории</label>
    <?= Select2::widget([
        'name' => 'category_id',
        'value' => ArrayHelper::getColumn($model->categories, 'category_id'),
        'data' => FaqCategory::getCategoryAll(true),
        'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
    ]); ?>

    <?=  $form->field($model, 'page_id')->widget(Select2::classname(), [
        'data' => \backend\modules\catalog\models\Catalog::getCategoryAll(true),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>


    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> '.Yii::t('app', 'Зберегти'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
