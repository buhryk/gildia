<?php
namespace backend\modules\adminuser\controllers;

use backend\models\User;
use backend\modules\adminuser\models\ChangePasswordForm;
use Yii;
use yii\web\Controller;
use backend\modules\accesscontrol\AccessControlFilter;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionView()
    {
        return $this->render('view', [
            'model' => Yii::$app->user->identity,
        ]);
    }

    public function actionUpdate()
    {
        $model = Yii::$app->user->identity;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionChangePassword()
    {
        $user = Yii::$app->user->identity;
        $model = new ChangePasswordForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->validateOldPassword($user, $model->old_password)) {
                $model->changePassword($user);
                \Yii::$app->getSession()->setFlash('success', 'Ваш пароль успешно изменен');
                return $this->refresh();
            } else {
                \Yii::$app->getSession()->setFlash('error', 'Произошла ошибка. Попробуйте, пожалуйста, повторить действия после перезагрузки страницы');
            }
        }

        return $this->render('change_password', [
            'model' => $model,
            'user' => $user
        ]);
    }
}