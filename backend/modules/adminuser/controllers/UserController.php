<?php
namespace backend\modules\adminuser\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\accesscontrol\models\Role;
use backend\models\User;
use backend\modules\adminuser\models\ChangePasswordForm;
use backend\modules\adminuser\models\UserSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new User();
        $model->scenario = User::SCENARIO_CREATE;

        $roleId = Yii::$app->request->get('role');
        if ($roleId) {
            $role = Role::findOne($roleId);
            if (!$role) {
                throw new NotFoundHttpException('Role not found');
            }

            $model->role_id = $role->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setPassword($model->password);
            $model->generateAuthKey();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChangePassword($id)
    {
        $user = $this->findModel($id);
        $model = new ChangePasswordForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->validateOldPassword($user, $model->old_password)) {
                $model->changePassword($user);
                \Yii::$app->getSession()->setFlash('success', 'Ваш пароль успешно изменен');
                return $this->refresh();
            } else {
                \Yii::$app->getSession()->setFlash('error', 'Произошла ошибка. Попробуйте, пожалуйста, повторить действия после перезагрузки страницы');
            }
        }

        return $this->render('change_password', [
            'model' => $model,
            'user' => $user
        ]);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}