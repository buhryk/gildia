<?php

use backend\modules\core\models\Page;
use backend\widgets\ImperaviWidget;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use common\models\Lang;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\SliderItem */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="modal-dialog" role="document">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel"></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <?php $form = ActiveForm::begin(['options'=>['class'=>'form-horizontal', 'id'=>'form-slider']]); ?>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Название</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'title')->textarea()->label(false) ?>
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Текст</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'text')->widget(ImperaviWidget::className(), [])->label(false) ?>
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Адрес</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'url')->textInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Название кнопки</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'btn_name')->textInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Картинка</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'image')->widget(InputFile::className(), [
                        'language'      => 'ru',
                        'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                        // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                        'options'       => ['class' => 'form-control'],
                        'path'          => 'section',
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'multiple'      => false       // возможность выбора нескольких файлов
                    ])->label(false); ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-0 col-sm-10">
                    <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>

                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>