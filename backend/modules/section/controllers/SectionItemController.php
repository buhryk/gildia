<?php

namespace backend\modules\section\controllers;

use backend\controllers\BaseController;
use backend\modules\catalog\models\CatalogSearch;
use backend\modules\section\models\SectionItemSearch;
use common\models\Lang;
use Yii;
use backend\modules\section\models\SectionItem;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * sectionItemController implements the CRUD actions for sectionItem model.
 */
class SectionItemController extends BaseController
{
    /**
     * Lists all sectionItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SectionItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider = new ActiveDataProvider([
//            'query' => SectionItem::find(),
//        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single sectionItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new sectionItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($section_id = false, $widget_id = false, $key = false)
    {
        $model = new SectionItem();

        if ($section_id) {
            $model->section_id = $section_id;
        }
        if ($widget_id) {
            $model->widget_id = $widget_id;
        }

        if ($model->load(Yii::$app->request->post())) {

            if ($forms = Yii::$app->request->post('SectionItem')) {
                if (isset($forms['forms'])){
                    $model->forms = json_encode($forms['forms']);
                }
            }
//            pr($model);
            if ($model->save()){
                if (!is_null($widget_id) && $widget_id) {
                    return $this->redirect(['/core/page/widget', 'id' => $section_id, 'section_id' => $model->id, 'key' => $key]);
                } else {
                    return $this->redirect(['/section/section-item/index']);
                }

            } else {
                return $this->render('create', [
                    'model' => $model,
                    'widget_id' => $widget_id,
                ]);
            }

        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
                'widget_id' => $widget_id,
            ]);
        }
    }

    /**
     * Updates an existing sectionItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$section_id = false, $widget_id = false)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($forms = Yii::$app->request->post('SectionItem')) {
                if (isset($forms['forms'])){
                    $model->forms = json_encode($forms['forms']);
                }
            }
            if ($model->save()) {
                if ($section_id) {
                    return $this->redirect(['/core/page/widget', 'id' => $section_id]);
                } else {
                    return $this->redirect(['/section/section-item/index']);
                }
            }
        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
                'widget_id' => $widget_id,
            ]);
        }
    }

    /**
     * Deletes an existing sectionItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/section/section-item/index']);
    }

    /**
     * Finds the sectionItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return sectionItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SectionItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
