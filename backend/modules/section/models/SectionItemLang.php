<?php

namespace backend\modules\section\models;

use backend\modules\core\models\Page;
use backend\modules\core\models\PageLang;
use backend\modules\transliterator\services\TransliteratorService;
use common\behaviors\LangBehavior;
use common\models\Lang;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "section_item".
 *
 * @property integer $id
 * @property integer $section_id
 * @property integer $lang_id
 * @property string $image
 * @property string $title
 * @property string $description
 * @property integer $created
 * @property integer $updated
 * @property integer $position
 */
class SectionItemLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'section_item_lang';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'lang_id'], 'required'],
            [[ 'lang_id'], 'integer'],
            [['description'], 'string'],
            [['title', 'text', 'btn_name'], 'string'],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => SectionItem::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */


    public function getRecord()
    {
        return $this->hasOne(SectionItem::className(), ['id' => 'record_id']);
    }
}
