<?php

namespace backend\modules\photo_gallery\models;

use backend\modules\images\models\Image;
use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image
 * @property string $alias
 * @property integer $category_id
 * @property string $additional_data
 */
class Photo extends BaseDataModel
{
    public $title;
    public $description;

    public static function tableName()
    {
        return 'photo';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
            [
                'class' => LangBehavior::className(),
                't' => new PhotoLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['data_pub'], 'required'],
            [['alias', 'image'], 'string', 'max' => 255],
            [['title', 'description'], 'safe']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => 'Alias',
            'title' => 'Заголовок',
            'description' => 'Опис',
            'data_pub' => 'Дата публікації'
        ]);
    }


    public function getPageLangs()
    {
        return $this->hasMany(PhotoLang::className(), ['record_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Photo::className(), ['id' => 'lang_id'])->viaTable('photo_lang', ['record_id' => 'id']);
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(), ['record_id' => 'id'])
            ->where([
                'table_name' => $this::tableName(),
               // 'is_main' => Image::IS_MAIN_YES
            ]);
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['record_id' => 'id'])
            ->where([
                'table_name' => $this::tableName(),
            ])
            ->orderBy('is_main DESC')
            ->addOrderBy('sort DESC');
    }
}
