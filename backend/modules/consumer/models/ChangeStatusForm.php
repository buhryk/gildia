<?php
namespace backend\modules\consumer\models;

use yii\base\Model;
use frontend\models\User;

/**
 * Signup form
 */
class ChangeStatusForm extends Model
{
    public $user_id;
    public $status;
    public $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true,
                'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['status', 'in', 'range' => array_keys(User::getAllStatuses())]
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function changeStatus()
    {
        if ($this->validate()) {
            $user = User::findOne($this->user_id);
            $user->status = $this->status;

            if ($user->save(false)) {
                return $user;
            }
        }

        return null;
    }
}