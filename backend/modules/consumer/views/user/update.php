<?php

use yii\helpers\Html;



$this->title = 'Редагування: ' . $model->firstname;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index', 'role' => ['index']]];
$this->params['breadcrumbs'][] = ['label' => $model->firstname, 'url' => ['view', 'id' => $model->id]];

$this->params['breadcrumbs'][] = 'Редагування';
?>
<div class="user-update">


    <?= $this->render('_form', [
        'model' => $model,
        'modelChange'=>$modelChange,
    ]) ?>

</div>
