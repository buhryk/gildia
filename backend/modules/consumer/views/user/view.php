<?php
use yii\helpers\Html;

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Список пользователей', 'url' => ['index']];;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="user-view">
    <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
    <hr>

    <div class="row">
        <div class="col-lg-6">
            <h3>Основная информация</h3>
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
                <tr>
                    <th width="200px">ID</th>
                    <td><?= $model->primaryKey; ?></td>
                </tr>


                <tr>
                    <th><?= $model->getAttributeLabel('name'); ?></th>
                    <td><?= $model->firstname; ?> <?= $model->lastname; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('email'); ?></th>
                    <td><?= $model->email; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('image'); ?></th>
                    <td><img src="<?= $model->image; ?>" style="height: 100px"></td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>
</div>