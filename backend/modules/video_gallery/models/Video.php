<?php

namespace backend\modules\video_gallery\models;

use backend\modules\images\models\Image;
use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image
 * @property string $alias
 * @property integer $category_id
 * @property string $additional_data
 */
class Video extends BaseDataModel
{
    public $title;
    public $description;

    public static function tableName()
    {
        return 'video';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
            [
                'class' => LangBehavior::className(),
                't' => new VideoLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['data_pub', 'video'], 'required'],
            [['alias', 'video'], 'string', 'max' => 255],
            [['title'], 'safe']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => 'Alias',
            'title' => 'Заголовок',
            'data_pub' => 'Дата публікації'
        ]);
    }


    public function getPageLangs()
    {
        return $this->hasMany(VideoLang::className(), ['record_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Video::className(), ['id' => 'lang_id'])->viaTable('video_lang', ['record_id' => 'id']);
    }
}
