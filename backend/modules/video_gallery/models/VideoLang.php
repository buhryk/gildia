<?php

namespace backend\modules\video_gallery\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "page_lang".
 *
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 * @property string $text
 */
class VideoLang extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'video_lang';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['title'], 'string', 'max' => 128],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Video::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
        ];
    }

    public function getRecord()
    {
        return $this->hasOne(Video::className(), ['id' => 'record_id']);
    }
}
