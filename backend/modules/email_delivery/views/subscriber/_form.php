<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\email_delivery\models\Subscriber;

/* @var $this yii\web\View */
/* @var $model backend\modules\email_delivery\models\Subscriber */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscriber-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'can_send')->dropDownList(Subscriber::getAllCanSendProperties()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
