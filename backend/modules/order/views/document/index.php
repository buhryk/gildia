<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\order\models\Document as CurrentModel;
use backend\widgets\SortActionWidget;

$this->title = 'Документы';
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => CurrentModel::className()]);
?>
<div class="page-index">
    <?php Pjax::begin(['id' => 'content-list']); ?>
    <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>

    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => CurrentModel::className()]) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-custom dataTable no-footer'],
        'class'=>'table table-custom dataTable no-footer',
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
            ],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->user->firstname;
                }
            ],

            'created_at:datetime',
            [
                'attribute' => 'status',
                'filter' => $searchModel::getStatusList(),
                'value' => 'statusDetail'
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
