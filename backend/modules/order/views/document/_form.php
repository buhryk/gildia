<?php
//use vova07\imperavi\Widget as ImperaviWidget;
use backend\assets\FrontendAsset;
use backend\assets\RedactorAsset;
use backend\modules\catalog\models\CatalogCategory;
use backend\modules\slider\models\Slider;
use backend\widgets\ImperaviWidget;
use backend\widgets\MainInputFile;
use frontend\models\User;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\modules\news\models\NewsCategory;
use mihaildev\elfinder\InputFile;
/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\News */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="page-category-form">

    <?php $form = ActiveForm::begin(['id'=>'form']); ?>


    <?= $form->field($model, 'name')->textInput() ?>

    <label class="control-label" >Пользователь</label>
    <?= Select2::widget([
        'name' => 'Document[user_id]',
        'value' => $model->user_id,
        'data' => User::getUserAll(true),
        'options' => [ 'placeholder' => 'Выберите нужного пользователя']
    ]); ?>

    <label class="control-label" >Файл</label>
    <?= $form->field($model, 'file')->widget(MainInputFile::className(), [
        'language'      => 'ru',
        'path'          => 'user',
        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
        // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'options'       => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple'      => false       // возможность выбора нескольких файлов
    ])->label(false); ?>


    <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script language="JavaScript">
    function validForm() {
        form.submit() // Отправляем на сервер
    }
</script>

