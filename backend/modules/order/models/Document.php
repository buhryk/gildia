<?php

namespace backend\modules\order\models;

use common\behaviors\PositionBehavior;
use common\models\BaseDataModel;
use frontend\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


class Document extends ActiveRecord
{
    const STATUS_NOTACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public static function tableName()
    {
        return 'documents';
    }

    public function behaviors()
    {
        return [
            PositionBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ ['file', 'name'], 'required'],
            [['position', 'status', 'created_at', 'updated_at', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь id',
            'name' => 'Название файла',
            'position' => 'Позиция',
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Редактирована',
        ];
    }

    static public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_NOTACTIVE => 'Не активный'
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
