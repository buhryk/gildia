<?php

namespace backend\modules\order\models;

use common\behaviors\PositionBehavior;
use common\models\BaseDataModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


class Order extends ActiveRecord
{
    const STATUS_PROCESS = 0;
    const STATUS_COMPLETED = 1;

    public static function tableName()
    {
        return 'orders';
    }

    public function behaviors()
    {
        return [
            PositionBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ ['phone', 'email'], 'required'],
            [ ['email'], 'email'],
            [ ['name', 'title', 'phone', 'email'], 'string'],
            [['position', 'status', 'created_at', 'updated_at', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь id',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'title' => 'Название формы',
            'position' => 'Позиция',
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Редактирована',
        ];
    }

    static public function getStatusList()
    {
        return [
            self::STATUS_PROCESS => 'В процесе',
            self::STATUS_COMPLETED => 'Завершено'
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }

}
