<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\Url;

$this->title = 'Користувачі';
$this->params['breadcrumbs'][] = $this->title;

$roleAll = User::getRoleAll();

?>
<div class="user-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="custom-tabs-line tabs-line-bottom left-aligned">
        <ul class="nav nav-tabs bar_tabs" role="tablist">
            <?php foreach ($roleAll as $key => $value):?>
                <li class="<?= $role == $key ? 'active' : '' ?>">
                    <a href="<?= Url::to(['index', 'role' => $key])?>"><?=$value ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <p>
        <br>
        <?= Html::a('Додати', ['create', 'role' => $role], ['class' => 'btn btn-success pull-right']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-custom dataTable no-footer'],
        'class'=>'table table-custom dataTable no-footer',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'name',
            'email:email',
            'phone',
            [
                'attribute' => 'status',
                'value' => 'statusDetail',
                'filter' => $searchModel::getStatusAll()
            ],
            'created_at:datetime',
            'last_visit:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
