<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;



$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => $model->roleDetail, 'url' => ['index', 'role' => $model->role]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th><?=$model->getAttributeLabel('id')?></th>
            <td><?=$model->id ?></td>
        </tr>
        <tr>
            <th><?=$model->getAttributeLabel('name')?></th>
            <td><?=$model->name ?></td>
        </tr>
        <tr>
            <th><?=$model->getAttributeLabel('username')?></th>
            <td><?=$model->username ?></td>
        </tr>
        <tr>
            <th><?=$model->getAttributeLabel('email')?></th>
            <td><?=$model->email ?></td>
        </tr>
        <tr>
            <th><?=$model->getAttributeLabel('phone')?></th>
            <td><?=$model->phone ?></td>
        </tr>
        <tr>
            <th><?=$model->getAttributeLabel('role')?></th>
            <td><?=$model->roleDetail ?></td>
        </tr>
        <tr>
            <th><?=$model->getAttributeLabel('status')?></th>
            <td><?=$model->statusDetail ?></td>
        </tr>
        <tr>
            <th><?=$model->getAttributeLabel('created_at')?></th>
            <td><?=Yii::$app->formatter->asDatetime($model->created_at) ?></td>
        </tr>
        <tr>
            <th><?=$model->getAttributeLabel('updated_at')?></th>
            <td><?=Yii::$app->formatter->asDatetime($model->created_at) ?></td>
        </tr>
        <tr>
            <th><?=$model->getAttributeLabel('last_visit')?></th>
            <td><?=Yii::$app->formatter->asDatetime($model->last_visit) ?></td>
        </tr>
        </tbody>
    </table>

</div>
