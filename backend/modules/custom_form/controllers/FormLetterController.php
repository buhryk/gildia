<?php

namespace backend\modules\custom_form\controllers;

use backend\modules\core\models\Page;
use backend\modules\custom_form\models\Form;
use backend\modules\custom_form\models\FormLetter;
use backend\modules\custom_form\models\FormLetterSearch;
use backend\modules\custom_form\models\FormSearch;
use backend\modules\faq\models\Faq;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `custom_form` module
 */
class FormLetterController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new FormLetterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
//        pr($this->findModel($id));
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    protected function findModel($id)
    {
        if (($model = FormLetter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
