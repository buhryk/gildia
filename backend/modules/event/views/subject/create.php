<?php

use yii\helpers\Html;

$this->title = Yii::t('event', 'Creating event subject');
$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Subjects list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>