<?php
$this->title = Yii::t('event', 'Type updating') . ' "' . $model->title . '"';

$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Event types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Event type updating')];
?>

<div class="rubric-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>