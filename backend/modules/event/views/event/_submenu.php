<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;
?>

<h1><?= Html::encode($this->title) ?></h1>

<ul class="nav nav-tabs">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
            <?= Yii::t('common', 'Editing'); ?>
        </a>
    </li>
</ul>
<br>
