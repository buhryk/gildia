<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use backend\assets\GoogleMapsAsset;
use backend\modules\event\models\Event;
GoogleMapsAsset::register($this);

$this->title = Yii::t('event', 'Event address updating');
$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Events list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model,
    ]); ?>

    <div class="rubric-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($modelLang, 'address')->textInput([
            'class' => 'form-control map_canvas',
            'data-error-message-no-results' => Yii::t('common', 'No results found'),
            'data-error-message-by-reason' => Yii::t('common', 'Geocode was not successful for the following reason'),
            'data-location' => $model->address
        ]); ?>

        <?php if ($model->address) { ?>
            <div style=" width: 100%; height: 400px;" id="map_canvas_container"></div>
            <p>&nbsp;</p>

            <?= $form->field($model, 'address_confirmed')->dropDownList([
                Event::ADDRESS_CONFIRMED_NO => Yii::t('common', 'No'),
                Event::ADDRESS_CONFIRMED_YES => Yii::t('common', 'Yes')
            ]); ?>
        <?php } ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>