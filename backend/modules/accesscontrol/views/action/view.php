<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Действие "' . $model->title . '"';
$this->params['breadcrumbs'][] = ['label' => 'Список модулей', 'url' => ['module/index']];
$controller = $model->controller;
$module = $controller ? $controller->module : null;
if ($module) {
    $this->params['breadcrumbs'][] = [
        'label' => 'Модуль "' . $module->title . '"',
        'url' => ['module/view', 'id' => $module->primaryKey]
    ];
    $this->params['breadcrumbs'][] = [
        'label' => 'Контроллер "' . $controller->title . '"',
        'url' => ['controller/view', 'id' => $controller->primaryKey]
    ];
}
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th>Контроллер</th>
            <td>
                <?php if ($controller) { ?>
                    <a href="<?= Url::to(['controller/view', 'id' => $controller->primaryKey]); ?>">
                        <?= $controller->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('alias'); ?></th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        </tbody>
    </table>
</div>