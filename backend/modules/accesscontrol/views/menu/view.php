<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Меню админ панели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('path'); ?></th>
            <td><?= $model->path; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('parent_id'); ?></th>
            <td>
                <?php $parent = $model->parent; ?>
                <?php if ($parent) { ?>
                    <a href="<?= Url::to(['view', 'id' => $parent->primaryKey]); ?>">
                        <?= $parent->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('icon'); ?></th>
            <td>
                <?php if ($model->icon) { ?>
                    <i class="<?= $model->icon; ?>" style="font-size: 20px;"></i>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('position'); ?></th>
            <td><?= $model->position; ?></td>
        </tr>
        <?php if (!$parent) { ?>
            <tr>
                <th>Подменю</th>
                <td>
                    <?php $submenus = $model->submenus; ?>
                    <?php foreach ($submenus as $submenu) { ?>
                        <a href="<?= Url::to(['view', 'id' => $submenu->primaryKey]); ?>" class="block">
                            <?= $submenu->title; ?>
                        </a>
                    <?php } ?>
                    <p></p>
                    <a href="<?= Url::to(['create', 'parent' => $model->primaryKey]); ?>"
                       class="btn-sm btn-success without-hover-text-decoration"
                    >
                        <i class="fa fa-plus" aria-hidden="true" style="padding-right: 10px;"></i>Добавить
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>