<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\accesscontrol\models\Menu;
?>

<div class="role-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(
        Menu::find()->where(['parent_id' => null])->all(), 'id', 'title'),
        ['prompt' => '']
    ); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'path') ?>
    <?= $form->field($model, 'icon') ?>
    <?= $form->field($model, 'position')->textInput(['type' => 'number']); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>