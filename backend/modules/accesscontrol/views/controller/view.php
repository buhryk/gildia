<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Контроллер "' . $model->title . '"';
$this->params['breadcrumbs'][] = ['label' => 'Список модулей', 'url' => ['module/index']];
$module = $model->module;
$this->params['breadcrumbs'][] = ['label' => 'Модуль "' . $module->title . '"', 'url' => ['module/view', 'id' => $module->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th>Модуль</th>
            <td>
                <?php $module = $model->module; ?>
                <?php if ($module) { ?>
                    <a href="<?= Url::to(['module/view', 'id' => $module->primaryKey]); ?>">
                        <?= $module->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('alias'); ?></th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        <tr>
            <th>Список действий</th>
            <td>
                <?php $actions = $model->actions; ?>
                <?php if ($actions) { ?>
                    <ul>
                        <?php foreach ($actions as $action) { ?>
                            <li>
                                <a href="<?= Url::to(['action/view', 'id' => $action->primaryKey]); ?>">
                                    <?= $action->title . ' ('.$action->alias.')'; ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <a href="<?= Url::to(['action/create', 'controller' => $model->primaryKey]); ?>"
                   class="btn-sm btn-success without-hover-text-decoration"
                >
                    <i class="fa fa-plus" aria-hidden="true" style="padding-right: 10px;"></i>Добавить действие
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>