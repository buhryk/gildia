<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Список ролей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('alias'); ?></th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th>Права</th>
            <td>
                <a href="<?= Url::to(['rules', 'id' => $model->primaryKey]); ?>">
                    Просмотреть
                </a>
            </td>
        </tr>
        <tr>
            <th>Меню админ панели</th>
            <td>
                <a href="<?= Url::to(['admin-menu', 'id' => $model->primaryKey]); ?>">
                    Просмотреть
                </a>
            </td>
        </tr>
        <tr>
            <th>Пользователи</th>
            <td>
                <ul>
                    <?php foreach ($model->users as $user) { ?>
                        <li>
                            <a href="<?= Url::to(['/adminuser/user/view', 'id' => $user->primaryKey]); ?>" class="block">
                                <?= $user->username . ' ('.$user->email.')'; ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>

                <a href="<?= Url::to(['/adminuser/user/create', 'role' => $model->primaryKey]); ?>"
                   class="btn-sm btn-success without-hover-text-decoration"
                >
                    <i class="fa fa-plus" aria-hidden="true" style="padding-right: 10px;"></i>Добавить пользователя
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>