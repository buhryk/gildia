<?php

namespace backend\modules\gallery\controllers;

use backend\modules\blank\models\BlankItem;
use backend\modules\core\models\PageCategory;
use backend\modules\gallery\models\GalleryItem;
use Yii;
use backend\modules\gallery\models\Gallery;
use backend\modules\gallery\models\GallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex($type = Gallery::TYPE_IMAGE)
    {
        $searchModel = new GallerySearch();
        $searchModel->type = $type;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionResources($id)
    {
        $model = $this->findModel($id);

        $itemModel = new GalleryItem();
        $itemModel->gallery_id = $id;


        if ($itemModel->load(Yii::$app->request->post()) && $itemModel->save()) {
            $itemModel = new GalleryItem();
        }

        return $this->render('resources', [
            'model' => $model,
            'itemModel' => $itemModel,
        ]);
    }

    public function actionItemUpdate($id)
    {
        $model = GalleryItem::findOne($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->save()) {
                $data['status'] = true;
                $data['message'] = 'Збережено';
            } else {
                $data['errors'] = ActiveForm::validate($model);
            }
            return $data;
        }
    }

    public function actionCreate($type = Gallery::TYPE_IMAGE)
    {
        $model = new Gallery();
        $model->type = $type;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('info', 'Створено успішно.');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'type' => $model->type]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
