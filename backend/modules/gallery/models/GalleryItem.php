<?php

namespace backend\modules\gallery\models;

use common\behaviors\PositionBehavior;
use Yii;

/**
 * This is the model class for table "gallery_item".
 *
 * @property integer $id
 * @property integer $gallery_id
 * @property integer $position
 * @property string $value
 *
 * @property Gallery $gallery
 */
class GalleryItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery_id', 'position'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            PositionBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gallery_id' => 'Gallery ID',
            'position' => 'Position',
            'value' => 'Value',
            'description' => 'Опис'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }

    public function getVideoKey()
    {
        return str_replace('https://youtu.be/', '', $this->value);
    }
}
