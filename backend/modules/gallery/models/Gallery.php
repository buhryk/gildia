<?php

namespace backend\modules\gallery\models;

use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use Yii;
use backend\modules\core\models\PageCategory;
use yii\behaviors\SluggableBehavior;
use common\models\Lang;
/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image
 * @property string $alias
 * @property integer $category_id
 * @property string $additional_data
 * @property integer $type
 *
 * @property PageCategory $category
 * @property GalleryItem[] $galleryItems
 * @property GalleryLang[] $galleryLangs
 * @property Lang[] $langs
 */
class Gallery extends BaseDataModel
{
    public $title;
    public $description;
    public $record_id;

    const TYPE_IMAGE = 1;
    const TYPE_VIDEO = 2;

    static $current = null;

    public static function tableName()
    {
        return 'gallery';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => LangBehavior::className(),
                't' => new GalleryLang(),
                'fk' => 'record_id',
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['image', 'alias'], 'string', 'max' => 255],
            [[ 'category_id', 'type'], 'integer'],
            [['title', 'description'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => PageCategory::className(), 'targetAttribute' => ['category_id' => 'id']],

        ]);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => 'Alias',
            'title' => 'Заголовок',
            'description' => 'Опис',
            'category_id' => 'Категорія',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PageCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(GalleryItem::className(), ['gallery_id' => 'id']);
    }

    public function getLang()
    {
        return $this->hasMany(GalleryLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Lang::className(), ['id' => 'lang_id'])->viaTable('gallery_lang', ['record_id' => 'id']);
    }

    public static function getTypeAll()
    {
        return [
            self::TYPE_IMAGE => 'Фото галерея',
            self::TYPE_VIDEO => 'Відео галерея'
        ];
    }

    public function getTypeDetail()
    {
        return isset(self::getTypeAll()[$this->type]) ? self::getTypeAll()[$this->type] : '';
    }

    public static function getCurrent()
    {
        if (self::$current == null) {
            self::$current = self::find()
                ->where(['alias' => Yii::$app->request->get('alias')])
                ->one();
        }

        return self::$current;
    }
}
