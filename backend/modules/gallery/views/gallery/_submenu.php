<?php
use yii\helpers\Url;

$action = Yii::$app->controller->action->id;

?>
<ul class="nav nav-tabs bar_tabs">

    <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
            Редагування
        </a>
    </li>
    <li <?= ($action === 'resources') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['resources', 'id' => $model->primaryKey]) ?>">
            Ресурси
        </a>
    </li>
</ul>
