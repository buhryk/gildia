<?php
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
use backend\modules\gallery\models\Gallery;

$formId = 'form-item-' . $item->id;
?>
<?php $form = ActiveForm::begin(['id' => $formId, 'action' => ['item-update', 'id' => $item->id]]); ?>
<tr>
    <td class="checkbox-item" style="width: 10px;" >
        <input type="checkbox" name="selection[]" value="<?=$item->id ?>">
    </td>
    <td  style="" class="sort-item">
        <i class="fa fa-arrows-alt">
    </td>

    <?php if ($type == Gallery::TYPE_IMAGE): ?>
        <td width="45%">
            <?= $form->field($item, 'value')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => 'elfinder',
                'path'          => $filePath,
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false
            ])->label(false); ?>
        </td>
        <td width="45%">
            <?= Html::img($item->value, ['width' => '230px']) ?>
        </td>
    <?php else: ?>
        <td width="55%">
            <?= $form->field($item, 'value')->textInput()->label(false) ?>
        </td>
        <td width="250px">
            <a href="<?= $item->value ?>" data-fancybox="video" class="video-gallery-item" >
                <img src="http://img.youtube.com/vi/<?= $item->getVideoKey() ?>/hqdefault.jpg" alt="video-preview">
            </a>
        </td>
    <?php endif; ?>
    <td><button class="btn btn-primary">Зберегти</button></td>
</tr>
<?php ActiveForm::end() ?>

<?= $this->registerJs("
    Form.init('".$formId."', {})
", \yii\web\View::POS_READY) ?>