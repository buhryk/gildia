<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use mihaildev\elfinder\InputFile;
use backend\modules\gallery\models\Gallery;
?>

<h2>Новий файл</h2>
<table class="table table-striped">
    <thead>
    <?php $form = ActiveForm::begin([]); ?>
    <tr>
        <th>
            <?php if ($model->type == Gallery::TYPE_IMAGE): ?>
                <?= $form->field($itemModel, 'value')->widget(InputFile::className(), [
                    'language'      => 'ru',
                    'controller'    => 'elfinder',
                    'path'          => $filePath,
                    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                    'options'       => ['class' => 'form-control'],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple'      => false
                ])->label(false); ?>
            <?php else: ?>
                <?= $form->field($itemModel, 'value')->textInput()->label(false) ?>
            <?php endif; ?>
        </th>
        <th>
            <?= $form->field($itemModel, 'description')->textarea() ?>
        </th>
        <th>
            <div class="form-group" style="text-align: right;">
                <?= Html::submitButton('Добавити', ['class' => 'btn btn-primary',]); ?>
            </div>
        </th>
    </tr>
    <?php ActiveForm::end(); ?>
    </thead>
</table>
