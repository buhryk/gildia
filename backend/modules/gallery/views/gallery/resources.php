<?php
use backend\modules\gallery\models\GalleryItem as CurrentModel;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

$this->title = 'Редагування: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Галерея', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ресурси';

$items = $model->items;
\backend\widgets\SortActionWidget::widget(['className' => CurrentModel::className()]);

?>
<div class="blank-view">

    <?= $this->render('resources/_create', [
            'itemModel' => $itemModel,
            'filePath' => 'resources',
            'model' => $model
    ]); ?>

    <h2>Додані файли</h2>
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'className' => CurrentModel::className()]) ?>
    </div>
    <?php Pjax::begin(['id' => 'content-list']) ?>
    <div id="myTabContent" class="grid-view">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" class="select-all" name="selection_all" value="1">
                </th>
                <th ></th>
                <th>Файл</th>
                <th width="20px">Опис</th>
                <th width="135"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($items as $item): ?>
                <?= $this->render('resources/_item', ['item' => $item, 'filePath' => $filePath, 'type' => $model->type]) ?>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
    <?php Pjax::end() ?>
</div>


