<?php

namespace backend\widgets;

use yii\base\Widget;

class SeoWidget extends Widget {

    public $model;
    public $parameters;

    public function init() {
        parent::init();
    }

    public function run() {
        return $this->render('seo', [
            'model' => $this->model,
            'modelLang' => $this->parameters['modelLang']
        ]);
    }
}