<?php

namespace backend\widgets;

use yii\bootstrap\Widget;
use yii\bootstrap\Modal;
use yii\web\View;


class MainModal extends Widget
{
    public $id = 'modal-popup';

    public function run()
    {
        ?>
        <div class="modal  fade" id="<?=$this->id ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow:hidden;">
            <div class="modal-dialog" role="document">

            </div>
        </div>
    <?php
        $this->view->registerJs("
             $('body').on('click', '.modalButton',function(e) {
                 e.preventDefault();
                 $('#{$this->id}').modal().load($(this).attr('href'));   
             });
        ", View::POS_READY);
    }
}
