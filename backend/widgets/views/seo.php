<?php
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use mihaildev\elfinder\InputFile;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'meta_title') ?>
    <?= $form->field($modelLang, 'meta_description')->textarea() ?>
    <?= $form->field($modelLang, 'meta_keywords')->textarea() ?>
<!--    <hr>-->
<!--    --><?//= $form->field($modelLang, 'og_title') ?>
<!--    --><?//= $form->field($modelLang, 'og_description')->textarea() ?>
<!--    <div class="row">-->
<!--        <div class="col-md-8">-->
<!--            --><?//= $form->field($model, 'og_image')->widget(InputFile::className(), [
//                'language'      => 'ru',
//                'controller'    => 'elfinder',
//                'filter'        => 'image',
//                'path'          => 'common',
//                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
//                'options'       => ['class' => 'form-control'],
//                'buttonOptions' => ['class' => 'btn btn-default'],
//                'multiple'      => false
//            ]); ?>
<!--        </div>-->
<!--        <div class="col-md-4">-->
<!--            --><?php //if ($model->og_image) : ?>
<!--                --><?//=Html::img($model->og_image, ['width' => 200]) ?>
<!--            --><?php //endif; ?>
<!--        </div>-->
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>