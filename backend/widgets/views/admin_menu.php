<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$module = Yii::$app->controller->module->id;
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$pathInfo = '/'.Yii::$app->request->pathInfo;
?>

<ul class="nav side-menu">
    <?php foreach ($items as $item) { ?>
        <li>
            <?php if ($item['submenus']) { ?>
                <a>
                    <i class="<?= $item['icon']; ?>"></i> <?= $item['title']; ?><?= $item['notification'] ?
                        ('<span class="admin-menu-notification">'.$item['notification'].'</span>') : ''; ?> <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <?php foreach ($item['submenus'] as $submenu) { ?>
                        <li><a href="<?= Url::to([$submenu['path']]) ; ?>"><?= $submenu['title']; ?><?= $submenu['notification'] ?
                                    ('<span class="admin-menu-notification">'.$submenu['notification'].'</span>') : ''; ?></a></li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <a href="<?= Url::to([$item['path']]) ; ?>">
                    <i class="<?= $item['icon']; ?>"></i> <?= $item['title']; ?><?= $item['notification'] ?
                        ('<span class="admin-menu-notification">'.$item['notification'].'</span>') : ''; ?>
                </a>
            <?php } ?>
        </li>
    <?php } ?>
</ul>