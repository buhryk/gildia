<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;


/**
 * This asset bundle provides the base JavaScript files for the Yii Framework.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RedactorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'vendors/redactor/redactor.js',

        'vendors/redactor/plugins/alignment.js',
        'vendors/redactor/plugins/table.js',
        'vendors/redactor/plugins/video.js',
        'vendors/redactor/plugins/source.js',
        'vendors/redactor/plugins/fullscreen.js',
        'vendors/redactor/plugins/pagebreak.js',
        'vendors/redactor/plugins/filemanager.js',
        'vendors/redactor/plugins/inlinestyle.js',
        'vendors/redactor/plugins/properties.js',
        'vendors/redactor/plugins/textdirection.js',
        'vendors/redactor/plugins/textexpander.js',
        'vendors/redactor/plugins/codemirror.js',
        'vendors/redactor/plugins/fontcolor.js',
        'vendors/redactor/plugins/fontsize.js',
        'vendors/redactor/plugins/readyblocks.js',
        'vendors/redactor/plugins/imagemanager.js',
        'vendors/redactor/codemirror.js',

        'vendors/redactor/lang/ru.js',
    ];

    public $css = [
        'vendors/redactor/redactor.css',
        'vendors/redactor/plugins/pagebreak.css',
        'vendors/redactor/plugins/alignment.css',
        'vendors/redactor/codemirror.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}