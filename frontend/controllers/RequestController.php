<?php
namespace frontend\controllers;

use frontend\models\RequestForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class RequestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionCreate($redirectUrl)
    {
        $model = new RequestForm();

        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->createRequest()){
                Yii::$app->session->setFlash('success', Yii::t('common', 'Ваша заявка отправлена!'));
                return $this->redirect($redirectUrl);
            }

            return ActiveForm::validate($model);
        }

        return false;
    }
}
