<?php
namespace frontend\controllers;

use backend\modules\catalog\models\Catalog;
use backend\modules\core\models\Page;
use backend\modules\core\models\Setting;
use backend\modules\news\models\News;
use backend\modules\order\models\Document;
use backend\modules\order\models\Order;
use backend\modules\tenders\models\tenders;
use common\components\user\events\FormEvent;
use frontend\models\EditForm;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\RegistrationForm;
use frontend\models\ResetPasswordForm;
use frontend\models\User;
use frontend\modules\cabinet\models\PasswordChangeForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class MemberController extends Controller
{
    public $enableCsrfValidation = false;

//    public $layout = 'cabinet';
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login'],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
//                'rules' => [
//                    ['allow' => true, 'actions' => ['login', 'register'], 'roles' => ['?']],
//                    ['allow' => true, 'actions' => [
//                        'login', 'logout', 'index', 'register',
//                        'edit', 'password-change', 'order',
//                        'delete', 'new-file'
//                    ], 'roles' => ['@']],
//                ],
            ],

        ];
    }

    public function actionIndex($file_id = false)
    {
        if (\Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(Url::to(['/site/index']));
        }

        $model = EditForm::find()->where(['id' => Yii::$app->user->identity->id ])->one();
        $modelChange = new PasswordChangeForm($model);

        $orders = Order::find()->where(['user_id' => $model->id])->all();
        $documents = Document::find()->where(['status' => Document::STATUS_ACTIVE])->all();
//pr( Document::find()->where(['id' => 7])->one());
        if ($file_id) {
            $document_new = Document::find()->where(['id' => $file_id])->one();
        } else {
            $document_new = new Document();
        }

//        $model = new User();
        return $this->render('index', [
            'orders' => $orders,
            'documents' => $documents,
            'document_new' => $document_new,
            'file_id' => $file_id,
            'userModel' => $model,
            'modelChange' => $modelChange,
            'user' => Yii::$app->user->identity
        ]);
    }

    public function actionLogin()
    {

        $model = \Yii::createObject(LoginForm::className());

        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return Yii::$app->getResponse()->redirect(['/member/index']);
        }

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

    }

    public function actionRegister()
    {
        $model = new RegistrationForm();

        $postData = \Yii::$app->request->post();
        $password = $postData['RegistrationForm']['password'];
        $login = $postData['RegistrationForm']['email'];

        if (Yii::$app->request->isAjax && $model->load($postData)) {

            $model->setPassword($password);
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->save()){

                $modelLogin = \Yii::createObject(LoginForm::className());

                $loginData = ['LoginForm' => [
                    'password' => $password,
                    'login' => $login,
                ]];
                if ($modelLogin->load($loginData) && $modelLogin->login()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return Yii::$app->getResponse()->redirect(['/member/index']);
                }

            }

            return ActiveForm::validate($model);

        }

    }

    public function actionEdit()
    {
        $model = EditForm::find()->where(['id' => Yii::$app->user->identity->id ])->one();

        $postData = \Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $model->load($postData)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->save()){
                return Yii::$app->getResponse()->redirect(['/member/index']);
            }

            return ActiveForm::validate($model);

        }

        return false;
    }

    public function actionPasswordChange()
    {
        $model = EditForm::find()->where(['id' => Yii::$app->user->identity->id ])->one();
        $modelChange = new PasswordChangeForm($model);

        $postData = \Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $modelChange->load($postData)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($modelChange->changePassword()){
                return Yii::$app->getResponse()->redirect(['/member/index']);
            }

            return ActiveForm::validate($model);

        }

        return false;
    }

    public function actionOrder($redirectUrl)
    {
        $model = new Order();

        $postData = \Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $model->load($postData)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->save()){
                Yii::$app->session->setFlash('success', Yii::t('common','Ваша заявка отправлена!' ));
                return $this->redirect($redirectUrl);
            }

            return ActiveForm::validate($model);

        }

        return false;
    }

    public function actionDelete($file_id)
    {
        Document::deleteAll(['id' => $file_id]);

        return $this->redirect(['/member/index']);
    }

    public function actionNewFile($file_id = false)
    {
        if ($file_id) {
            $model = Document::find()->where(['id' => $file_id])->one();
        } else {
            $model = new Document();
        }


        $postData = \Yii::$app->request->post();

        if (Yii::$app->request->isAjax && $model->load($postData)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->save()){
                if ($file_id) {
                    Yii::$app->session->setFlash('success', Yii::t('common', 'Файл успешно был изменен'));
                } else {
                    Yii::$app->session->setFlash('success', Yii::t('common', 'Файл успешно был добавлен'));
                }
                return $this->redirect(['/member/index']);
            }

            return ActiveForm::validate($model);

        }

        return false;
    }

    public function actionLogout(){
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('common', 'Check your email for further instructions.'));

                return $this->goHome();
            }

            return ActiveForm::validate($model);
        }

        return false;
    }

    public function actionResetPassword($token)
    {

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('common', 'New password saved.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


}
