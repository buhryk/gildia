<?php
namespace frontend\controllers;

use backend\modules\catalog\models\CatalogCategory;
use backend\modules\core\models\Menu;
use backend\modules\core\models\Page;
use backend\modules\core\models\Setting;
use backend\modules\email_delivery\models\Subscriber;
use backend\modules\event\models\Event;
use backend\modules\faq\models\Faq;
use backend\modules\faq\models\FaqCategory;
use backend\modules\news\models\News;
use backend\modules\section\models\SectionItem;
use backend\modules\seo\models\Seo;
use backend\modules\slider\models\Slider;
use backend\modules\slider\models\SliderItem;
use backend\modules\table\models\Firm;
use backend\modules\table\models\TableClass;
use backend\modules\table\models\TableExcel;
use backend\modules\table\models\TableExcelMark;
use backend\modules\table\models\TableType;
use common\models\Lang;
use frontend\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $lastNews = News::find()
            ->andWhere(['status' => News::STATUS_ACTIVE])
            ->orderBy(['id' => SORT_DESC])
            ->limit(2)
            ->joinWith('lang')
            ->all();

        $inWhichAreas = Slider::getSlider(1);
        $howWeWork = Slider::getSlider(2);
        $why_we = Slider::getSlider(3);
        $why_does_it_work = Slider::getSlider(4);
//pr($inWhichAreas);
        $my_izbavim = SectionItem::getSectionByAlias('my-izbavim-vas-ot-byurokratii-i-rutiny-');
        $luchshie_spetsialisty = SectionItem::getSectionByAlias('luchshie-spetsialisty');
        $o_proekte = SectionItem::getSectionByAlias('o-proekte');


        return $this->render('index', [
            'lastNews' => $lastNews,
            'inWhichAreas' => $inWhichAreas,
            'why_we' => $why_we,
            'why_does_it_work' => $why_does_it_work,
            'howWeWork' => $howWeWork,
            'my_izbavim' => $my_izbavim,
            'luchshie_spetsialisty' => $luchshie_spetsialisty,
            'o_proekte' => $o_proekte,
        ]);

    }

    public function actionContact()
    {
//        $this->layout = 'contact';

        $page = Page::getPageByAlias('kontakty');
//pr(explode(',',Setting::getValueByKey('emails')));
        return $this->render('contact', [
                'page' => $page,
        ]);

    }

    public function actionSend()
    {

        $postData = Yii::$app->request->post();


        if (!empty($_FILES)) {

//            $file =  $_FILES['Файл']['name'];
            $file = str_replace(' ', '_', $_FILES['Файл']['name']);

            $postData['Файл'] = $file;

            move_uploaded_file($_FILES['Файл']['tmp_name'], 'uploads/contact/' . $file);

//            pr($_FILES);
        }

        $message = Yii::$app->mailer->compose('contactForm', ['data' => $postData])
            ->setFrom($postData['E-mail'])
            ->setTo(['buhrykserhii@gmail.com'])
            ->setSubject('Steel Форма обратной связи')
            ->send();
        if ($message) {

            Yii::$app->session->setFlash('success', Yii::t('common', "Форма отправлена"));
            return $this->redirect(Yii::$app->request->referrer);
        }

    }

    public function actionFaq($category_id = false)
    {
        $page = Page::getPageByAlias('faq');

        $query = Faq::find()
            ->rightJoin('faq_to_category', 'id = faq_id')
            ->where([
                'status' => Faq::STATUS_ACTIVE,
            ]);
        if ($category_id) {
            $query->andWhere(['category_id' => $category_id]);
        }
        $faqs = $query->orderBy('position')->all();

        $categories = FaqCategory::getCategoryAll(true);

        return $this->render('faq', [
            'page' => $page,
            'faqs' => $faqs,
            'categories' => $categories,
        ]);
    }


    public function actionSubscribeForm($rediractUrl)
    {

        $model = new Subscriber();


        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        else if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', Yii::t('common', 'Спасибо за подписку на новости'));
            return $this->redirect($rediractUrl);
        }

    }

}
