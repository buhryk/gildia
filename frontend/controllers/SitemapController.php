<?php

namespace frontend\controllers;

use backend\modules\core\models\Menu;
use backend\modules\core\models\Page;
use backend\modules\core\models\PageLang;
use backend\modules\gallery\models\Gallery;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsLang;
use yii\web\Controller;
use yii\db\Query;
use Yii;

class SitemapController extends Controller
{

    public function actionIndex()
    {

        if (!$xml_sitemap = Yii::$app->cache->get('sitemap')) {  // проверяем есть ли закэшированная версия sitemap
            $urls = array();

            // Выбираем Страници
            $pages = Page::find()->all();
            $angl_pages = PageLang::find()->where(['lang_id' => 2])->all();
            foreach ($pages as $page) {
                // Укр
                $urls[] = array(
                    Yii::$app->urlManager->createUrl(['/' . $page->alias]) // создаем ссылки на выбранные категории
                , 'daily'                                                           // вероятная частота изменения категории
                , '0.7'
                );
                // Англ
                foreach ($angl_pages as $item){
                    if ($page->id == $item->record_id){
                        $urls[] = array(
                            Yii::$app->urlManager->createUrl(['/en/' . $page->alias]) // создаем ссылки на выбранные категории
                        , 'daily'                                                           // вероятная частота изменения категории
                        , '0.7'
                        );
                    }
                }
            }

            // Выбираем Новости
            $news = News::find()->all();
            $angl_news = NewsLang::find()->where(['lang_id' => 2])->all();
            foreach ($news as $item) {
                // Укр
                $urls[] = array(
                    Yii::$app->urlManager->createUrl(['/news/news/view?alias=' . $item->alias]) // создаем ссылки на выбранные категории
                , 'daily'                                                           // вероятная частота изменения категории
                , '0.5'
                );
                // Англ
                foreach ($angl_news as $angl) {
                    if ($news->id == $angl->record_id) {
                        $urls[] = array(
                            Yii::$app->urlManager->createUrl(['/en/news/news/view?alias=' . $item->alias])  // создаем ссылки на выбранные категории
                        , 'daily' // вероятная частота изменения категории
                        , '0.5'
                        );
                    }
                }
            }

            // Выбираем Галерею
            $gallery = Gallery::find()->all();
            foreach ($gallery as $item) {
                // Укр
                $urls[] = array(
                    Yii::$app->urlManager->createUrl(['/gallery/gallery/view?alias=' . $item->alias]) // создаем ссылки на выбранные категории
                , 'daily'                                                           // вероятная частота изменения категории
                , '0.5'
                );
                // Англ
                $urls[] = array(
                    Yii::$app->urlManager->createUrl(['/en/gallery/gallery/view?alias=' . $item->alias]) // создаем ссылки на выбранные категории
                , 'daily'                                                           // вероятная частота изменения категории
                , '0.5'
                );
            }

            // Выбираем ссылки привязаные к меню
            $gallery = Menu::find()->all();
            foreach ($gallery as $item) {
                if ($item->url){
                    // Укр
                    $urls[] = array(
                        Yii::$app->urlManager->createUrl([$item->url]) // создаем ссылки на выбранные категории
                    , 'daily'                                                           // вероятная частота изменения категории
                    , '0.7'
                    );
                    // Англ
                    $urls[] = array(
                        Yii::$app->urlManager->createUrl(['/en' . $item->url]) // создаем ссылки на выбранные категории
                    , 'daily'                                                           // вероятная частота изменения категории
                    , '0.7'
                    );
                }
            }

            // Укр
            $urls[] = array(
                Yii::$app->urlManager->createUrl(['/gallery']) // создаем ссылки на выбранные категории
            , 'daily'                                                           // вероятная частота изменения категории
            , '0.7'
            );
            // Англ
            $urls[] = array(
                Yii::$app->urlManager->createUrl(['/en/gallery']) // создаем ссылки на выбранные категории
            , 'daily'                                                           // вероятная частота изменения категории
            , '0.7'
            );

            // Укр
            $urls[] = array(
                Yii::$app->urlManager->createUrl(['/gallery/gallery/view?alias=video-golerea']) // создаем ссылки на выбранные категории
            , 'daily'                                                           // вероятная частота изменения категории
            , '0.7'
            );
            // Англ
            $urls[] = array(
                Yii::$app->urlManager->createUrl(['/en/gallery/gallery/view?alias=video-golerea']) // создаем ссылки на выбранные категории
            , 'daily'                                                           // вероятная частота изменения категории
            , '0.7'
            );

            // Укр
            $urls[] = array(
                Yii::$app->urlManager->createUrl(['/']) // создаем ссылки на выбранные категории
            , 'daily'                                                           // вероятная частота изменения категории
            , '1'
            );
            // Англ
            $urls[] = array(
                Yii::$app->urlManager->createUrl(['/en/']) // создаем ссылки на выбранные категории
            , 'daily'                                                           // вероятная частота изменения категории
            , '1'
            );

            $xml_sitemap = $this->renderPartial('index', array( // записываем view на переменную для последующего кэширования
                'host' => Yii::$app->request->hostInfo,         // текущий домен сайта
                'urls' => $urls,                                // с генерированные ссылки для sitemap
            ));

            Yii::$app->cache->set('sitemap', $xml_sitemap, 3600*12); // кэшируем результат, чтобы не нагружать сервер и не выполнять код при каждом запросе карты сайта.
        }

        header('Content-Type: application/xml');

        echo $xml_sitemap;
    }
}