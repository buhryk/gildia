$( document ).ready(function() {
    $('[data-popup-btn]').click(function (e) {
        e.preventDefault();
        $('.popup-dev').fadeOut();
        var popup = $(this).data('popup-btn');
        $('[data-popup=' + popup + ']').fadeIn();
    });
    $('.popup-dev .exit-btn').click(function (e) {
        e.preventDefault();
        $(this).closest('.popup-dev').fadeOut();
    });

    var selector = $('input[type="tel"]');
    var im = new Inputmask("+38(999)999-99-99");
    im.mask(selector);
});