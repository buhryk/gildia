;
(function( $ ) {

    $( document ).ready(function() {
        $('.permission-alpha a').click(function (e) {
            e.preventDefault();
            var id  = $(this).attr('href'),
                top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1000);
        });
        /* Slick Init */
        $('.navbar-toggler').addClass('collapsed');
        // $( ".user" ).click(function() {
        //     $( ".pop__up" ).removeClass("hide");
        // });
        $( ".close" ).click(function() {
            $( ".pop__up" ).addClass("hide");
        });

        $('.services__section-nb .box-dev-item .arrow-btn').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('open');
            $(this).closest('.top').next('.bottom').slideToggle();
        });

        $("#showpass").click(function() {
            var x = document.getElementById("pass");
            if (x.type === "password") {
                x.type = "text";
                $("#showpass").addClass('eye');
            } else {
                x.type = "password";
                $("#showpass").removeClass('eye');
            }
        });

        $('.pop__up-wrap').slick({
            arrows:false,
            dots:true,
            slidesToShow: 1,
            slidesToScroll: 1
        });

        $('.dots').click(function(){
            if($(this).hasClass('edits-show')){
                $(this).removeClass('edits-show');
            }
            else{
                $(this).addClass('edits-show');
            }
        });

        $('.services__wrapper').click(function(){
            if($(this).hasClass('show-content')){
                $(this).removeClass('show-content');
            }
            else{
                $(this).addClass('show-content');
            }
        });

    });

    var target = $('.wow');
    var targetPos;
    var winHeight = $(window).height();
    var scrollToElem;
    var winScrollTop;
    winScrollTop = $(window).scrollTop();
    target.each(function () {
        targetPos = $(this).offset().top;
        scrollToElem = targetPos - winHeight;
        if(winScrollTop > scrollToElem + 200){
            $(this).removeClass('wow');
        }
    });
    $(window).scroll(function(){
        winScrollTop = $(this).scrollTop();
        target.each(function () {
            targetPos = $(this).offset().top;
            scrollToElem = targetPos - winHeight;
            if(winScrollTop > scrollToElem + 100){
                $(this).removeClass('wow');
            }
        });
    });

    /* $(window).scroll(function(){

         $('section').each(function() {
             console.log($(this).offset().top);
             if($(window).scrollTop() >= $(this).offset().top + $(this).height() || $(window).scrollTop() < $(this).offset().top)
                 $(this).addClass('wow');
             else
                 $(this).removeClass('wow');
         });

     });*/

    $('[data-popup-btn]').click(function (e) {
        e.preventDefault();
        $('.popup-dev').fadeOut();
        var popup = $(this).data('popup-btn');
        $('[data-popup='+popup+']').fadeIn();
    });
    $('.popup-dev .exit-btn').click(function (e) {
        e.preventDefault();
        $(this).closest('.popup-dev').fadeOut();
    });

}( jQuery ));

(function () {
    var toAnimate = $('.to-animate');

    function mainScroll() {
        if (toAnimate.not('.animated').length) {
            toAnimate.each(function () {
                var t = $(this);
                if (pageYOffset + innerHeight > t.offset().top + 50) {
                    this.transitionDuration = $(this).addClass('animated').css('transition-duration');
                    this.duration = this.transitionDuration.indexOf('ms') > -1 ? parseFloat(this.transitionDuration) : parseFloat(this.transitionDuration) * 1000;
                    setTimeout(function () {
                        t.trigger('animated');
                    }, this.duration);
                }
            });
        }
    }

    setTimeout(function () {
        mainScroll();
    }, 1000);
    $(window).scroll(mainScroll);
    window.onload = mainScroll();
})();