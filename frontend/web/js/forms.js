// Request
$( document ).ready(function() {
    $('#create_request').on('click', function () {
        var form = $('#cooperate-form');

        var successMessage = form.data('success-message');
        $('.validate-done').remove();

        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serialize(),
            success: function(data) {
                console.log(data);
                if (data.status == 'success') {

                    $('.exit-btn').click();
                    $('#popup-success').fadeIn();
                    $("#popup-success h2").html(successMessage);


                    clearForm(this);
                } else if (data.errors) {
                    $.each(data.errors, function(index, value) {
                        console.log(value);
                        addError(index, value);

                    });
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    function addError(elementId, value) {
        var element = $('#'+elementId);
        // element.parent().addClass("no-valid");
        element.parent().append('<div class="help-block">'+value+'</div>');
    }

    function clearForm(form) {
        $.each(form.find('.input, textarea'), function() {
            $(this).val('')
        });
    }
});

// Custom form
$( document ).ready(function() {

    $('.create_form').on('click', function () {

        var successMessage = $(this).closest('form').data('success-message');

        $('.validate-done').remove();

        // var form = document.forms.cooperateForm;
        var formForClear = $(this).closest('form');
        var formId = $(this).closest('form');
        var formIndex = $('form').index($(formId));

        var form = document.forms[formIndex];
        // var form1 = $(this).closest('.form_pop').find('form');
         // var form = document.forms.form1;
        console.log(formIndex);
        console.log($(this).closest('.form_pop').find('form'));
        // var form = $(this).closest('.form_pop').find('form');
        var formData = new FormData(form);

        var xhr = new XMLHttpRequest();
        xhr.open("POST", $(this).closest('form').data('url'));


        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if(xhr.status == 200) {

                    data = xhr.responseText;
                    if(data == "true") {
                        $('.exit-btn').click();
                            $('#popup-success').fadeIn();
                            $("#popup-success h2").html(successMessage);

                        $('html, body').animate({scrollTop: 0},0);
                            location.reload();
                            // clearForm(formForClear);
                    } else {

                    }
                }
            }
        };

        xhr.send(formData);

    });

    function addError(elementId, value) {
        var element = $('#'+elementId);
        // element.parent().addClass("no-valid");
        element.parent().append('<div class="help-block">'+value+'</div>');
    }

    function clearForm(form) {
        // console.log($(th));
        $.each($(form).find('input, textarea'), function() {
            $(this).val('')
        });
    }
});

$('label.file').click(function (e) {
    e.preventDefault();
    var label = $(this);
    var input = label.closest('form').find('input[type=file]');
    input.trigger('click');
});
$('input[type=file]').change(function (e) {
    var fileName = '';
    var input = $(this);
    var label = input.closest('form').find('label.file').find('span');
    if( input.files && input.files.length > 1 ){
        fileName = ( input.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', input.files.length );
    }
    else{
        fileName = e.target.value.split( '\\' ).pop();
    }
    if( fileName ){
        label.text(fileName);
    }
    else{
        label.text("прикрепить файл");
    }
});

// Login, Register form

$('#login-form, #register, #edit, #passwordChange, #order-form, .order-form, #new-file, #request-form, #reset-form').on('beforeSubmit', function () {
    var $yiiform = $(this);
    // отправляем данные на сервер
    $.ajax({
            type: $yiiform.attr('method'),
            url: $yiiform.attr('action'),
            data: $yiiform.serializeArray(),
            success: function(res){
                console.log(res);
            },
            error: function (err) {
                console.log(err);
            }
        }
    )
        .done(function(data) {
            if(data.success) {
                console.log(data);
                // данные сохранены
            } else {
                console.log('err');
                console.log(data);
                // сервер вернул ошибку и не сохранил наши данные
            }
        })
        .fail(function () {
            console.log('fail');
            // не удалось выполнить запрос к серверу
        })

    return false; // отменяем отправку данных формы
})