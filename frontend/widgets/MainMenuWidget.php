<?php
namespace frontend\widgets;

use backend\modules\core\models\Page;
use backend\modules\core\models\PageLang;
use yii\base\Widget;
use backend\modules\core\models\Menu;
use yii\helpers\Url;

class MainMenuWidget extends Widget
{
    public $mobileMenu = false;
    public $menuItems;
    public $type = 'header';

    public function init()
    {
        $query = Menu::find();
        $query->where(['not_show_menu' => null]);
        $query->orWhere(['not_show_menu' => 0]);

        if ($this->type == 'header') {
            $query->andWhere(['type' => 1]);
        } else {
            $query->andWhere(['type' => 2]);
        }

        $query->andWhere(['parent' => 0]);
        $query->orderBy(['position' => SORT_ASC]);
        $this->menuItems = $query->all();
    }

    public function run()
    {
        if ($this->mobileMenu) {
            return $this->render('main-menu-mobile', [
                'menuItems' => $this->menuItems,
            ]);
        } else {
            return $this->render('main-menu', [
                'menuItems' => $this->menuItems,
            ]);
        }
    }

    public function getMainParent($menuModel)
    {

    }
}