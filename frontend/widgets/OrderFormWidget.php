<?php
namespace frontend\widgets;

use backend\modules\order\models\Order;

class OrderFormWidget extends \yii\bootstrap\Widget
{

    public $title;
    public $user;
    public $form_id;

    public function init(){

    }

    public function run() {
        $model = new Order();
        return $this->render('order-form', [
            'title' => $this->title,
            'model' => $model,
            'user' => $this->user,
            'form_id' => $this->form_id,
        ]);
    }
}