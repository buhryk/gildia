<?php
namespace frontend\widgets;

use yii\base\Widget;
use backend\modules\core\models\Menu;
use yii\helpers\Url;

class SearchMenuWidget extends Widget
{
    public $menuItems;
    public $parent;
    public function init()
    {
        $query = Menu::find();
        $query->andWhere(['parent' => $this->parent]);
        $query->orderBy(['position' => SORT_ASC]);
        $this->menuItems = $query->all();
    }

    public function run()
    {
            return $this->render('search-menu', [
                'menuItems' => $this->menuItems
            ]);

    }

    public function getMainParent($menuModel)
    {

    }
}