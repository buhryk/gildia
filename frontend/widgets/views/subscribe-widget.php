<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Url;

$redirectUrl = Yii::$app->request->url;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/site/subscribe-form', 'rediractUrl' => $redirectUrl],
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'fieldConfig' => [
        'template' => "<span>{input}{error}</span>",
    ],
]); ?>

    <?= $form->field($model, 'email')
        ->input('email', ['placeholder' => Yii::t('common', 'Подпишитесь на нашу рассылку'),'class' => 'input'])->label(false); ?>
    <button type="submit"><?= Yii::t('common', 'ПОДПИСАТЬСЯ') ?></button>

<?php ActiveForm::end(); ?>


