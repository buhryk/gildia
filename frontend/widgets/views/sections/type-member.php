<?php
use backend\modules\slider\models\Slider;

$items = Slider::getSlider($slider);

?>


<section class="<?= $color ?>" >
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="container-100">
        <div class="template_4_accr">
            <?php foreach ($items as $item): ?>
            <div class="box-item setup">
                <div class="close"></div>
                <div class="left">
                    <img src="<?= $item->image ?>" alt="">
                </div>
                <div class="right">
                    <div class="small">
                        <?= $item->title ?><br>
                        <?= $item->post ?>
                    </div>
                    <div class="big">
                        <div class="coll">
                            <?= $item->description ?>
                        </div>

<!--                        <div class="coll">-->
<!--                            --><?//= $item->text ?>
<!--                        </div>-->
                        <?php if ($item->url) : ?>
                            <div class="bottom">
                                <a href="<?= $item->url ?>" class="btn-orange">СВЯЗАТЬСЯ С ПРЕДСТАВИТЕЛЕМ<br> КОМПАНИИ</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>