<?php

use backend\modules\slider\models\Slider;

$members = Slider::getSlider($slider);
?>

<section class="partners-subTop <?= $color ?>" style="background-image: url('/img/bg_1.png')">
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="container-100 txt-content">

        <div class="catalog-list double" style="padding-top: 0">
            <?php foreach ($members as $member): ?>
                <div class="box">
                    <img src="<?= $member->image ?>" alt="">
                    <h2><?= $member->title ?></h2>
                    <?= $member->description ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
