<?php
use backend\modules\faq\models\Faq;

$faq = Faq::find()->where(['status' => Faq::STATUS_ACTIVE, 'page_id' => $page->id])->orderBy('position')->all();

?>

<section class="template_4_faq <?= $color ?>" >
    <div class="container-100">
        <div class="left">

            <?php if ($section->title): ?>
                <div class="txt-content">
                    <h2><?= $section->title ?></h2>
                </div>
            <?php endif; ?>
            <aside class="acc">
                <?php foreach ($faq as $item): ?>
                    <p class="acc_title triangle"><?= $item->title ?></p>
                    <div class="acc_body txt-content" style="display: none;">
                        <?= $item->text ?>
                    </div>
                <?php endforeach; ?>
            </aside>
        </div>
        <div class="right">
            <img src="<?= $section->image ?>" alt="">
        </div>
    </div>
</section>
<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>