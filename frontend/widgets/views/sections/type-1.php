<?php

?>
<section class="template_4_double-coll small-padding <?= $color ?>" >
    <div class="container-100">
        <div class="left txt-content">
            <?php if ($section->title): ?>
                <h2><?= $section->title ?></h2>
            <?php endif; ?>
<!--            <h2>ИНДЕКС</h2>-->
        </div>
        <div class="right txt-content">
            <?= $section->text ?>
            <?php if ($section->btn_name) : ?>
                <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_'.$form.'"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>