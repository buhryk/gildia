<?php

?>

<section class="template_4_fulImg <?= $color ?>">
    <div class="left txt-content">
        <?php if ($section->title): ?>
            <h2><?= $section->title ?></h2>
        <?php endif; ?>
        <?= $section->text ?>
        <?php if ($section->btn_name) : ?>
            <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_'.$form.'"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
        <?php endif; ?>

    </div>
    <div class="right">
        <img src="<?= $section->image ?>" alt="">
    </div>
</section>
<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>