<?php

use backend\modules\table\models\Firm;
use backend\modules\table\models\Table;
use backend\modules\table\models\TableType;
use yii\helpers\Url;
use yii\widgets\Pjax;

//Pjax::begin(['timeout' => 10000]);

$firm_id = Yii::$app->request->get('firm');
$type_id = Yii::$app->request->get('type');

$url = Url::current();

$query = Table::find()->where([
        'status' => Table::STATUS_ACTIVE,
        'page_id' => $page->id,
]);

//if ($firm_id) {
//    $query->andFilterWhere(['firm_id' => $firm_id]);
//}
//if ($type_id) {
//    $query->andFilterWhere(['type_id' => $type_id]);
//}

$items = $query->orderBy('position')->all();

?>

<section class="<?= $color ?>" style="padding: 120px 0">
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="container-100">
        <div class="template_4_table">
            <div class="row-filter">
                <div class="select-box custom-select filter1">
                    <select>
                        <option value="---">Фильтр_1</option>
                        <?php foreach (TableType::getTypeAll() as $type): ?>
                        <option value="<?= $type->id ?>"><?= $type->title ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="select-box custom-select filter2">
                    <select >
                        <option value="---">Фильтр_2</option>
                        <?php foreach (Firm::getFirmAll() as $firm): ?>
                            <option  value="<?= $firm->id ?>"><?= $firm->title ?></option>
                        <?php endforeach; ?>
                    </select>

                </div>
            </div>
            <div class="scroll-table">
                <table>
                <tr>
                    <th>НАИМЕНОВАНИЕ</th>
                    <th>РАСКРОЙ</th>
                    <th>МАРКА СТАЛИ</th>
                    <th>СТАНДАРТ</th>
                    <th>ПОСТАВЩИК/ФИРМА</th>
                    <th>ОБНОВЛЕНО</th>
                </tr>
                <?php foreach ($items as $item): ?>
                    <tr data-filter1="<?= $item->type_id ?>" data-filter2="<?= $item->firm_id ?>">
                        <td><?= $item->title ?></td>
                        <td><?= $item->cut ?></td>
                        <td><?= $item->mark ?></td>
                        <td><?= $item->standard ?></td>
                        <td><?= $item->firm->title ?></td>
                        <td><?= date('d.m.Y', $item->updated_at) ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            </div>
        </div>
    </div>
</section>
<?php //Pjax::end(); ?>


<script>

</script>