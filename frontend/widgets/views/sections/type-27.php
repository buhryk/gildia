
<section class="template_4_with-title-full fullImg">
    <!--    Если не нужен заголовок то удалить весь блок title-full    -->
    <?php if ( $section->title ): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <!--    До этого момента    -->
    <div class="left txt-content">
        <?= $section->text ?>
    </div>
    <div class="right">
        <img src="<?= $section->image ?>" alt="">
    </div>

</section>
<section class="acc_and_filtr">
    <!--    Если не нужен заголовок то удалить весь блок title-full    -->
    <div class="title-full txt-content">
        <h2>Шаблон с заголовком</h2>
    </div>
    <!--    До этого момента    -->
    <div class="container-100">
        <aside class="acc">
            <p class="acc_title triangle">НАПРАВЛЕНИЯ ДЕЯТЕЛЬНОСТИ</p>
            <div class="acc_body" style="display: none;">
                <div class="check-box">
                    <input type="checkbox" id="cat_1_item_1">
                    <label for="cat_1_item_1">Направление 1</label>
                </div>
                <div class="check-box">
                    <input type="checkbox" id="cat_1_item_2">
                    <label for="cat_1_item_2">Направление 2</label>
                </div>
                <div class="check-box">
                    <input type="checkbox" id="cat_1_item_3">
                    <label for="cat_1_item_3">Направление 3</label>
                </div>
                <div class="check-box">
                    <input type="checkbox" id="cat_1_item_4">
                    <label for="cat_1_item_4">Направление 4</label>
                </div>
            </div>
            <p class="acc_title triangle">НАПРАВЛЕНИЯ ДЕЯТЕЛЬНОСТИ 2</p>
            <div class="acc_body">
                <div class="check-box">
                    <input type="checkbox" id="cat_2_item_1">
                    <label for="cat_2_item_1">Направление 1</label>
                </div>
                <div class="check-box">
                    <input type="checkbox" id="cat_2_item_2">
                    <label for="cat_2_item_2">Направление 2</label>
                </div>
                <div class="check-box">
                    <input type="checkbox" id="cat_2_item_3">
                    <label for="cat_2_item_3">Направление 3</label>
                </div>
                <div class="check-box">
                    <input type="checkbox" id="cat_2_item_4">
                    <label for="cat_2_item_4">Направление 4</label>
                </div>
            </div>
        </aside>
        <div class="template_4_accr">
            <div class="box-item setup">
                <div class="close"></div>
                <div class="left">
                    <img src="/img/logo.png" alt="">
                </div>
                <div class="right">
                    <div class="small">
                        <h2 class="title">НАЗВАНИЕ ПРОЕКТА</h2>
                        <p class="date">20.03.2019</p>
                        <p class="cat">Название категории</p>
                    </div>
                    <div class="big">
                        <div class="coll">
                            <h3>НАПРАВЛЕНИЕ ДЕЯТЕЛЬНОСТИ</h3>
                            <p>20.03.2019</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                        </div>
                        <div class="coll">
                            <h3>КОНТАКТЫ</h3>
                            <p>Почта</p>
                            <p>Телефон</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                        </div>
                        <div class="bottom">
                            <a href="##" class="btn-orange">СВЯЗАТЬСЯ С ПРЕДСТАВИТЕЛЕМ<br> КОМПАНИИ</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-item setup">
                <div class="close"></div>
                <div class="left">
                    <img src="/img/logo2.png" alt="">
                </div>
                <div class="right">
                    <div class="small">
                        <h2 class="title">НАЗВАНИЕ ПРОЕКТА</h2>
                        <p class="date">20.03.2019</p>
                        <p class="cat">Название категории</p>
                    </div>
                    <div class="big">
                        <div class="coll">
                            <h3>НАПРАВЛЕНИЕ ДЕЯТЕЛЬНОСТИ</h3>
                            <p>20.03.2019</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                        </div>
                        <div class="coll">
                            <h3>КОНТАКТЫ</h3>
                            <p>Почта</p>
                            <p>Телефон</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                        </div>
                        <div class="bottom">
                            <a href="##" class="btn-orange">СВЯЗАТЬСЯ С ПРЕДСТАВИТЕЛЕМ<br> КОМПАНИИ</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-item setup">
                <div class="close"></div>
                <div class="left">
                    <img src="/img/logo.png" alt="">
                </div>
                <div class="right">
                    <div class="small">
                        <h2 class="title">НАЗВАНИЕ ПРОЕКТА</h2>
                        <p class="date">20.03.2019</p>
                        <p class="cat">Название категории</p>
                    </div>
                    <div class="big">
                        <div class="coll">
                            <h3>НАПРАВЛЕНИЕ ДЕЯТЕЛЬНОСТИ</h3>
                            <p>20.03.2019</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                        </div>
                        <div class="coll">
                            <h3>КОНТАКТЫ</h3>
                            <p>Почта</p>
                            <p>Телефон</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                        </div>
                        <div class="bottom">
                            <a href="##" class="btn-orange">СВЯЗАТЬСЯ С ПРЕДСТАВИТЕЛЕМ<br> КОМПАНИИ</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-item setup">
                <div class="close"></div>
                <div class="left">
                    <img src="/img/logo2.png" alt="">
                </div>
                <div class="right">
                    <div class="small">
                        <h2>НАЗВАНИЕ ПРОЕКТА</h2>
                        <p>20.03.2019</p>
                        <p>Название категории</p>
                    </div>
                    <div class="big">
                        <div class="coll">
                            <h3>НАПРАВЛЕНИЕ ДЕЯТЕЛЬНОСТИ</h3>
                            <p>20.03.2019</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                        </div>
                        <div class="coll">
                            <h3>КОНТАКТЫ</h3>
                            <p>Почта</p>
                            <p>Телефон</p>
                            <p>Название категории</p>
                            <p>Название категории</p>
                        </div>
                        <div class="bottom">
                            <a href="##" class="btn-orange">СВЯЗАТЬСЯ С ПРЕДСТАВИТЕЛЕМ<br> КОМПАНИИ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="block_catalog triple nb_new">
    <div class="container-100">
        <h2 class="title_1">ПОСЛЕДНИЕ НОВОСТИ</h2>
        <div class="catalog-list">
            <div class="box">
                <img src="/img/single.jpg" alt="">
                <h2>ОЧЕНЬ ОЧЕНЬ ДЛИННОЕ НАЗВАНИЕ НОВОСТИ</h2>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <div class="bottom"><p>26.03.15</p><a href="##">ПОДРОБНЕЕ</a></div>
            </div>
            <div class="box">
                <img src="/img/single.jpg" alt="">
                <h2>ОЧЕНЬ ОЧЕНЬ ДЛИННОЕ НАЗВАНИЕ НОВОСТИ</h2>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <div class="bottom"><p>26.03.15</p><a href="##">ПОДРОБНЕЕ</a></div>
            </div>
            <div class="box">
                <img src="/img/single.jpg" alt="">
                <h2>ОЧЕНЬ ОЧЕНЬ ДЛИННОЕ НАЗВАНИЕ НОВОСТИ</h2>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <div class="bottom"><p>26.03.15</p><a href="##">ПОДРОБНЕЕ</a></div>
            </div>
            <div class="box">
                <img src="/img/single.jpg" alt="">
                <h2>ОЧЕНЬ ОЧЕНЬ ДЛИННОЕ НАЗВАНИЕ НОВОСТИ</h2>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <div class="bottom"><p>26.03.15</p><a href="##">ПОДРОБНЕЕ</a></div>
            </div><div class="box">
                <img src="/img/single.jpg" alt="">
                <h2>ОЧЕНЬ ОЧЕНЬ ДЛИННОЕ НАЗВАНИЕ НОВОСТИ</h2>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <div class="bottom"><p>26.03.15</p><a href="##">ПОДРОБНЕЕ</a></div>
            </div><div class="box">
                <img src="/img/single.jpg" alt="">
                <h2>ОЧЕНЬ ОЧЕНЬ ДЛИННОЕ НАЗВАНИЕ НОВОСТИ</h2>
                <p>Примеры реализации объектов строительства в сегментах недвижимости с констуктивными решениями под
                    любой тип зданий</p>
                <div class="bottom"><p>26.03.15</p><a href="##">ПОДРОБНЕЕ</a></div>
            </div></div>
    </div>
</section>