<?php

?>

<section class="<?= $class ?> <?= $color ?>" >
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="container-100">
        <?php if ($imageLeft): ?>
            <div class="left">
                <img src="<?= $section->image?>" alt="">
            </div>
            <div class="right txt-content">
                <?= $section->text?>
                <?php if ($section->btn_name) : ?>
                    <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_'.$form.'"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
                <?php endif; ?>
<!--                <a href="--><?//= $section->url?><!--" class="btn-orange">--><?//= $section->btn_name?><!--</a>-->
            </div>
        <?php else: ?>
        <div class="left txt-content">
            <?= $section->text ?>
            <?php if ($section->btn_name) : ?>
                <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_'.$form.'"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
            <?php endif; ?>
        </div>
        <div class="right ">
            <img src="<?= $section->image ?>" alt="">
        </div>
        <?php endif; ?>
    </div>
</section>
<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>

