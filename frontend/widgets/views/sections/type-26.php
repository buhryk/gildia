<?php

use backend\modules\slider\models\Slider;

$items = Slider::getSlider($slider);
?>
<section class="block_catalog triple nb_new <?= $color ?>">
    <div class="container-100">
        <?php if ( $section->title ): ?>
            <h2 class="title_1"><?= $section->title ?></h2>
        <?php endif; ?>

        <div class="catalog-list">
            <?php foreach ($items as $item): ?>
                <div class="box">
                    <img src="<?= $item->image ?>" alt="">
                    <h2><?= $item->title ?></h2>
                    <?= $item->description ?>
                    <?php if ($item->url) : ?>
                        <div class="bottom"><a href="<?= $item->url ?>">ПОДРОБНЕЕ</a></div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>