<?php
use backend\modules\slider\models\Slider;

$servisySlider = Slider::findOne($slider);
 ?>

<section class="template_4_services mTop <?= $color ?>">
    <?php if ($section->title): ?>
        <div class="top">
            <div class="container-100">
                <h2 class="title_1"><?= $section->title ?></h2>
            </div>
        </div>
    <?php endif; ?>

    <div class="bottom" >
        <?php foreach ($servisySlider->items as $item):?>
            <div class="box" style="background-image: url(<?= $item->image ?>)">
                <?= $item->title ?>
                <?= $item->description ?>
                <?php if ($item->url) : ?>
                    <a href="<?= $item->url ?>" class="btn-orange">ПОДРОБНЕЕ</a>
                 <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</section>

