<?php
use backend\modules\event\models\Event;
use yii\helpers\Html;
use yii\helpers\Url;

$events = Event::find()
    ->where(['active' => Event::ACTIVE_YES, 'id' => $calendar])
    ->orderBy('start_date desc')
    ->limit(4)
    ->all();
 ?>
<section class="template_4_calendar <?= $color ?>" style="flex-wrap: wrap" >
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="left txt-content">
        <?= $section->text ?>
        <?php if ($section->btn_name) : ?>
            <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_'.$form.'"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
        <?php endif; ?>
    </div>
    <div class="right-dates" style="background-image: url(<?= $section->image ?>)">
        <?php foreach ($events as $event): ?>
            <div class="box"  style="background-image: url(<?= $event['image'] ?>)">
                <h3><?= $event['title'] ?></h3>
                <?= $event['short_description'] ?>
                <div class="bottom">
                    <p><?= $event['start_date'] ?> <?= $event['start_time'] ?></p>
                    <a href="<?=$event['url'] ?>"  target="_blank" class="btn-orange">Регистрация</a>
                </div>

            </div>
        <?php endforeach; ?>
    </div>
</section>

<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>
