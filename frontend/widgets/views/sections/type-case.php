<?php
use backend\modules\slider\models\Slider;

$items = Slider::getSlider($slider);
?>

<section class="template_4_cases <?= $color ?>" >
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="container-100">
        <div class="left txt-content">
            <?= $section->text ?>
            <?php if ($section->btn_name) : ?>
                <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_'.$form.'"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
            <?php endif; ?>
        </div>
        <div class="right two-cases">
            <?php foreach ($items as $item): ?>
                <a href="<?= $item->url ?>" class="case-box">
                    <img src="<?= $item->image ?>" alt="">
                    <?= $item->title ?>
                    <?= $item->description ?>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php if ($form): ?>
    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>
<?php endif; ?>