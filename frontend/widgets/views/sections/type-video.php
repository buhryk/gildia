<?php



?>


<section class="template_4_defVideo <?= $color ?>" >
    <?php if ($section->title): ?>
        <div class="title-full txt-content">
            <h2><?= $section->title ?></h2>
        </div>
    <?php endif; ?>
    <div class="container-100">

        <div class="left txt-content">

            <?= $section->text ?>

            <?php if ($section->btn_name) : ?>
                <a href="<?= $section->url ?>" <?= $form ? 'data-popup="forma_'.$form.'"' : '' ?> class="btn-orange"><?= $section->btn_name ?></a>
            <?php endif; ?>

        </div>

        <div class="right">

            <div class="video">

<!--                <video poster="--><?//= $section->video_image ?><!--" autobuffer="" webkit-playsinline="false" playsinline="true">-->
<!---->
<!--                    -->
<!--                    <source src="--><?//= $section->video ?><!--" type="video/mp4">-->
<!---->
<!--                </video>-->


                <iframe width="1141" height="642" src="<?= $section->video ?>" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<!--                <div class="btn"></div>-->

            </div>

        </div>

    </div>

</section>

<?php if ($form): ?>

    <?= \frontend\widgets\FormWidget::widget(['form' => $form, 'form_title' => $section->title]); ?>

<?php endif; ?>