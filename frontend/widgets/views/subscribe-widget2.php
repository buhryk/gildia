<?php
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Url;

$redirectUrl = Yii::$app->request->url;
?>

<div class="container">
    <div class="form__wrapper to-animate">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="form">
                    <h2>Подпишитесь на рассылку</h2>
                    <?php $form = ActiveForm::begin([
                        'action' => ['/site/subscribe-form', 'rediractUrl' => $redirectUrl],
                        'fieldConfig' => [
                            'template' => "{input}{error}",
                        ],
                    ]); ?>

                    <?= $form->field($model, 'email',['enableAjaxValidation' => true])
                        ->input('email', ['placeholder' => Yii::t('common', 'Ваша почта')])->label(false); ?>
                    <button type="submit"><?= Yii::t('common', 'ПОДПИСАТЬСЯ') ?></button>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>




