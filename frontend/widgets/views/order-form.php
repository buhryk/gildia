<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use \backend\modules\custom_form\models\Form;

$redirectUrl = Yii::$app->request->url;
?>
<?php $form = ActiveForm::begin([
    'action' => ['/member/order',  'redirectUrl' => $redirectUrl],
//    'id' => 'order-form',
    'class' => 'order-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnType' => false,
    'validateOnChange' => false,
]) ?>
<?= $form->field($model, 'name', [
    'template' => '{label}{input}{error}',
    'options' => ['class' => 'input_dev_box'],
    'inputOptions' => [
        'class' => 'input',
        'value' => $user ? $user->firstname . ' ' . $user->lastname : false,
        'autofocus' => 'autofocus',
        'placeholder' => Yii::t('login', 'Имя'),
        'tabindex' => '1'
    ]
])->label(false);; ?>

<?= $form->field($model, 'email', [
    'template' => '{label}{input}{error}',
    'options' => ['class' => 'input_dev_box'],
    'inputOptions' => [
        'class' => 'input',
        'value' => $user ? $user->email : false,
        'autofocus' => 'autofocus',
        'placeholder' => Yii::t('login', 'E-mail'),
        'tabindex' => '2'
    ]
])->label(false); ?>

<?= $form->field($model, 'phone', [
    'template' => '{label}{input}{error}',
    'options' => ['class' => 'input_dev_box'],
    'inputOptions' => [
        'class' => 'input',
        'value' => $user ?  $user->phone : false,
        'autofocus' => 'autofocus',
        'placeholder' => Yii::t('login', 'Телефон'),
        'tabindex' => '3'
    ]
])->label(false); ?>

<?php if ($user): ?>
    <input type="hidden" name="Order[user_id]" value="<?= $user->id ?>">
<?php endif; ?>

<input type="hidden" name="Order[title]" value="<?= $title ?>">


<button type="submit" class="create_form"><?= Yii::t('common', 'Отправить')?></button>

<?php ActiveForm::end(); ?>


