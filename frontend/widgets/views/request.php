<?php

use backend\widgets\MainInputFile;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$redirectUrl = Yii::$app->request->url;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/request/create', 'redirectUrl' => $redirectUrl],
    'id' => 'request-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnType' => false,
    'validateOnChange' => false,
]) ?>


<?= $form->field($model, 'name', [
    'template' => '{label}{input}{error}',
    'options' => ['class' => 'input_dev_box'],
    'inputOptions' => [
        'autofocus' => 'autofocus',
        'class' => 'form__input login__input',
        'placeholder' => Yii::t('login', 'Имя'),
        'tabindex' => '1'
    ]
])->label(false); ?>

<?= $form->field($model, 'email', [
    'template' => '{label}{input}{error}',
    'options' => ['class' => 'input_dev_box'],
    'inputOptions' => [
        'autofocus' => 'autofocus',
        'class' => 'form__input login__input',
        'placeholder' => Yii::t('login', 'E-mail'),
        'tabindex' => '2'
    ]
])->label(false); ?>

<?= $form->field($model, 'phone', [
    'template' => '{label}{input}{error}',
    'options' => ['class' => 'input_dev_box'],
    'inputOptions' => [
        'autofocus' => 'autofocus',
        'type' => 'tel',
        'class' => 'form__input login__input',
        'placeholder' => Yii::t('login', 'Телефон'),
        'tabindex' => '3'
    ]
])->label(false); ?>

<?= $form->field($model, 'text', [
    'template' => '{label}{input}{error}',
    'options' => ['class' => 'input_dev_box'],
    'inputOptions' => [
        'autofocus' => 'autofocus',
        'class' => 'form__input login__input',
        'placeholder' => Yii::t('login', 'Сообщение'),
        'tabindex' => '4'
    ]
])->textarea()->label(false); ?>

<input type="hidden" name="RequestForm[form_title]" value="<?= $form_title ?>">

<button type="submit"><?= Yii::t('common', 'Отправить')?></button>

<?php ActiveForm::end(); ?>
