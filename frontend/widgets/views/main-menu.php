<?php
use backend\modules\core\models\PageLang;
use common\widgets\WLang;
use yii\helpers\Url;
use backend\modules\core\models\Menu;
?>

<?php foreach ($menuItems as $item): ?>

        <li class="nav-item">
            <a class="nav-link" href="<?= Url::to($item->getUrl(true), true); ?>"><?=$item->name ?></a>
        </li>

<?php endforeach; ?>
