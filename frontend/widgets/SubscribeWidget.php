<?php
namespace frontend\widgets;

use backend\modules\core\models\Setting;
use backend\modules\email_delivery\models\Subscriber;
use backend\modules\email_delivery\models\Subscriber2;
use yii\base\Widget;

class SubscribeWidget extends Widget
{

    public $news = false;

    public function run()
    {

        $model = new Subscriber();

        if ($this->news) {
            return $this->render('subscribe-widget2', [
                'model' => $model,
            ]);
        } else {
            return $this->render('subscribe-widget', [
                'model' => $model,
            ]);
        }

    }
}