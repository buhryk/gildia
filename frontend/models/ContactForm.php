<?php
namespace frontend\models;

use backend\modules\core\models\Setting;
use function Sodium\crypto_kx;
use yii\base\Model;
use Yii;

class ContactForm extends Model
{
    public $name;
    public $contactInfo;
    public $message;

    public $reCaptcha;

    public function rules()
    {
        return [
            [['name', 'contactInfo'], 'required'],
            [['name', 'contactInfo', 'message'], 'string'],
            [['name', 'contactInfo'], 'string', 'max' => 100],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(),
                'secret' => '6LeHGj4UAAAAAA-5tff76si_Si8WpwX4ZY-8zbqR', 'uncheckedMessage' => Yii::t('common', 'Будь ласка, підтвердьте, що ви не бот.')]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('common', 'Ім\'я'),
            'contactInfo' =>  Yii::t('common', 'Email\Телефон'),
            'message' => Yii::t('common', 'Повідомлення'),

        ];
    }

    public function send()
    {
        if (! $this->validate()) {
            print_r($this);
            return false;
        }

        $emailTo = Setting::getByKey('email');
        $email = [];
        foreach ($emailTo as $item) {
            if (filter_var($item, FILTER_VALIDATE_EMAIL)){
                $email[] = $item;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'contactForm'],
                ['contact' => $this])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($email)
            ->setSubject('Заповнили контакну форму.')
            ->send();
    }

}