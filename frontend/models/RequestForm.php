<?php
namespace frontend\models;

use backend\modules\request\models\Request;
use yii\base\Model;
use Yii;

/**
 * Request form
 */
class RequestForm extends Model
{
    public $form_title;
    public $name;
    public $phone;
    public $email;
    public $type;
    public $text;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'required', 'message' => Yii::t('request', 'Необходимо заполнить "{attribute}"')],
            [['name', 'phone'], 'trim'],
            ['phone', 'match', 'pattern' => "/^\+38\(0[0-9]{2}\)[0-9]{3}-[0-9]{2}-[0-9]{2}$/",
                'message' => Yii::t('request', 'Поле "{attribute}" должно быть записано в формате "+38(0XX)XXX-XX-XX"')],
            ['text', 'string', 'max' => 1000],
            ['email', 'email', 'message' => Yii::t('request', 'Поле "{attribute}" введено некорректно')],
            ['email', 'string', 'max' => 55],
            ['form_title', 'string'],
            ['type', 'in', 'range' => array_keys(Request::getAllTypes())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('request', 'Имя'),
            'phone' => Yii::t('request', 'Телефон'),
            'email' => Yii::t('request', 'Email'),
            'type' => Yii::t('request', 'Предмет заявки'),
            'text' => Yii::t('request', 'Сообщение'),
        ];
    }

    public function createRequest()
    {
        if ($this->validate()) {
            $request = new Request();
            $request->form_title = $this->form_title;
            $request->name = $this->name;
            $request->phone = $this->phone;
            $request->email = $this->email;
            $request->type = $this->type;
            $request->text = $this->text;

            if ($request->save()) {
                $request->notifyAboutNewRequest();
                return $request;
            }
        }

        return null;
    }
}