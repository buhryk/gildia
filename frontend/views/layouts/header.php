<?php
use common\widgets\WLang;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\RegistrationForm;
use frontend\models\User;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$model = \Yii::createObject(LoginForm::className());
$userModel = new RegistrationForm();
$resetModel = new PasswordResetRequestForm();
?>

<header>
    <div class="contacts">
        <div class="container">
            <div class="row">
                <?= \backend\modules\core\models\Widget::getWidgetByKey('header-block-contact') ?>

                <div class="col-md-5">
                    <div class="language">
                        <!--    Start lang-->
                        <?= WLang::widget();?>
                        <!--    End lang-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="logo">
                        <a href="<?=Url::to(['/']) ?>">
                            <?= \backend\modules\core\models\Widget::getWidgetByKey('logo') ?>
                        </a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12">
                    <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
                            aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i
                                    class="fas fa-bars fa-1x"></i></span></button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent1">
                        <ul class="header-menu navbar-nav mr-auto">
                            <?= \frontend\widgets\MainMenuWidget::widget() ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-1">
                    <?php
                    if (!\Yii::$app->user->isGuest): ?>
                        <a href="<?= Url::to(['/member/index']) ?>" class="user" >
                            <div><i class="fas fa-user-alt"></i></div>
                        </a>
                    <?php else: ?>
                        <div class="user" data-popup-btn="login">
                            <div><i class="fas fa-user-alt"></i></div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>


    <div class="popup-dev" data-popup="login">
        <div class="popup-dev__container">
            <h2><?= Yii::t('common', 'Вход в личный кабинет')?></h2>
            <?php $form = ActiveForm::begin([
                'action' => ['/member/login'],
                'id' => 'login-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]) ?>
            <?= $form->field($model, 'login', [
                'template' => '{label}{input}{error}',
                'options' => ['class' => 'input_dev_box'],
                'inputOptions' => [
                    'autofocus' => 'autofocus',
                    'class' => 'form__input login__input',
                    'placeholder' => Yii::t('login', 'E-mail'),
                    'id' => 'enter-login',
                    'tabindex' => '1'
                ]
            ]); ?>
            <?= $form->field($model, 'password', [
                'template' => '{label}{input}{error}',
                'options' => ['class' => 'input_dev_box'],
                'inputOptions' => [
                    'type' => 'password',
                    'class' => 'form__input login__input',
                    'placeholder' => Yii::t('login', 'Пароль'),
                    'id' => 'enter-pass',
                    'tabindex' => '2'
                ]
            ]) ?>
            <input type="submit" class="btn-form-red btn-orange" value="Войти">

            <p data-popup-btn="reset"><?= Yii::t('common', 'Забыли пароль?')?></p>
            <p class="dop-info">
                <?= Yii::t('common', 'У Вас еще нету аккаунта?')?> <a href="##" data-popup-btn="registration"> <?= Yii::t('common', 'Регистрация')?></a>
            </p>
            <?php ActiveForm::end(); ?>

            <div class="exit-btn"></div>
        </div>
    </div>

    <div class="popup-dev" data-popup="registration">
        <div class="popup-dev__container">
            <h2><?= Yii::t('common', 'Регистрация')?></h2>

            <?php $form = ActiveForm::begin([
                'action' => ['/member/register'],
                'id' => 'register-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]) ?>
            <?= $form->field($userModel, 'firstname', [
                'template' => '{label}{input}{error}',
                'options' => ['class' => 'input_dev_box'],
                'inputOptions' => [
                    'autofocus' => 'autofocus',
                    'class' => 'form__input login__input',
                    'placeholder' => Yii::t('login', 'Имя'),
                    'tabindex' => '1'
                ]
            ]); ?>

            <?= $form->field($userModel, 'lastname', [
                'template' => '{label}{input}{error}',
                'options' => ['class' => 'input_dev_box'],
                'inputOptions' => [
                    'autofocus' => 'autofocus',
                    'class' => 'form__input login__input',
                    'placeholder' => Yii::t('login', 'Фамилия'),
                    'tabindex' => '2'
                ]
            ]); ?>

            <?= $form->field($userModel, 'phone', [
                'template' => '{label}{input}{error}',
                'options' => ['class' => 'input_dev_box'],
                'inputOptions' => [
                    'autofocus' => 'autofocus',
                    'class' => 'form__input login__input',
                    'placeholder' => Yii::t('login', 'Телефон'),
                    'tabindex' => '3'
                ]
            ]); ?>

            <?= $form->field($userModel, 'email', [
                'template' => '{label}{input}{error}',
                'options' => ['class' => 'input_dev_box'],
                'inputOptions' => [
                    'autofocus' => 'autofocus',
                    'class' => 'form__input login__input',
                    'placeholder' => Yii::t('login', 'E-mail'),
                    'tabindex' => '4'
                ]
            ]); ?>

            <?= $form->field($userModel, 'password', [
                'template' => '{label}{input}{error}',
                'options' => ['class' => 'input_dev_box'],
                'inputOptions' => [
                    'type' => 'password',
                    'autofocus' => 'autofocus',
                    'class' => 'form__input login__input',
                    'placeholder' => Yii::t('login', 'Пароль'),
                    'tabindex' => '5'
                ]
            ]); ?>

            <input type="submit" class="btn-form-red" value="<?= Yii::t('common', 'Зарегистрироваться')?>">
            <p class="dop-info">
                <?= Yii::t('common', 'У Вас уже есть аккаунт?')?> <a href="##" data-popup-btn="login"> <?= Yii::t('common', 'Войти')?></a>
            </p>
            <?php ActiveForm::end(); ?>

            <div class="exit-btn"></div>
        </div>
    </div>

    <div class="popup-dev" data-popup="reset">
        <div class="popup-dev__container">
            <h2><?= Yii::t('common', 'Сброс пароля')?></h2>
            <?php $form = ActiveForm::begin([
                'action' => ['/member/reset'],
                'id' => 'reset-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]) ?>
            <?= $form->field($resetModel, 'email', [
                'template' => '{label}{input}{error}',
                'options' => ['class' => 'input_dev_box'],
                'inputOptions' => [
                    'autofocus' => 'autofocus',
                    'class' => 'form__input login__input',
                    'placeholder' => Yii::t('login', 'E-mail'),
                    'tabindex' => '1'
                ]
            ]); ?>

            <input type="submit" class="btn-form-red btn-orange" value="<?= Yii::t('common', 'Отправить')?>">

            <?php ActiveForm::end(); ?>

            <div class="exit-btn"></div>
        </div>
    </div>

</header>

<?php if( Yii::$app->session->hasFlash('success') ): ?>
    <div class="alert alert-success alert-dismissible" role="alert" style="display: block">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif;?>
