<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\modules\core\models\Setting;
use common\models\Lang;
use frontend\assets\CatalogAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\widgets\BreadcrumbsWidget;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= Setting::getValueByKey('tag_manager_head')?>
</head>
<body>
<?php $this->beginBody() ?>
<?= Setting::getValueByKey('tag_manager_body')?>

<?= $this->render('header');?>
<?=$content?>
<?= $this->render('footer'); ?>

<?= Setting::getValueByKey('tag_manager_body_end')?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
