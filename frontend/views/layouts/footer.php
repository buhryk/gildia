<?php

use backend\modules\core\models\Setting;

?>

<footer>
    <div class="container to-animate">
        <div class="row">
            <div class="col-md-2">
                <div class="footer__menu">
                    <ul>
                        <?= \frontend\widgets\MainMenuWidget::widget(['type' => 'footer']) ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="contacts__wrapper">
                    <div class="form">
                        <?= \frontend\widgets\SubscribeWidget::widget() ?>
                    </div>
                    <div class="contacts">
                        <?= \backend\modules\core\models\Widget::getWidgetByKey('footer-block-contact') ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="socials">
                    <?php
                    $iconsItems = Setting::getDataByKey(Setting::FOOTER_SOCIAL_ICONS, Setting::ADDITIONAL_DATA_ITEMS);
                    ?>
                    <?php if ($iconsItems): ?>
                        <?php foreach ($iconsItems as $item): ?>

                            <?php if ( $item['icons'] == 'fa-viber'): ?>

                                <? if(check_mobile_device()) :?>
                                    <a href="viber://add?number=<?= $item['link'] ?>" target="_blank">
                                <? else : ?>
                                    <a href="viber://add?number=<?= $item['link'] ?>" target="_blank">
                                <? endif; ?>

                            <?php else: ?>
                            <a href="<?= $item['link'] ?>" target="_blank">
                            <?php endif; ?>
                                <i class="fab <?= $item['icons'] ?>"></i>
                            </a>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
                <div class="site">
                    <a href="<?= \yii\helpers\Url::to(['/']) ?>">guild.com.ua</a>
                </div>
            </div>
        </div>
    </div>
</footer>