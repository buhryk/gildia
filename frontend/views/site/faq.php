<section class="news news__archive faq__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="news__archive-heading to-animate">
                    <h2><?= $page->title ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="news__archive-cats to-animate">
                    <h3><?= Yii::t('common', 'Категории')?>:</h3>
                    <div class="cat-dd">
                        <ul>
                            <li class="active"><a href="<?= \yii\helpers\Url::to(['/site/faq']) ?>"><?= Yii::t('common', 'Все')?></a></li>
                            <?php foreach ($categories as $k => $category): ?>
                                <li><a href="<?= \yii\helpers\Url::to(['/site/faq', 'category_id' => $k]) ?>"><?= $category ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="accordion" id="accordionExample">
                    <?php foreach ($faqs as $faq): ?>
                        <div class="card to-animate">
                            <div class="card-header" id="heading-<?= $faq->id ?>">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse-<?= $faq->id ?>" aria-expanded="false" aria-controls="collapseTwo">
                                        <?= $faq->title ?>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapse-<?= $faq->id ?>" class="collapse" aria-labelledby="heading-<?= $faq->id ?>" data-parent="#accordionExample">
                                <div class="card-body single__content">
                                    <?= $faq->text ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

    </div>
</section>
<div class="container">
    <section class="ask__question to-animate">
        <div class="row">
            <div class="col-md-6">
                <div class="contact__form">
                    <h2><?= Yii::t('common', 'Задать свой вопрос')?></h2>
                    <p><?= Yii::t('common', 'Если вы не нашли ответа на свой вопрос, напишите его нам через форму.')?></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contacts__section" style="padding-bottom: 0">
                    <div class="contact__form">
                        <?= \frontend\widgets\RequestWidget::widget(['form_title' => 'FAQ форма']) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
