<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
    <h1><?= Yii::t('common', '404')?></h1>
    <p><?= Yii::t('common', 'Такой страницы не существует')?></p>
    <a class="btn-orange" href="<?= \yii\helpers\Url::to(['/']) ?>"><?= Yii::t('common', 'На главную')?></a>
</div>
