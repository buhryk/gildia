<?php

use backend\modules\core\models\Setting;
use yii\helpers\Url;

?>
<section class="contacts__section to-animate">
    <div class="line-wrapper">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="contacts__heading">
                <h2 class=" to-animate"><?= $page->title ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="contacts__data to-animate">
                    <?= $page->description ?>
                </div>
                <div class="contact__form to-animate">
                    <span><?= Yii::t('common', 'Задайте вопрос тут')?></span>
                    <?= \frontend\widgets\RequestWidget::widget(['form_title' => 'Контакт форма']) ?>
                </div>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="contact__map to-animate">
                    <?= Setting::getValueByKey('map')?>
                </div>
            </div>
        </div>
    </div>
</section>

