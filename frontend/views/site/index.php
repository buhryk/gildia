<section class="hero__section to-animate">
    <div class="container">
        <div class="line-wrapper">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="caption">
                    <h1 class=" to-animate"><?= Yii::t('common', 'Гильдия')?></h1>
                    <h2 class=" to-animate"><?= Yii::t('common', 'Мы единый «центр развития  бизнеса», объединивший в себе сотни лучших компаний и профессионалов.')?></h2>
                    <div class="form to-animate">
                        <form name="test" method="get" action="<?= \yii\helpers\Url::to(['/catalog/catalog/category']) ?>">
                            <input type="text" name="search" placeholder="<?= Yii::t('common', 'Введите ваш запрос')?>">
                            <button type="submit"><?= Yii::t('common', 'Найти Услугу')?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="work">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="work__heading">
                    <h2 class=" to-animate"><?= Yii::t('common', 'В каких сферах мы работаем')?></h2>
                </div>
            </div>
        </div>
        <div class="row">

            <?php foreach ($inWhichAreas as $item): ?>
                <div class="col-md-4 to-animate">
                    <div class="work__item">
                        <a href="<?= $item->url ?>"><h3><?= $item->title ?></h3></a>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="work__button to-animate">
                    <a href="" class="button "><?= Yii::t('common', 'Все услуги')?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="routine__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="routine__section-info to-animate">
                    <h2><?= $my_izbavim->title ?></h2>
                    <?= $my_izbavim->text ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="how-work__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="how-work__heading to-animate">
                    <h2><?= Yii::t('common', 'КАК МЫ РАБОТЕМ')?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php $i = 1; foreach ($howWeWork as $item): ?>
                <div class="col-md-3 to-animate">
                    <div class="how-work__item">
                        <?= $item->text ?>
                        <div class="number">0<?= $i ?></div>
                    </div>
                </div>
            <?php $i++; endforeach; ?>

        </div>
    </div>
</section>
<section class="why-us__section to-animate">
    <div class="line-wrapper">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="why-us__heading to-animate">
                    <h2><?= Yii::t('common', 'Почему мы?')?></h2>
                </div>
            </div>
        </div>
        <?php foreach (array_chunk($why_we, 3) as $items): ?>
            <div class="row">
                <?php foreach ($items as $item): ?>
                    <div class="col-md-4">
                        <div class="why-us__item to-animate">
                            <div class="why-us__item-icon">
                                <img src="<?= $item->image ?>" alt="<?= $item->title ?>">
                            </div>
                            <div class="why-us__item-text">
                                <h2><?= $item->title ?></h2>
                                <?= $item->text ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>

    </div>
</section>
<section class="why-it__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="why-it__heading to-animate">
                    <h2><?= Yii::t('common', 'Почему это работает?')?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="why-it__list">
                    <ul>
                        <?php foreach ($why_does_it_work as $item): ?>
                            <li class=" to-animate">
                                <?= $item->text ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="why-it__button to-animate">
                    <a href="" class="button"><?= Yii::t('common', 'Зарегистрироваться')?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="best__section to-animate">
    <div class="container">
        <div class="col-md-12">
            <div class="best__heading">
                <h2><?= $luchshie_spetsialisty->title ?></h2>
                <?= $luchshie_spetsialisty->text ?>
            </div>
        </div>
    </div>
</section>
<section class="project">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="project__heading to-animate">
                    <h2><?= $o_proekte->title ?></h2>
                </div>
            </div>
        </div>
        <div class="row to-animate">
            <div class="col-md-4">
                <div class="project__image">
                    <img src="<?= $o_proekte->image ?>" alt="<?= $o_proekte->title ?>">
                </div>
            </div>
            <div class="col-md-8 line-fix">
                <div class="project__text">
                    <?= $o_proekte->text ?>
                    <div class="project__button">
                        <a href="<?= $o_proekte->url ?>" class="button"><?= $o_proekte->btn_name ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (!empty($lastNews)): ?>
    <section class="news">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="news__heading to-animate">
                        <h2><?= Yii::t('common', 'Наши новости')?></h2>
                        <h3><?= Yii::t('common', 'Здесь будет информация о нашем блоге, там много интересного, можете почитать')?></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <?= \Yii::$app->view->renderFile('@frontend/modules/news/views/news/news-items.php', ['models' => $lastNews]); ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="news__button to-animate">
                        <a href="<?= \yii\helpers\Url::to(['/news'])?>" class="button"><?= Yii::t('common', 'Все новости')?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>




