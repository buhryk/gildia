<?php


use backend\widgets\MainInputFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>

<section class="cab__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cab__heading">
                    <h2><?= Yii::t('common', 'Личный кабинет')?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="cab__profile">
                    <div class="cab__add-wrapper">
                        <a href=""><?= Yii::t('common', 'Инфо')?></a>
                        <div class="nb__dots">
                            <span></span>
                            <div class="info-w">
                                <div class="white">
                                    <a href="##" data-popup-btn="edit"><?= Yii::t('common', 'Редактировать профиль')?></a>
                                    <a href="##" data-popup-btn="password-change" ><?= Yii::t('common', 'Изменить пароль')?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cab__image">
                        <img src="<?= $user->image ?>" alt="">
                    </div>
                    <div class="cab__name">
                        <h3><?= $user->firstname ?> <?= $user->lastname ?></h3>
                    </div>
                    <div class="cab__button">
                        <a href="<?= \yii\helpers\Url::to(['/catalog/catalog/index']) ?>" class="button"><?= Yii::t('common', 'Заказать услугу')?></a>
                    </div>
                    <div class="cab__button">
                        <?= Html::a('ВЫХОД', ['member/logout'], [
                            'class' => 'acc_title',
                            'onclick' => 'location.href=\'/member/logout\''
                        ]) ?>
                    </div>
                    <div class="cab__contact">
                        <div class="cab__contact-mail">
                            <p><?= Yii::t('common', 'Почта')?></p>
                            <a href=""><?= $user->email ?></a>
                        </div>
                        <div class="cab__contact-phone">
                            <p><?= Yii::t('common', 'Телефон')?></p>
                            <a href=""><?= $user->phone ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="cab__history">
                    <h3><?= Yii::t('common', 'История заказов')?></h3>
                    <div class="cab__history-wrapper">
                        <?php foreach ($orders as $order): ?>
                            <div class="cab__history-item">
                                <h3><?= $order->title ?></h3>
                                <div class="status">
                                    <span class="done"><?= $order->statusDetail ?></span>
                                    <p><?= date('d.m.Y',$order->created_at) ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
                <div class="cab__history">
                    <h3><?= Yii::t('common', 'Документы')?></h3>
                    <div class="cab__history-wrapper">
                        <?php foreach ($documents as $document): ?>
                            <?php
                                $extension = end(explode('.', $document->file));

                            ?>
                            <div class="cab__history-item docs-item">
                                <a href="<?= $document->file ?>" download class="pdf"><?= mb_strtoupper($extension) ?></a>
                                <h3><?= $document->name ?></h3>
                                <p><?= date('d.m.Y', $document->created_at) ?></p>
                                <div class="nb__dots">
                                    <span></span>
                                    <div class="info-w">
                                        <div class="white">
                                            <a href="<?= \yii\helpers\Url::to(['member/delete', 'file_id' => $document->id]) ?>"><?= Yii::t('common', 'Удалить')?></a>
                                            <a href="<?= $document->file ?>" download><?= Yii::t('common', 'Скачать')?></a>
                                            <a href="<?= \yii\helpers\Url::to(['member/index', 'file_id' => $document->id]) ?>"><?= Yii::t('common', 'Загрузить новый')?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="cab__button">
                            <a href="##" data-popup-btn="new-file" class="button"><?= Yii::t('common', 'Добавить новый файл')?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="popup-dev" data-popup="edit">
    <div class="popup-dev__container">
        <h2><?= Yii::t('common', 'Редактирование профиля')?></h2>

        <?php $form = ActiveForm::begin([
            'action' => ['/member/edit'],
            'id' => 'edit-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnBlur' => false,
            'validateOnType' => false,
            'validateOnChange' => false,
        ]) ?>
        <?= $form->field($userModel, 'firstname', [
            'template' => '{label}{input}{error}',
            'options' => ['class' => 'input_dev_box'],
            'inputOptions' => [
                'autofocus' => 'autofocus',
                'class' => 'form__input login__input',
                'placeholder' => Yii::t('login', 'Имя'),
                'tabindex' => '1'
            ]
        ]); ?>

        <?= $form->field($userModel, 'lastname', [
            'template' => '{label}{input}{error}',
            'options' => ['class' => 'input_dev_box'],
            'inputOptions' => [
                'autofocus' => 'autofocus',
                'class' => 'form__input login__input',
                'placeholder' => Yii::t('login', 'Фамилия'),
                'tabindex' => '2'
            ]
        ]); ?>

        <?= $form->field($userModel, 'phone', [
            'template' => '{label}{input}{error}',
            'options' => ['class' => 'input_dev_box'],
            'inputOptions' => [
                'autofocus' => 'autofocus',
                'class' => 'form__input login__input',
                'placeholder' => Yii::t('login', 'Телефон'),
                'tabindex' => '3'
            ]
        ]); ?>

        <?= $form->field($userModel, 'image')->widget(MainInputFile::className(), [
            'language'      => 'ru',
            'path'          => 'user/' . $userModel->id,
            'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template'      => '<div class="file-input-image"><div class="img" style="width: 100px">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
            'options'       => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple'      => false       // возможность выбора нескольких файлов
        ])?>

        <input type="submit" class="btn-form-red" value="<?= Yii::t('common', 'Сохранить')?>">

        <?php ActiveForm::end(); ?>

        <div class="exit-btn"></div>
    </div>
</div>

<div class="popup-dev" data-popup="password-change">
    <div class="popup-dev__container">
        <h2><?= Yii::t('common', 'Смена пароля')?></h2>

        <?php $form = ActiveForm::begin([
            'action' => ['/member/password-change'],
            'id' => 'passwordChange',

        ]) ?>
        <?= $form->field($modelChange, 'newPassword', [
            'template' => '{label}{input}{error}',
            'options' => ['class' => 'input_dev_box'],
            'inputOptions' => [
                    'type' => 'password',
                'placeholder' => Yii::t('login', 'Новый пароль'),
                'tabindex' => '1'
            ]
        ]); ?>

        <?= $form->field($modelChange, 'newPasswordRepeat', [
            'template' => '{label}{input}{error}',
            'options' => ['class' => 'input_dev_box'],
            'inputOptions' => [
                'type' => 'password',
                'placeholder' => Yii::t('login', 'Повторить новый пароль'),
                'tabindex' => '2'
            ]
        ]); ?>

        <input type="submit" class="btn-form-red" value="<?= Yii::t('common', 'Сохранить')?>">

        <?php ActiveForm::end(); ?>

        <div class="exit-btn"></div>
    </div>
</div>

<div class="popup-dev" style="display: <?= $file_id ? 'block' : 'none'?>" data-popup="new-file">
    <div class="popup-dev__container">
        <h2><?= Yii::t('common', 'Загрузка файла')?></h2>

        <?php
        $action = $file_id ? ['/member/new-file', 'file_id' => $file_id] : ['/member/new-file'];
        $form = ActiveForm::begin([
            'action' => $action,
            'id' => 'new-file',

        ]) ?>

        <?= $form->field($document_new, 'name', [
            'template' => '{label}{input}{error}',
            'options' => ['class' => 'input_dev_box'],
            'inputOptions' => [
                'autofocus' => 'autofocus',
                'class' => 'form__input login__input',
                'placeholder' => Yii::t('login', 'Название файла'),
                'tabindex' => '3'
            ]
        ]); ?>

        <?= $form->field($document_new, 'file')->widget(MainInputFile::className(), [
            'language'      => 'ru',
            'path'          => 'user/' . $userModel->id,
            'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options'       => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple'      => false       // возможность выбора нескольких файлов
        ])?>
        <input type="hidden" name="Document[user_id]" value="<?= $userModel->id ?>">

        <input type="submit" class="btn-form-red" value="<?= Yii::t('common', 'Сохранить')?>">

        <?php ActiveForm::end(); ?>

        <div class="exit-btn"></div>
    </div>
</div>
