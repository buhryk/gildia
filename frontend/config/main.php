<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'en',
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'news' => [
            'class' => 'frontend\modules\news\Module',
        ],
        'catalog' => [
            'class' => 'frontend\modules\catalog\Module',
        ],
        'cabinet' => [
            'class' => 'frontend\modules\cabinet\Module',
            'defaultRoute' => 'profile/index'
        ],
    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'baseUrl' => '/uploads',
                'basePath' => '@frontend/web/uploads',
                //'path' => '/uploads/images/news/',
                'name' =>'Files'
            ],
        ],
    ],
    'components' => [
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LeHGj4UAAAAAL3ozyDiJ77T1xgiTx6Ui_rsH7Cx',
            'secret' => '6LeHGj4UAAAAAA-5tff76si_Si8WpwX4ZY-8zbqR',
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
            'class' => 'frontend\components\LangRequest'
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl' => ['member/login']
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
//            'urlFormat'=>'path',
            'showScriptName'=>false,
            'enablePrettyUrl' => true,
//            'showScriptName' => false,
            'rules' => [
                'sitemap.xml' => 'sitemap/index',
                'news/<alias>' => 'news/news/view',
                'news' => 'news/news/all',
                'services/category/<alias>' => 'catalog/catalog/category',
                'services/search/<search>' => 'catalog/catalog/category',
                'services/<alias>' => 'catalog/catalog/view',
                'services' => 'catalog/catalog/index',
                '<alias>' => 'page/view',
            ],
        ],
        'thumbnail' => [
            'class' => 'common\components\Thumbnail',
            'basePath' => '@webroot',
            'prefixPath' => '/',
            'cachePath' => 'uploads/thumbnails'
        ],
    ],
    'params' => $params,
];
