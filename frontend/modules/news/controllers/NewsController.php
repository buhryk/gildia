<?php

namespace frontend\modules\news\controllers;

use backend\modules\core\models\Page;
use backend\modules\email_delivery\models\Subscriber;
use backend\modules\event\models\Event;
use backend\modules\images\models\Image;
use backend\modules\news\models\NewsCategory;
use common\models\Lang;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use backend\modules\news\models\News;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `news` module
 */
class NewsController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public  $childrenIds = [];

    public function actionAll($category = false)
    {

        $query = News::find()->leftJoin('news_to_category', 'id = news_id')->where([
            'status' => News::STATUS_ACTIVE
        ]);
        if ($category) {
            $query->andWhere(['category_id' => $category]);
        }


        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $models = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $categories = NewsCategory::getCategoryAll();


//        pr($categories);

            return $this->render('all', [
                'subscribe' => $model,
                'models' => $models,
                'category_id' => $category,
                'categories' => $categories,
                'pagination' => $pagination,
            ]);


    }

    public function actionCategory($alias)
    {
        $ids = Yii::$app->request->get('categories_id');
        $letter = Yii::$app->request->get('letter');

        if(!$ids) {
            $category = $this->findCategory($alias);
            $this->childrenIds[] = $category->id;

            $ids =  $this->getChildrenIds($category->childrens);
        }

        $query = News::find();
        $query->andWhere(['status' => News::STATUS_ACTIVE]);
        $query->andWhere(['category_id' => $ids]);
        $query->orderBy(['id' => SORT_DESC]);

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $models = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $model = new News();

        $letters = $model->getLettersItem($ids);

        if ($letter) {
            $models = $model->getItemByLetter($letter, $ids);

        }
//        pr($news);

        if (Yii::$app->request->isAjax) {

            $html = $this->renderAjax('news-items', ['models' => $models, 'pagination' => $pagination]);

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'html' => $html,
            ];
        }


        return $this->render('category', [
            'models' => $models,
            'pagination' => $pagination,
            'childrens' => $category->childrens,
            'letters' => $letters,
            'category' => $category
        ]);
    }


    public function getChildrenIds($model)
    {
        if($model){
            foreach ($model as $children){
                $this->childrenIds[] = $children->id;
                $this->getChildrenIds($children->childrens);
            }

        }

        return $this->childrenIds;
    }


    public function actionView($alias)
    {
        $model = $this->findModel($alias);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionEvent($alias)
    {
        $model = Event::findOne(['alias' => $alias]);
//        pr($model);
        return $this->render('event', [
            'model' => $model,
        ]);
    }

    protected function findCategory($alias)
    {
        if (($model = NewsCategory::find()->where(['alias' => $alias, 'status' => NewsCategory::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function findModel($alias)
    {
        if (($model = News::find()->where(['alias' => $alias, 'status' => News::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function creationQuery()
    {
        $query = News::find();
        $query->andWhere(['status' => News::STATUS_ACTIVE]);
        $query->orderBy(['id' => SORT_DESC]);

        return $query;
    }
}
