<?php

namespace frontend\modules\news;

/**
 * news module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\news\controllers';

//   public $layout = '/news';


    public function init()
    {
        parent::init();

    }
}
