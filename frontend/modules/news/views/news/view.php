<?php

use frontend\widgets\SubscribeWidget;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = $model->title;

$backgroundImage = $model->image;

foreach ($model->images as $image){

        $backgroundImage = $image->path;

    if ($backgroundImage) {
        break;
    }
}

$thisUrl = Yii::$app->request->absoluteUrl;

?>

<section class="breadcrumbs__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs">
                    <a href="<?=Url::to(['/']) ?>"><?= Yii::t('common', 'Главная')?></a>
                    <a href="<?=Url::to(['/news']) ?>"><?= Yii::t('common', 'Новости')?></a>
                    <a href="" class="active"><?= $model->title ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="single__section">
    <div class="container to-animate">
        <div class="row">
            <div class="col-md-12">
                <div class="single__heading">
                    <h2><?= $model->title ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="single__info">
                    <div class="single__info-cat">
                        <?= Html::a($model->category->category->title, ['/news/news/category', 'alias' => $model->category->category->alias]) ?>
                        <div class="single__info-date">
                            <p><?=date("d.m.Y",strtotime($model->data_pub))?></p>
                        </div>
                    </div>
                    <div class="single__info-socials socials">
                        <a href="https://telegram.me/share/url?url=<?= $thisUrl ?>&text=<?= $model->title ?>" target="_blank">
                            <i class="fab fa-telegram-plane"></i>
                        </a>
                        <a href="viber://forward?text=<?= $model->title ?> <?= $thisUrl ?>" target="_blank">
                            <i class="fab fa-viber"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="single__image to-animate">
        <img src="<?= $backgroundImage ?>" alt="<?= $model->title ?>">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="single__content to-animate">
                    <?= $model->text ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="single__info">
                    <div class="single__info-cat">
                        <?= Html::a($model->category->category->title, ['/news/news/category', 'alias' => $model->category->category->alias]) ?>
                        <div class="single__info-date">
                            <p><?=date("d.m.Y",strtotime($model->data_pub))?></p>
                        </div>
                    </div>
                    <div class="single__info-socials socials">
                        <a href="https://telegram.me/share/url?url=<?= $thisUrl ?>&text=<?= $model->title ?>" target="_blank">
                            <i class="fab fa-telegram-plane"></i>
                        </a>
                        <a href="viber://forward?text=<?= $model->title ?> <?= $thisUrl ?>" target="_blank">
                            <i class="fab fa-viber"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (!empty($model->news)): ?>
    <section class="interest__section news">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="interest__heading to-animate">
                    <h2><?= Yii::t('common', 'Вам так же будет интересно:')?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?= $this->render('news-items', ['models' => $model->news, 'related' => true]) ?>

        </div>
    </div>
</section>
<?php endif; ?>


