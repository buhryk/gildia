<?php

use frontend\widgets\SubscribeWidget;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = $page->title;
?>

<section class="news news__archive">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="news__archive-heading to-animate">
                    <h2><?= Yii::t('common', 'Новости')?></h2>
                </div>
            </div>
        </div>
        <div class="row catssss">
            <div class="col-md-12">
                <div class="news__archive-cats to-animate">
                    <h3><?= Yii::t('common', 'Категории:')?></h3>
                    <div class="cat-dd">
                        <ul>
                            <li class="<?= $category_id ? '' : 'active' ?>"><a href="<?= Url::to(['/news/news/all']) ?>"><?= Yii::t('common', 'Все')?></a></li>
                            <?php foreach ($categories as $category): ?>
                                <li class="<?= $category_id != $category->id ? '' : 'active' ?>"><a href="<?= Url::to(['/news/news/all', 'category' => $category->id]) ?>"><?= $category->title ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php foreach (array_chunk($models, 2) as $items): ?>
            <div class="row">
                <?= $this->render('news-items', ['models' => $items]) ?>
            </div>
        <?php endforeach; ?>
    </div>
    <?= \frontend\widgets\SubscribeWidget::widget(['news' => true]) ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="pagination">
                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pagination,
                        'options' => ['class' => ''],
                        'firstPageLabel' => '<<',
                        'lastPageLabel' => '>>',
                        'activePageCssClass' => 'active',
                        'maxButtonCount' => 3,
                        'linkOptions' => ['class' => ''],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

