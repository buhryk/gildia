<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;use yii\widgets\LinkPager;

foreach ($models as $item): ?>
<?php if (isset($related)) {
    $model = $item->article;
} else {
        $model = $item;
    }
?>
                <div class="col-md-6 to-animate">
                <div class="news__item ">
                    <a href="<?= Url::to(['/news/news/view', 'alias' => $model->alias]) ?>" class="news__item-image">
                        <img src="<?= $model->image ?>" alt="<?= $model->title ?>">
                    </a>
                    <div class="news__item-info">
                        <div class="news__item-cats">
                            <?php if ($model->category->category): ?>
                                <a href="<?= Url::to(['/news/news/all', 'category' => $model->category->category->id]) ?>" class="news__item-cat">
                                    <p><?= $model->category->category->title ?></p>
                                </a>
                            <?php endif; ?>
                            <div class="news__item-date">
                                <p><?=date("d.m.Y",strtotime($model->data_pub))?></p>
                            </div>
                        </div>
                        <a href="<?= Url::to(['/news/news/view', 'alias' => $model->alias]) ?>" class="news__item-text">
                            <h3><?= $model->title ?></h3>
                            <?= $model->description ?>
                        </a>
                        <div class="news__item-link">
                            <?= Html::a(Yii::t('common', 'ПОДРОБНЕЕ'), ['/news/news/view', 'alias' => $model->alias]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
