<section class="hero__section to-animate us__section">
    <div class="container">
        <div class="line-wrapper">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="caption">
                    <h1 class=" to-animate"><?= Yii::t('common', 'Все услуги')?></h1>
                    <h2 class=" to-animate"><?= Yii::t('common', 'Выберите направление или выберите то, что вас интересует.')?></h2>
                    <div class="form to-animate">
                        <form name="test" action="<?= \yii\helpers\Url::to(['/catalog/catalog/category']) ?>">
                            <input type="text" name="search" placeholder="<?= Yii::t('common', 'Введите ваш запрос')?>">
                            <button type="submit"><?= Yii::t('common', 'Найти Услугу')?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="services__section-nb">
    <div class="container">

        <?php foreach([1,2,0] as $value): ?>
            <div class="coll-nb-dev">
                <?php $i = 1; foreach ($categories as $item): ?>
                    <?php if ($i % 3 == $value): ?>
                        <div class="box-dev-item to-animate">
                            <div class="top">
                                <a href="<?= \yii\helpers\Url::to(['/catalog/catalog/category', 'alias' => $item->alias]) ?>">
                                    <?= $item->title ?>
                                </a>
                                <div class="arrow-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="15" viewBox="0 0 13 15" fill="none">
                                        <path d="M0.0712891 8.56721L6.49988 15L12.9285 8.5693L12.173 7.81177L7.0356 12.9492L7.0356 0L5.96416 0L5.96416 12.9492L0.824621 7.80968L0.0712891 8.56721Z" fill="#242326"/>
                                    </svg>
                                </div>
                            </div>
                            <div class="bottom">

                                <?= $item->description ?>
                                <a href="<?= \yii\helpers\Url::to(['/catalog/catalog/category', 'alias' => $item->alias]) ?>">
                                    <?= Yii::t('common', 'Показать еще') ?>
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php $i++; endforeach; ?>
            </div>
        <?php endforeach; ?>

    </div>
</section>

