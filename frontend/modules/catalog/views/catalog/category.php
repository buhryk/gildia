<?php
use backend\modules\images\models\Image;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;

$this->title = Html::encode($category->title);

?>

<section class="permission__section">
    <div class="container">
        <div class="permission__section-heading">
            <h2 class="to-animate"><?= Yii::t('common', 'Услуги по Разрешениям')?></h2>
            <p class="to-animate"><?= Yii::t('common', 'Выберите направление или выберите то, что вас интересует.')?></p>
            <form action="" class="to-animate">
                <input type="text" name="search" value="<?= $search ?>" placeholder="<?= Yii::t('common', 'Введите ваш запрос')?>">
                <button class="button"><?= Yii::t('common', 'Найти услугу')?></button>
            </form>
        </div>
    </div>
</section>
<section class="permission ">
    <div class="container">
        <div class="permission-heading to-animate">
            <h3><?= Yii::t('common', 'Разрешения')?></h3>
        </div>
        <div class="permission-alpha to-animate">
            <?php foreach ($servicesGrouped as $k => $items): ?>
                <a class="ok" href="#<?= $k ?>"><?= $k ?></a>
            <?php endforeach; ?>
        </div>
        <?php foreach ($servicesGrouped as $k => $items): ?>
            <div id="<?= $k ?>" class="permission-wrapper to-animate">
                <div class="permission-wrapper-letter">
                    <h3><?= $k ?></h3>
                </div>
                <div class="permission-wrapper-text">
                    <ul>
                        <?php foreach ($items as $item): ?>
                            <li><a href="<?= $item['url'] ?>"><?= $item['title'] ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

