<section class="hero__section to-animate us__section service__section" style="background-image: url(<?= $model->image ?>)">
    <div class="container">
        <div class="line-wrapper">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="caption">
                    <?= $model->first_text ?>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form">
                    <h3><?= Yii::t('common', 'Оставить заявку')?></h3>
                    <?= \frontend\widgets\OrderFormWidget::widget([
                        'title' => 'Форма которая в самом верху услуги - ' . $model->title,
                        'form_id' =>  'header',
                        'user' => $user
                    ]) ?>
                 </div>
            </div>
        </div>
    </div>
</section>
<section class="about-project__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="about-project__heading to-animate">
                    <h2><?= Yii::t('common', 'Об услуге')?></h2>
                </div>
            </div>
        </div>
        <div class="about-project__list">
            <?php if ($model->description): ?>
                <div class="row to-animate">
                    <div class="col-md-3">
                        <div class="about-project-number">
                            <p></p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="about-project-text">
                            <?= $model->description ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($model->text): ?>
                <div class="row to-animate">
                    <div class="col-md-3">
                        <div class="about-project-number">
                            <p></p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="about-project-text">
                            <?= $model->text ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="work service__content">
    <div class="container">
        <div class="row">
            <?php foreach ($slider_1 as $item): ?>
                <div class="col-md-4 to-animate">
                    <div class="work__item">
                        <a href="<?= $item->url ?>"><h3><?= $item->title ?></h3></a>
                    </div>
                </div>
            <?php endforeach; ?>

            <div class="col-md-8 to-animate">
                <div class="service__descrip">
                    <?= $model->text_3 ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="why-us__section to-animate" style="background-image: url(<?= $model->image_2 ?>)">
    <div class="line-wrapper">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="why-us__heading to-animate">
                    <h2><?= Yii::t('common', 'Что вы получите после регистрации')?></h2>
                </div>
            </div>
        </div>
        <?php foreach (array_chunk($slider_2, 3) as $items): ?>
            <div class="row">
                <?php foreach ($items as $item): ?>
                    <div class="col-md-4">
                    <div class="why-us__item to-animate">
                        <div class="why-us__item-icon">
                            <img src="<?= $item->image ?>" alt="<?= $item->title ?>">
                        </div>
                        <div class="why-us__item-text">
                            <?= $item->text ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</section>
<section class="prices__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="prices__section-heading to-animate">
                    <h2><?= Yii::t('common', 'Наши цены')?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php foreach ($slider_3 as $item): ?>
                    <div class="prices__item to-animate">
                        <div class="prices__item-left">
                            <?= $item->text ?>
                        </div>
                        <div class="prices__item-right">
                            <p data-popup-btn="request-<?= $item->id ?>"><?= Yii::t('common', 'Заказать услугу')?></p>
                        </div>
                    </div>
                    <div class="popup-dev" data-popup="request-<?= $item->id ?>">
                        <div class="popup-dev__container">
                            <h2><?= Yii::t('common', 'Оставить заявку')?></h2>
                            <?= \frontend\widgets\OrderFormWidget::widget([
                                'title' =>  $item->title,
                                'form_id' =>  'service-'.$item->id,
                                'user' => $user
                            ]) ?>
<!--                            --><?//= \frontend\widgets\FormWidget::widget(['form' => 1, 'form_title' => $item->title]) ?>

                            <div class="exit-btn"></div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</section>
<section class="best__section to-animate">
    <div class="container">
        <div class="col-md-12">
            <div class="best__heading">
                <?= $model->text_4 ?>
            </div>
        </div>
    </div>
</section>
<section class="register__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="register__heading to-animate">
                    <h2><?= Yii::t('common', 'Регистрируйся сейчас')?></h2>
                </div>
            </div>
        </div>
        <div class="row to-animate">
            <div class="col-md-4">
                <div class="register__form">
                    <?= \frontend\widgets\OrderFormWidget::widget([
                        'title' =>  'Форма которая в самом низу услуги - ' . $model->title,
                        'form_id' =>  'footer',
                        'user' => $user
                    ]) ?>
<!--                    --><?//= \frontend\widgets\FormWidget::widget(['form' => 1, 'form_title' => 'Форма которая в самом низу услуги ' . $model->title]) ?>

                </div>
            </div>
            <div class="col-md-8">
                <div class="register__text">
                    <?= $model->text_5 ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="news news__archive faq__section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="news__archive-heading to-animate">
                    <h2><?= Yii::t('common', 'FAQ')?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="accordion" id="accordionExample">
                    <?php foreach ($faqs as $faq): ?>
                        <div class="card to-animate">
                        <div class="card-header" id="heading-<?= $faq->id ?>">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse-<?= $faq->id ?>" aria-expanded="false" aria-controls="collapseTwo">
                                    <?= $faq->title ?>
                                </button>
                            </h2>
                        </div>
                        <div id="collapse-<?= $faq->id ?>" class="collapse" aria-labelledby="heading-<?= $faq->id ?>" data-parent="#accordionExample">
                            <div class="card-body single__content">
                                <?= $faq->text ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="seek">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="seek__content">
                    <h3><?= Yii::t('common', 'Не увидели что искали?')?></h3>
                    <a href="<?= \yii\helpers\Url::to(['/site/faq']) ?>"><?= Yii::t('common', 'Посмотрите тут')?></a>
                </div>
            </div>
        </div>
    </div>
</section>