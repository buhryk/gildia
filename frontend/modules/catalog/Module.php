<?php

namespace frontend\modules\catalog;

/**
 * news module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\catalog\controllers';

//   public $layout = '/news';


    public function init()
    {
        parent::init();

    }
}
