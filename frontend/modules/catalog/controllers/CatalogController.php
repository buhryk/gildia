<?php

namespace frontend\modules\catalog\controllers;

use backend\modules\catalog\models\CatalogToCategory;
use backend\modules\catalog\models\Property;
use backend\modules\core\models\Page;
use backend\modules\email_delivery\models\Subscriber;
use backend\modules\faq\models\Faq;
use backend\modules\images\models\Image;
use backend\modules\catalog\models\CatalogCategory;
use backend\modules\slider\models\Slider;
use common\models\Lang;
use frontend\models\EditForm;
use frontend\models\User;
use Yii;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use backend\modules\catalog\models\Catalog;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `catalog` module
 */
class CatalogController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex()
    {
        $categories = CatalogCategory::find()
            ->where(['status' => CatalogCategory::STATUS_ACTIVE])
            ->orderBy('position')
            ->all();

        return $this->render('index', [
            'categories' => $categories,
        ]);


    }

    public function actionCategory($alias = false, $search = false)
    {
        $catalog_id = CatalogCategory::find()->where([
            'status' => CatalogCategory::STATUS_ACTIVE,
            'alias' => $alias
            ])->one()->id;


        $servicesQuery = Catalog::find()
            ->leftJoin('catalog_to_category', 'id = catalog_id')
            ->leftJoin('catalog_lang', 'id = record_id')
            ->where([
            'status' => Catalog::STATUS_ACTIVE,
            'lang_id' => Lang::getCurrent()->id,
        ]);

        if ($alias) {
            $servicesQuery->andWhere(['category_id' => $catalog_id]);
        }

        if ($search) {
            $servicesQuery->andWhere(['like', 'title', $search]);
        }

        $services = $servicesQuery->orderBy('title')->all();

        $servicesGrouped = [];

        foreach ($services as $service) {
            $servicesGrouped[mb_substr($service->title, 0, 1)][] = [
                'title' => $service->title,
                'url' => Url::to(['/catalog/catalog/view', 'alias' => $service->alias]),
            ];
        }
//        pr($servicesGrouped);
        return $this->render('category', [
            'servicesGrouped' => $servicesGrouped,
            'search' => $search
        ]);
    }


    public function actionView($alias)
    {
        $model = $this->findModel($alias);
//        pr($model);

        $slider_1 = Slider::getSlider($model->slider_1);
        $slider_2 = Slider::getSlider($model->slider_2);
        $slider_3 = Slider::getSlider($model->slider_3);

        $faqs = Faq::find()->where(['status' => Faq::STATUS_ACTIVE, 'page_id' => $model->id])->all();

//        pr($faqs);
        $user = false;
        if (Yii::$app->user->identity) {
            $user = User::find()->where(['id' => Yii::$app->user->identity->id ])->one();
        }

        return $this->render('view', [
            'user' => $user,
            'model' => $model,
            'faqs' => $faqs,
            'slider_1' => $slider_1,
            'slider_2' => $slider_2,
            'slider_3' => $slider_3,
        ]);
    }

    protected function findCategory($alias)
    {
        if (($model = catalogCategory::find()->where(['alias' => $alias, 'status' => catalogCategory::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function findModel($alias)
    {
        if (($model = Catalog::find()->where(['alias' => $alias, 'status' => Catalog::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function creationQuery()
    {
        $query = CatalogCategory::find();
        $query->andWhere(['status' => CatalogCategory::STATUS_ACTIVE]);
        $query->orderBy(['position' => SORT_DESC]);

        return $query;
    }
}
