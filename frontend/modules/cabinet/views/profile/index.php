<?php
use yii\helpers\Url;
use frontend\modules\cabinet\widgets\CabinetMenu;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use frontend\assets\RegistrationPageAsset;
use borales\extensions\phoneInput\PhoneInput;

RegistrationPageAsset::register($this);
$this->title = Yii::t('cabinet', 'Личный данные')

?>

<div class="section profile">

    <div class="row">
        <div class="column">
            <ul class="crumbs">
                <li><a href="<?=Url::to(['/']) ?>"><?=Yii::t('common', 'Главная') ?></a></li>
                <li><?=$this->title ?></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_toppaddoff"><?=Yii::t('cabinet', 'Личный кабинет') ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="column small-12">
            <div class="section__caption section__caption_twoline profile__caption">
                <span class="section__line"></span>
                <h2 class="section__title shrink"><?=$this->title ?></h2>
                <span class="section__line"></span>
            </div>
        </div>
    </div>

    <div class="profile__content">

        <div class="row">
            <?=CabinetMenu::widget() ?>
            <div class="columns small-12 medium-7 large-10">

               <div class="row">
                    <div class="column small-12 medium-12 large-6">
                        <?php $form = ActiveForm::begin() ?>
                            <div class="form__wrap">
                                <?= $form->field($model, 'name', [
                                    'template' => '{input}{error}',
                                    'labelOptions' => ['class' => 'form__label'],
                                ])
                                    ->textInput([
                                        'class' => 'form__input',
                                        'placeholder' => "*".Yii::t('cabinet', 'Изменить имя')
                                    ]);
                                ?>

                                <?= $form->field($model, 'surname', [
                                    'template' => '{input}{error}',
                                    'labelOptions' => ['class' => 'form__label'],
                                ])
                                    ->textInput([
                                        'class' => 'form__input',
                                        'placeholder' => "*".Yii::t('cabinet', 'Изменить фамилию')
                                    ]);
                                ?>

                                <label for="date" class="form__label form__label_bday">
                                    <?= DatePicker::widget([
                                        'model' => $model,
                                        'attribute' => 'birthday',
                                        'language' => 'ru',
                                        'dateFormat' => 'yyyy-MM-dd',
                                        'clientOptions' => [
                                            'changeMonth' => true,
                                            'changeYear' => true,
                                            'yearRange' => "-80y:-10y",
                                            'minDate' => "-80y",
                                            'maxDate' => "-10y"
                                        ],
                                        'options' => ['placeholder' => Yii::t('common', 'Дата рождения'), 'id' => 'date', 'class' => 'form__input']
                                    ]); ?>
                                    <i class="flaticon-calendar"></i>
                                    <?php if ($model->getErrors('birthday')) { ?>
                                        <p class="help-block help-block-error"><?= implode(', ', $model->getErrors('birthday')); ?></p>
                                    <?php } ?>
                                </label>

                                <div class="form__row">
                                    <div class="large-6">
                                        <?= $form->field($model, 'country', [
                                            'template' => '{input}{error}',
                                            'labelOptions' => ['class' => 'form__label form__label_half small-12 '],
                                        ])
                                            ->textInput([
                                                'class' => 'form__input',
                                                'id' => 'country',
                                                'placeholder' => "*".Yii::t('cabinet', 'Выбрать страну')
                                            ]);
                                        ?>
                                    </div>
                                    <div class="large-6">
                                        <?= $form->field($model, 'city', [
                                            'template' => '{input}{error}',
                                            'labelOptions' => ['class' => 'form__label form__label_half-2 small-12'],
                                        ])
                                            ->textInput([
                                                'class' => 'form__input',
                                                'id' => 'city',
                                                'placeholder' => "*".Yii::t('cabinet', 'Выбрать город')
                                            ]);
                                        ?>
                                    </div>
                                </div>

                                <?= $form->field($model, 'address', [
                                    'template' => '{input}{error}',
                                    'labelOptions' => ['class' => 'form__label']
                                ])
                                    ->textInput([
                                        'class' => 'form__input',
                                        'id' => 'address',
                                        'placeholder' => "*".Yii::t('cabinet', 'Введите адрес')
                                    ]);
                                ?>

                                <?= $form->field($model, 'card_number', [
                                    'template' => '{input}{error}',
                                    'labelOptions' => ['class' => 'form__label']
                                ])
                                    ->textInput([
                                        'class' => 'form__input',
                                        'placeholder' => Yii::t('cabinet', 'Введите номер карточки')
                                    ]);
                                ?>

                                <?= $form->field($model, 'email', [
                                    'template' => '{input}{error}',
                                    'labelOptions' => ['class' => 'form__label'],
                                ])
                                    ->textInput([
                                        'class' => 'form__input',
                                        'type' => 'email',
                                        'placeholder' => "*".Yii::t('cabinet', 'Изменить email')
                                    ]);
                                ?>

                                <?= $form->field($model, 'phone', [
                                    'template' => '{input}{error}',
                                    'labelOptions' => ['class' => 'form__label'],
                                ])->widget(PhoneInput::className(), [
                                    'options' => [ 'class' => 'form__input'],
                                    'jsOptions' => [
                                        'preferredCountries' => ['ua', 'ru', 'en'],
                                        'allowExtensions' => true,
                                        'nationalMode' => false
                                    ],
                                ], ['class' => 'form__input']); ?>
                            </div>
                            <div class="form__group form__group_check profile__check">
                                <input type="hidden"  name="User[subscription]" value="0">
                                <?=\yii\helpers\Html::checkbox('User[subscription]', $model->subscription, ['class' => 'checkbox-preload', 'id' => 'subscribe-check'] ) ?>
                                <label for="subscribe-check"><?=Yii::t('cabinet', 'Подписаться на рассылку') ?></label>
                            </div>
                            <div class="form__group form__group_btn profile__group_btn">
                                <button class="btn btn_small"><?=Yii::t('cabinet', 'Сохранить') ?></button>
                            </div>
                        <?php ActiveForm::end(); ?>
                        </div>
                       <div class="column small-12 medium-12 large-6 profile__column_offset">
                           <?php $form = ActiveForm::begin() ?>
                                <div class="form__wrap">
                                    <p class="profile__change"><?=Yii::t('cabinet', 'Изменить пароль') ?></p>
                                    <?= $form->field($modelChange, 'newPassword', [
                                        'template' => '{input}{error}',
                                        'labelOptions' => ['class' => 'form__label'],
                                    ])
                                        ->textInput([
                                            'class' => 'form__input',
                                            'placeholder' => "*".Yii::t('cabinet', 'Введите новый пароль')
                                        ]);
                                    ?>
                                    <?= $form->field($modelChange, 'newPasswordRepeat', [
                                        'template' => '{input}{error}',
                                        'labelOptions' => ['class' => 'form__label'],
                                    ])
                                        ->textInput([
                                            'class' => 'form__input',
                                            'placeholder' => "*".Yii::t('cabinet', 'Повторите пароль')
                                        ]);
                                    ?>
                                </div>
                                <div class="form__group form__group_btn profile__group_btn">
                                    <button class="btn btn_small"><?=Yii::t('cabinet', 'Сохранить') ?></button>
                                </div>
                           <?php ActiveForm::end(); ?>
                       </div>
                </div>

            </div>
        </div>
    </div>
</div>
