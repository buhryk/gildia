<?php
namespace frontend\modules\cabinet\controllers;

use frontend\models\User;
use yii\web\Controller;
use frontend\modules\cabinet\models\PasswordChangeForm;
use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class ProfileController extends Controller
{
    public function actionIndex()
    {
        $user_id = Yii::$app->user->id;
        $model = User::findOne($user_id);
        $model->scenario = 'update';
        $modelChange = new PasswordChangeForm($model);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } elseif ($modelChange->load(Yii::$app->request->post()) && $modelChange->changePassword()) {
            return $this->redirect(['index']);
        }

        return $this->render('index', ['model' => $model, 'modelChange' => $modelChange]);
    }

}
